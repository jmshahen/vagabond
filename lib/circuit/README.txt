Circuit.jar contains proprietary code, but we have elected to share the JAR file
in order to allow other researchers and cloud computing hosts to run the software.

Circuit.jar is used to convert a Circuit-SAT problem into a CNF-SAT problem, we
can then run it through a boolean satisfiability solver (we chose lingeling).

-- Jonathan Shahen
