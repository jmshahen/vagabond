# I am a comment
maxCCIL=false
#reduction=nomad
#reduction=ilp
reduction=cnf
# randomPlacement equalSpread equalSpreadHalf nomadTest percentFill nahidTest
placement=equalSpreadHalf2
equalSpreadNum=7
migrationBudget=-1
numberOfEpochs=1
#
numberOfMachines=5
numberOfMachineSlots=4
numberOfClients=6
numberOfVMsPerClient=3
fillInEmpty=false
# Keep at 5 unless you recompile NOMAD for a different value
slidingWindow=5
# OPTIONAL: Can Comment these lines out
#seed=10
name=equalSpread_1Epoch_MB-1
comment=