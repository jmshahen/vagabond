/*********************************************
 * OPL 12.6.1.0 Model
 * Author: jonathan
 * Creation Date: May 17, 2016 at 12:26:31 PM
 *********************************************/

int numMachines=...;
range Machines=1..numMachines;

int numClients=...;
range Clients=1..numClients;

//int numVMs=...;
int migrationBudget=...;
int vmsPerClient[Clients]=...;
int machineCapacity[Machines]=...;
 
// Sum of previous Information Leakage ClinetToClient
int L[Clients,Clients]=...;
// Sum of Client VMs per Machine
int p0[Machines,Clients]=...;

int r=...;
int t=ftoi(ceil(lg(r+1)));
int dMaxVal=maxl(ftoi(pow(2,(2*t)-1)) - 1, 1);

range tRange=0..t-1;
range zRange=0..t-1;

// NEW Placement of the Sum of Client VMs per Machine
dvar int p1[Machines,Clients] in 0..r;
// Variables to fix the quadratic variable from old reduction
dvar int p[Machines,Clients,tRange] in 0..1;
dvar int e[Clients,Clients,Machines,tRange,tRange] in 0..1;
dvar int d[Clients,Clients,Machines,zRange] in 0..dMaxVal;

minimize
  sum(c0, c1 in Clients: c0 != c1) (L[c0,c1] + sum(k in Machines, z in zRange) (d[c0,c1,k,z]) );
  
subject to {
	// p1 represented as a value from p's bit form
	forall(i in Clients, k in Machines) 
		p1[k,i] == sum(l in tRange) (pow(2,l) * p[k,i,l]);
	// Bitwise AND for all pairs in p1
	forall(k in Machines, c0 in Clients, c1 in Clients, u in tRange, v in tRange)
	  0 <= p[k,c0,u] + p[k,c1,v] - (2 * e[c0,c1,k,u,v]) <= 1;
	// Setting up values for d
	forall(k in Machines, c0 in Clients, c1 in Clients, z in zRange)
	  d[c0,c1,k,z] == sum(l in zRange) (pow(2,z+l) * e[c0,c1,k,l,z]);
	// Capacity of servers respected
	forall(k in Machines)
	  machineCapacity[k] >= sum(i in Clients) (p1[k,i]);
	// Sum of clients in p1 should equal the number of vms of that client
	forall(i in Clients)
	  vmsPerClient[i] == sum(k in Machines) (p1[k,i]);
	// Migration budget
	2 * migrationBudget >= sum(i in Clients, k in Machines) ( abs(p1[k,i] - p0[k,i]) );
};