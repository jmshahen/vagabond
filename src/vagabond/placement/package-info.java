/**
 *
 * Contains functions related to producing PlacementMaps used for running the program (i.e. not for testing).
 */
package vagabond.placement;