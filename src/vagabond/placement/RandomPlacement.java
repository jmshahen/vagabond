package vagabond.placement;

import java.util.ArrayList;
import java.util.Random;
import java.util.logging.Level;
import java.util.logging.Logger;

import vagabond.pieces.PlacementMap;
import vagabond.pieces.VM;
import vagabond.singleton.VagabondSettings;
import vagabond.timing.TimingManager;

public class RandomPlacement {
    // Settings
    public static VagabondSettings _settings;

    // Logger
    public static Logger logger;
    public static final Level FINE = Level.FINE; // Makes for Shorter code

    // Timing
    public TimingManager timing = null;

    // Random Generator
    public Random randGen;

    public long randomSeed = -1;

    public RandomPlacement() {
        // Connect to the global logger
        _settings = VagabondSettings.getInstance();
        logger = _settings.getLogger();
        timing = _settings.timing;

        randGen = new Random();
    }

    /**
     * Set the random seed to allow for repeatable tests
     * @param randomSeed Random Generator seed
     */
    public void randomSeed(long randomSeed) {
        if (randomSeed != -1) {
            randGen.setSeed(randomSeed);
        }

        this.randomSeed = randomSeed;
    }

    /**
     * Generates a random placement map using the global settings.
     * @return PlacementMap that has randomly placed VMs
     */
    public PlacementMap generatePlacementMapFromSettings() {
        // Quick access variables
        Integer machines = _settings.numberOfMachines;
        Integer slots = _settings.numberOfMachineSlots;
        Integer clients = _settings.numberOfClients;
        Integer vmsperclient = _settings.numberOfVMsPerClient;
        Boolean fillInEmpty = _settings.fillInEmptySpots;

        return generatePlacementMap(machines, slots, clients, vmsperclient, fillInEmpty);
    }

    /**
     * Generates a random Placement Map using the settings provided as parameters
     * @param machines the number of machines
     * @param slots the number of slots that each machine has
     * @param clients the number of clients
     * @param vmsperclient the number of VMs per client
     * @param fillInEmpty If TRUE then fill in all empty space with random new client VMs
     * @return a randomly placed PlacementMap from the settings provided
     */
    public PlacementMap generatePlacementMap(Integer machines, Integer slots, Integer clients, Integer vmsperclient,
            Boolean fillInEmpty) {
        /* Timing */timing.startTimer("generatePlacementMap");

        // Sanity Check
        if (clients * vmsperclient > machines * slots) {
            logger.severe("You have too many Clients, " + clients + ", and VMs per Client, " + vmsperclient + ", " +
                    "and too few machines, " + machines + ", and Machine Slots, " + slots + "! "
                    + (clients * vmsperclient) + " > " + (machines * slots));

            return null;
        }

        // Setup the PlacementMap
        PlacementMap place = new PlacementMap(machines, slots);

        // Create a public pool of all the VMs
        ArrayList<VM> vmPool = new ArrayList<VM>();

        for (int c = 0; c < clients; c++) {
            for (int s = 0; s < vmsperclient; s++) {
                vmPool.add(new VM(c, s, place.getNextAvailableVMID()));
            }
        }

        // if (logger.isLoggable(Level.FINE)) {
        // logger.fine("Public Pool of VMS: " + vmPool);
        // }

        // Create a public pool of all the machine IDs
        ArrayList<Integer> machineIDPool = new ArrayList<Integer>();

        for (int m = 0; m < machines; m++) {
            machineIDPool.add(m);
        }

        // Randomly add VMs to a random Machine
        Integer machineID = null;
        Integer machineIDIndex = null;
        VM vm = null;
        while (!vmPool.isEmpty()) {
            // Get a random Machine ID
            machineIDIndex = randGen.nextInt(machineIDPool.size());
            machineID = machineIDPool.get(machineIDIndex);
            // Get a random VM and remove it from the list
            vm = vmPool.remove(randGen.nextInt(vmPool.size()));

            place.addNewVMToMachine(vm, machineID);

            // Remove machines that are FULL
            if (place.isMachineFull(machineID)) {
                // HOTFIX: requires the .intValue() or else it removes the object and not the index!
                machineIDPool.remove(machineIDIndex.intValue());
            }
        }

        // Fill in the extra space if the flag is set and there is space
        if (fillInEmpty && !place.isFull()) {
            Integer spotsToFill = (machines * slots) - (clients * vmsperclient);

            // All of the available clients
            ArrayList<Integer> clientIDs = new ArrayList<Integer>(place._clients.keySet());
            Integer clientIndex = null;
            VM tempVM = null;
            for (int i = 0; i < spotsToFill; i++) {
                // Randomly pick a client and give them an extra VM
                clientIndex = randGen.nextInt(clientIDs.size());
                tempVM = new VM(clientIDs.get(clientIndex), place.getNextAvailableServiceID(clientIDs.get(clientIndex)),
                        place.getNextAvailableVMID());

                place.addNewVMToMachine(tempVM, place.getFirstFreeMachineID());
            }

        }

        /* Timing */timing.stopTimer("generatePlacementMap");
        return place;
    }

    /**
     * Test parameters taken from NOMAD paper on page 1603. Interpreting "50% occupancy rate" to mean that the 
     * vmsperclient = 2 because:
     * <ol>
     * <li>40 machines * 4 slots = 160 available spaces</li>
     * <li>20 clients * vmsperclient / 160 available spaces = 50%</li>
     * <li>vmsperclient = 4</li>
     * </ol>
     * @return Random PlacementMap as described by NOMAD (the closed system equivalent
     */
    public PlacementMap generatePlacementMapNomadHardILP() {
        return generatePlacementMap(40, 4, 20, 4, false);
    }

}
