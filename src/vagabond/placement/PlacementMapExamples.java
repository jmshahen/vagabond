package vagabond.placement;

import vagabond.pieces.PlacementMap;
import vagabond.pieces.VM;
import vagabond.singleton.VagabondSettings;

public class PlacementMapExamples {

    /**
     * Creates a Placement Map where the number of machines is equal to the number of clients and equal to the number 
     * of slots per machine. This allows the the Placement Map to be completely filled by putting 1 VM from each client
     * on a single machine. This creates a Placement Map where the SumOfClientVMsPerMachine is a matrix completely
     * filled with '1's.  
     * @param machineAndSlotAndClientSize the number of machines, also the number of slots per machine, and also the 
     * number of clients
     * @return an evenly distributed Placement Map where each client has exactly 1 VM on a machine and all machines are 
     * full
     */
    public static PlacementMap equalSpreadClients(int machineAndSlotAndClientSize) {
        PlacementMap place = new PlacementMap(machineAndSlotAndClientSize, machineAndSlotAndClientSize);

        int machineID = 0;
        for (int serviceID = 0; serviceID < machineAndSlotAndClientSize; serviceID++) {
            machineID = serviceID;
            for (int clientID = 0; clientID < machineAndSlotAndClientSize; clientID++) {
                place.addNewVMToMachine(
                        new VM(clientID, serviceID, place.getNextAvailableVMID()),
                        machineID);
            }
        }

        VagabondSettings s = VagabondSettings.getInstance();
        s.numberOfClients = machineAndSlotAndClientSize;
        s.numberOfMachines = machineAndSlotAndClientSize;
        s.numberOfMachineSlots = machineAndSlotAndClientSize;
        s.numberOfVMsPerClient = machineAndSlotAndClientSize;

        return place;
    }

    /**
     * Provides the 3x3 standard example that is talked about frequently during development.
     * <ul>
     * <li>PlacementMap(3 Machines; 3 Clients; 9 Total VMS)</li>
     * <li>Machine 0 {1&lt;0,0&gt;, 2&lt;1,0&gt;, 3&lt;2,0&gt;}</li>
     * <li>Machine 1 {4&lt;0,1&gt;, 5&lt;1,1&gt;, 6&lt;2,1&gt;}</li>
     * <li>Machine 2 {7&lt;0,2&gt;, 8&lt;1,2&gt;, 9&lt;2,2&gt;}</li>
     * </ul>
     * @return an equally spread placement map for 3x3x3x3 (machines x slots x clients x vms per client)
     */
    public static PlacementMap equalSpreadClients() {
        return equalSpreadClients(3);
    }

    /**
     * Creates a Placement Map where the number of machines is equal to HALF the number of clients and equal to the 
     * number of slots per machine and equal to the number of VMs per client. This allows the the Placement Map to be 
     * half filled.
     * @param machineAndSlotAndClientSize the number of machines, also the number of slots per machine, and also half  
     * half number of clients
     * @return an evenly distributed Placement Map where each client has exactly 1 VM on a machine and all machines are 
     * half full
     */
    public static PlacementMap equalSpreadClientsHalfFull(int machineAndSlotAndClientSize) {
        PlacementMap place = new PlacementMap(machineAndSlotAndClientSize, machineAndSlotAndClientSize);

        int machineID = 0;
        int half = (int) (((double) machineAndSlotAndClientSize) / 2);
        for (int serviceID = 0; serviceID < machineAndSlotAndClientSize; serviceID++) {
            machineID = serviceID;
            for (int clientID = 0; clientID < half; clientID++) {
                place.addNewVMToMachine(
                        new VM(clientID, serviceID, place.getNextAvailableVMID()),
                        machineID);
            }
        }

        VagabondSettings s = VagabondSettings.getInstance();
        s.numberOfClients = half;
        s.numberOfMachines = machineAndSlotAndClientSize;
        s.numberOfMachineSlots = machineAndSlotAndClientSize;
        s.numberOfVMsPerClient = machineAndSlotAndClientSize;

        return place;
    }

    public static PlacementMap equalSpreadClientsHalfFull2(int machineAndSlotAndClientSize) {
        PlacementMap place = new PlacementMap(machineAndSlotAndClientSize, machineAndSlotAndClientSize);

        int machineID = 0;
        int half = (int) (((double) machineAndSlotAndClientSize) / 2);
        for (int serviceID = 0; serviceID < half; serviceID++) {
            machineID = serviceID;
            for (int clientID = 0; clientID < machineAndSlotAndClientSize; clientID++) {
                place.addNewVMToMachine(
                        new VM(clientID, serviceID, place.getNextAvailableVMID()),
                        machineID);
            }
        }

        VagabondSettings s = VagabondSettings.getInstance();
        s.numberOfClients = machineAndSlotAndClientSize;
        s.numberOfMachines = machineAndSlotAndClientSize;
        s.numberOfMachineSlots = machineAndSlotAndClientSize;
        s.numberOfVMsPerClient = half;

        return place;
    }

    /**
     * Provides the 4x4 standard example but each client has half the number of VMs.
     * <ul>
     * <li>PlacementMap(4 Machines; 4 Clients; 8 Total VMS)</li>
     * <li>Machine 0 {1&lt;0,0&gt;, 2&lt;1,0&gt; [], []}</li>
     * <li>Machine 1 {4&lt;0,1&gt;, 5&lt;1,1&gt; [], []}</li>
     * <li>Machine 2 {7&lt;0,2&gt;, 8&lt;1,2&gt; [], []}</li>
     * <li>Machine 3 {7&lt;0,2&gt;, 8&lt;1,2&gt; [], []}</li>
     * </ul>
     * @return an equally spread placement map for 3x3x3x3 (machines x slots x clients x vms per client)
     */
    public static PlacementMap equalSpreadClientsHalfFull() {
        return equalSpreadClientsHalfFull(4);
    }

    public static PlacementMap specialTest1(RandomPlacement rp, int specialSize) {
        int half = specialSize / 2;
        int dbl = specialSize * 2;

        VagabondSettings s = VagabondSettings.getInstance();
        s.numberOfMachines = specialSize;
        s.numberOfMachineSlots = specialSize;
        s.numberOfClients = dbl;
        s.numberOfVMsPerClient = half;

        return rp.generatePlacementMap(s.numberOfMachines, s.numberOfMachineSlots, s.numberOfClients,
                s.numberOfVMsPerClient, false);
    }

    public static PlacementMap nomadTest(RandomPlacement rp, int specialSize) {
        VagabondSettings s = VagabondSettings.getInstance();
        s.numberOfMachines = specialSize;
        s.numberOfMachineSlots = 4;
        s.numberOfClients = specialSize;
        s.numberOfVMsPerClient = 2;

        return rp.generatePlacementMap(s.numberOfMachines, s.numberOfMachineSlots, s.numberOfClients,
                s.numberOfVMsPerClient, false);
    }

    public static PlacementMap specialTest2(RandomPlacement rp, int specialSize) {
        int quarter = (int) (((double) specialSize) / 4);
        int dbl = specialSize * 2;

        VagabondSettings s = VagabondSettings.getInstance();
        s.numberOfMachines = specialSize;
        s.numberOfMachineSlots = specialSize;
        s.numberOfClients = dbl;
        s.numberOfVMsPerClient = quarter;

        return rp.generatePlacementMap(s.numberOfMachines, s.numberOfMachineSlots, s.numberOfClients,
                s.numberOfVMsPerClient, false);
    }

    public static PlacementMap randomHalfFull(RandomPlacement rp, int specialSize) {
        int half = (int) (((double) specialSize) / 2);

        VagabondSettings s = VagabondSettings.getInstance();
        s.numberOfClients = specialSize;
        s.numberOfMachines = specialSize;
        s.numberOfMachineSlots = specialSize;
        s.numberOfVMsPerClient = half;

        return rp.generatePlacementMap(s.numberOfMachines, s.numberOfMachineSlots, s.numberOfClients,
                s.numberOfVMsPerClient, false);
    }

    public static PlacementMap percentFilled(RandomPlacement rp, int specialSize) {
        VagabondSettings s = VagabondSettings.getInstance();
        s.numberOfMachines = 8;// 5;
        s.numberOfMachineSlots = 4;// 4;
        s.numberOfClients = 10;// 6;
        double val2 = (double) specialSize / 100;
        int val = (int) Math.floor(val2 * s.numberOfMachines * s.numberOfMachineSlots / s.numberOfClients);
        s.numberOfVMsPerClient = val;

        System.out.println("[percentFilled] Value: " + val);
        System.out.println("[percentFilled] Value2: " + val2);

        return rp.generatePlacementMap(s.numberOfMachines, s.numberOfMachineSlots, s.numberOfClients,
                s.numberOfVMsPerClient, false);
    }

    /**
     * Creates a Placement Map where the number of machines is equal to the number of clients and equal to the number 
     * of slots per machine. This allows the the Placement Map to be completely filled by putting all VMs from a client
     * on a single machine. This creates a Placement Map where the SumOfClientVMsPerMachine is a matrix diagonal
     * of the values machineAndSlotAndClientSize and an Information Leakage of zero.  
     * @param machineAndSlotAndClientSize the number of machines, also the number of slots per machine, and also the 
     * number of clients
     * @return a Placement Map where each client has all VMs on a machine and all machines are full
     */
    public static PlacementMap optimalPlacement(int machineAndSlotAndClientSize) {
        PlacementMap place = new PlacementMap(machineAndSlotAndClientSize, machineAndSlotAndClientSize);

        int machineID = 0;
        for (int clientID = 0; clientID < machineAndSlotAndClientSize; clientID++) {
            machineID = clientID;
            for (int serviceID = 0; serviceID < machineAndSlotAndClientSize; serviceID++) {
                place.addNewVMToMachine(
                        new VM(clientID, serviceID, place.getNextAvailableVMID()),
                        machineID);
            }
        }

        VagabondSettings s = VagabondSettings.getInstance();
        s.numberOfClients = machineAndSlotAndClientSize;
        s.numberOfMachines = machineAndSlotAndClientSize;
        s.numberOfMachineSlots = machineAndSlotAndClientSize;
        s.numberOfVMsPerClient = machineAndSlotAndClientSize;

        return place;
    }

    /**
     * Provides the optimal placement for the 3x3 standard example that is talked about frequently during development.
     * <ul>
     * <li>PlacementMap(3 Machines; 3 Clients; 9 Total VMS)</li>
     * <li>Machine 0 {1&lt;0,0&gt;, 2&lt;0,1&gt;, 3&lt;0,2&gt;}</li>
     * <li>Machine 1 {4&lt;1,0&gt;, 5&lt;1,1&gt;, 6&lt;1,2&gt;}</li>
     * <li>Machine 2 {7&lt;2,0&gt;, 8&lt;2,1&gt;, 9&lt;2,2&gt;}</li>
     * </ul>
     * @return optimal placement map for a 3x3x3x3 (machines x slots x clients x vms per client)
     */
    public static PlacementMap optimalPlacement() {
        return optimalPlacement(3);
    }

    /**
     * Nahid produced an initial placement map where Nomad cannot get to the optimal solution 
     * (even with infinite migration budget). In fact it does not migrate any VMs when presented with this
     * placement map.
     * @return a placement map specifically designed for the &lt;R,C&gt; information leakage in a closed system
     */
    public static PlacementMap nomadSubOptimalRCPlacement() {
        PlacementMap place = new PlacementMap(3, 3);

        VM vm;

        /**
         * MACHINE 0
         */
        // 1a in the example
        vm = new VM(0, 0, place.getNextAvailableVMID());
        place.addNewVMToMachine(vm, 0);

        // 1b in the example
        vm = new VM(0, 1, place.getNextAvailableVMID());
        place.addNewVMToMachine(vm, 0);

        // 4a in the example
        vm = new VM(3, 0, place.getNextAvailableVMID());
        place.addNewVMToMachine(vm, 0);

        /** 
         * MACHINE 1
         */
        // 2a in the example
        vm = new VM(1, 0, place.getNextAvailableVMID());
        place.addNewVMToMachine(vm, 1);

        // 2b in the example
        vm = new VM(1, 1, place.getNextAvailableVMID());
        place.addNewVMToMachine(vm, 1);

        // 4b in the example
        vm = new VM(3, 1, place.getNextAvailableVMID());
        place.addNewVMToMachine(vm, 1);

        /**
         * MACHINE 2
         */
        // 3a in the example
        vm = new VM(2, 0, place.getNextAvailableVMID());
        place.addNewVMToMachine(vm, 2);

        // 3b in the example
        vm = new VM(2, 1, place.getNextAvailableVMID());
        place.addNewVMToMachine(vm, 2);

        // 4c in the example
        vm = new VM(3, 2, place.getNextAvailableVMID());
        place.addNewVMToMachine(vm, 2);

        VagabondSettings s = VagabondSettings.getInstance();
        s.numberOfClients = 4;
        s.numberOfMachines = 3;
        s.numberOfMachineSlots = 3;
        s.numberOfVMsPerClient = 3; // The maximum

        return place;
    }
}
