package vagabond.placement;

import java.io.*;
import java.nio.charset.Charset;
import java.nio.file.*;
import java.util.*;
import java.util.logging.Level;
import java.util.logging.Logger;

import org.apache.commons.csv.*;

import vagabond.enums.PlacementMapDisplayStyle;
import vagabond.pieces.*;
import vagabond.results.StatisticsManager;
import vagabond.singleton.VagabondSettings;

/**
 * Helper class for reading and writing PlacementMaps
 * @author Jonathan Shahen
 *
 */
public class PlacementMapFile {
    /**
     * Customizable comment characters; if a line starts with this string it will be ignored
     */
    public static ArrayList<String> commentStrs = new ArrayList<String>(Arrays.asList("//", "#"));

    public static PlacementMap arrangePlacementMap(Hashtable<Integer, ArrayList<VM>> flexiblePlacement) {
        Integer numberOfMachines = flexiblePlacement.size();
        Integer maxSlots = 0;
        for (Integer mID : flexiblePlacement.keySet()) {
            if (maxSlots < flexiblePlacement.get(mID).size()) {
                maxSlots = flexiblePlacement.get(mID).size();
            }
        }

        PlacementMap place = new PlacementMap(numberOfMachines, maxSlots);

        for (Integer mID : flexiblePlacement.keySet()) {
            for (VM vm : flexiblePlacement.get(mID)) {
                place.addNewVMToMachine(vm, mID);
            }
        }

        return place;
    }

    /**
     * Reads in a placement map that can be written by hand or taken from the NOMAD edited test-cases or from 
     * PlacementMapFile::writePlacementMap(). All lines that start with one of the strings from 
     * PlacementMapFile::commentStrs will be ignored.
     * 
     * @param filename the path to the file that is to be read in
     * @return a PlacementMap that was represented in the file, null if any issues occur
     * @throws IOException occurs if the placement file does not exist or is unable to read from it
     */
    public static PlacementMap readPlacementMap(String filename) throws IOException {
        Logger logger = VagabondSettings.getInstance().getLogger();

        File input = new File(filename);

        if (!input.exists()) {
            logger.severe("The filename given does not point to a file that exists! Filename = " + filename);
            throw new IOException("The filename given does not point to a file that exists! Filename = " + filename);
        }

        CSVParser parser = CSVParser.parse(input, Charset.defaultCharset(), CSVFormat.RFC4180);

        // Machine ID => VMs
        Hashtable<Integer, ArrayList<VM>> flexiblePlacement = new Hashtable<>();
        Integer machineID;
        Integer globalID;
        Integer clientID;
        Integer serviceID;
        ArrayList<VM> vms;
        for (CSVRecord record : parser) {
            // SKIP THE HEADER IF IT HAS ONE
            if (record.get(0).equals("Machine ID")) {
                continue;
            }

            if (record.size() != 4) { throw new IllegalArgumentException(
                    "Record is not formatted in the proper CSV format!"); }

            machineID = Integer.decode(record.get(0));
            globalID = Integer.decode(record.get(1));
            clientID = Integer.decode(record.get(2));
            serviceID = Integer.decode(record.get(3));
            vms = flexiblePlacement.getOrDefault(machineID, new ArrayList<VM>());
            vms.add(new VM(clientID, serviceID, globalID));
            flexiblePlacement.put(machineID, vms);
        }

        if (logger.isLoggable(Level.FINE)) {
            logger.fine("flexiblePlacement: " + flexiblePlacement);
        }

        Integer numberOfMachines = flexiblePlacement.size();
        Integer maxSlots = 0;
        for (Integer mID : flexiblePlacement.keySet()) {
            if (maxSlots < flexiblePlacement.get(mID).size()) {
                maxSlots = flexiblePlacement.get(mID).size();
            }
        }

        PlacementMap place = new PlacementMap(numberOfMachines, maxSlots);

        for (Integer mID : flexiblePlacement.keySet()) {
            for (VM vm : flexiblePlacement.get(mID)) {
                place.addNewVMToMachine(vm, mID);
            }
        }

        // Update the settings file
        VagabondSettings s = VagabondSettings.getInstance();
        s.numberOfClients = place.getNumberOfClients();
        s.numberOfMachines = place.getNumberOfMachines();
        s.numberOfMachineSlots = place.getMaxNumberOfSlotsPerMachine();
        s.numberOfVMsPerClient = place.getMaxNumberOfVMsPerClient();

        return place;
    }

    /**
     * Will write a placement map to the file 'filename', it will create the file and folders if needed.
     * BEWARE: this function will APPEND to files, thus making it hard to read the PlacementMap back in
     * 
     * This function uses the same format that was created by Jonathan Shahen for NOMAD
     * 
     * @param filePath the file to APPEND the placement map to 
     * @param place Placement map that need to be written out
     * @param displayStyle The style of the Placement Map that will be written in 
     * @param comment Places this string with a comment character in front of every new line at the top of the file
     * @return TRUE if it succeeded, FALSE if an error occurred
     */
    public static Boolean writePlacementMap(String filePath, PlacementMap place, PlacementMapDisplayStyle displayStyle,
            String comment) {
        Boolean success = Boolean.FALSE;

        Path fileP = Paths.get(filePath);
        Charset charset = Charset.forName("utf-8");

        try (BufferedWriter writer = Files.newBufferedWriter(fileP, charset)) {
            if (displayStyle != PlacementMapDisplayStyle.CSV) {
                writer.write("//" + comment.replace("\n", "\n// ") + "\n");
            }

            place.writeOut(writer, displayStyle);
        } catch (IOException e) {
            e.printStackTrace();
        }

        return success;
    }

    public static EpochHistory readEpochHistory(File input, int slidingWindow, StatisticsManager stats)
            throws IOException {
        Logger logger = VagabondSettings.getInstance().getLogger();

        if (!input.exists()) { throw new IOException(
                "The filename given does not point to a file that exists! Filename = " + input.getAbsolutePath()); }

        CSVParser parser = CSVParser.parse(input, Charset.defaultCharset(), CSVFormat.RFC4180);

        // Machine ID => VMs
        Hashtable<Integer, ArrayList<VM>> flexiblePlacement = new Hashtable<>();
        Integer machineID;
        Integer globalID;
        Integer clientID;
        Integer serviceID;
        Integer currentEpoch = null;
        ArrayList<VM> vms;
        Hashtable<Integer, PlacementMap> outOfOrderPlacementMaps = new Hashtable<>();
        for (CSVRecord record : parser) {
            // SKIP THE HEADER IF IT HAS ONE
            if (record.get(0).equals("Machine ID")) {
                continue;
            }

            // The start of a new Placement Map
            if (record.get(0).equals("Epoch")) {
                if (currentEpoch != null) {
                    if (outOfOrderPlacementMaps.get(currentEpoch) != null) { throw new IllegalArgumentException(
                            "Two placement maps have the same Epoch Number! " + "epochNumber=" + currentEpoch); }

                    if (logger.isLoggable(Level.FINE)) {
                        logger.fine("flexiblePlacement: " + flexiblePlacement);
                    }
                    PlacementMap place = arrangePlacementMap(flexiblePlacement);
                    outOfOrderPlacementMaps.put(currentEpoch, place);
                    flexiblePlacement.clear();
                }

                currentEpoch = Integer.decode(record.get(1));

                if (stats != null) {
                    stats.addEpochTime(Long.decode(record.get(2)));
                }

                continue;
            }

            // Error Checking
            if (currentEpoch == null) { throw new IllegalArgumentException(
                    "Before a Placement Map starts you need to have a line: \"Epoch,epochNumber\""); }

            if (record.size() != 4) { throw new IllegalArgumentException(
                    "Record is not formatted in the proper CSV format!"); }

            // Decode the VM information
            machineID = Integer.decode(record.get(0));
            globalID = Integer.decode(record.get(1));
            clientID = Integer.decode(record.get(2));
            serviceID = Integer.decode(record.get(3));
            vms = flexiblePlacement.getOrDefault(machineID, new ArrayList<VM>());
            vms.add(new VM(clientID, serviceID, globalID));
            flexiblePlacement.put(machineID, vms);
        }
        // Add the last placement map
        if (logger.isLoggable(Level.FINE)) {
            logger.fine("flexiblePlacement: " + flexiblePlacement);
        }
        PlacementMap place = arrangePlacementMap(flexiblePlacement);
        outOfOrderPlacementMaps.put(currentEpoch, place);

        EpochHistory eh = new EpochHistory(slidingWindow);
        PlacementMap tmp = null;
        for (int i = outOfOrderPlacementMaps.size() - 1; i >= 0; i--) {
            tmp = outOfOrderPlacementMaps.get(i);
            if (tmp == null) { throw new IllegalArgumentException("Missing the Epoch: " + i); }
            eh.add(tmp);

            if (stats != null) {
                // STATS
                stats.listTIL.add(eh.get(0).getSumInformationLeak());
                stats.listMCCIL.add(eh.get(0).getMaxInformationLeak());
                stats.listMovesPerEpoch.add(eh._latestNumberOfMoves);
            }
        }

        // Update the settings file
        VagabondSettings s = VagabondSettings.getInstance();
        s.numberOfClients = tmp.getNumberOfClients();
        s.numberOfMachines = tmp.getNumberOfMachines();
        s.numberOfMachineSlots = tmp.getMaxNumberOfSlotsPerMachine();
        s.numberOfVMsPerClient = tmp.getMaxNumberOfVMsPerClient();

        return eh;
    }

}
