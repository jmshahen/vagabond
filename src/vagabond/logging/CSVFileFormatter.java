package vagabond.logging;

import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.logging.*;

import org.apache.commons.lang3.StringEscapeUtils;

/**
 * Log Formatter to format log messages to CSV format
 * @author Jonathan Shahen
 *
 */
public class CSVFileFormatter extends Formatter {
    public final static Logger logger = Logger.getLogger("mohawk");
    private static final DateFormat df = new SimpleDateFormat("yyyy/MM/dd HH:mm:ss.SSSZ");

    public String format(LogRecord record) {
        StringBuilder builder = new StringBuilder(1000);
        builder.append(df.format(new Date(record.getMillis()))).append(",");
        builder.append(record.getSourceClassName()).append(",");
        builder.append(record.getSourceMethodName()).append(",");
        builder.append(record.getLevel()).append(",");
        builder.append(StringEscapeUtils.escapeCsv(formatMessage(record)));
        builder.append("\n");
        return builder.toString();
    }

    public static String csvHeaders() {
        StringBuilder builder = new StringBuilder(100);
        builder.append("Datetime").append(",");
        builder.append("Classname").append(",");
        builder.append("Method").append(",");
        builder.append("Level").append(",");
        builder.append("Message");
        builder.append("\n");
        return builder.toString();
    }

    public String getHead(Handler h) {
        return super.getHead(h);
    }

    public String getTail(Handler h) {
        return super.getTail(h);
    }
}
