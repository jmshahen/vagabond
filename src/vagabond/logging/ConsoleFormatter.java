package vagabond.logging;

import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.logging.*;

import org.apache.commons.lang3.text.WordUtils;

/**
 * Log Formatter to print log messages to the terminal
 * @author Jonathan Shahen
 *
 */
public class ConsoleFormatter extends Formatter {
    public final static Logger logger = Logger.getLogger("mohawk");
    private static final DateFormat df = new SimpleDateFormat("yyyy/MM/dd HH:mm:ss.SSSZ");
    public Integer padLeft = Level.WARNING.toString().length() + 1;
    public Integer maxWidth = 0;
    public String newLineStr = "\n    ";

    public String format(LogRecord record) {
        StringBuilder builder = new StringBuilder(1000);
        // Date Time
        builder.append(df.format(new Date(record.getMillis()))).append(" - ");
        // Class Name
        builder.append("[").append(record.getSourceClassName()).append(".");
        // Method Name
        builder.append(record.getSourceMethodName()).append(" #");
        // Thread ID
        builder.append(record.getThreadID()).append("]:\n");
        // Logging Level
        String signifcantLevel = "";
        char padChar = ' ';
        if (record.getLevel() == Level.WARNING || record.getLevel() == Level.SEVERE) {
            signifcantLevel = "";
            padChar = '*';
        }
        builder.append("[");
        builder.append(String.format("%1$" + signifcantLevel + padLeft + "s", record.getLevel()).replace(' ', padChar));
        builder.append("] ");
        // Message
        builder.append(formatMessage(record));
        builder.append("\n"); // End of Message
        if (maxWidth == 0) {
            return builder.toString();
        } else {
            return WordUtils.wrap(builder.toString(), maxWidth, newLineStr, true);
        }
    }

    /**
     * Get the current line number.
     * 
     * @return int - Current line number.
     */
    public static int getLineNumber() {
        return Thread.currentThread().getStackTrace()[2].getLineNumber();
    }

    public String getHead(Handler h) {
        return super.getHead(h);
    }

    public String getTail(Handler h) {
        return super.getTail(h);
    }
}
