/**
 *
 * Provides the classes necessary for logging to the Console and to the Filesystem.
 * <p>
 * Vagabond is a complex program and debugging the code is made easier with a extensive logging platform. 
 * All classes related to logging are stored in this package.
 */
package vagabond.logging;