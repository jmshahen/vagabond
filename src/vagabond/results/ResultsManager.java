package vagabond.results;

import java.io.FileWriter;
import java.io.IOException;

import vagabond.enums.PlacementMapDisplayStyle;
import vagabond.pieces.*;
import vagabond.singleton.VagabondSettings;

public class ResultsManager {
    public static String separator = "\n\n###############################\n\n";

    public PlacementMap initialPlacement = null;
    public PlacementMap finalPlacement = null;

    public ClientToClientInformationLeakage initialCCIL = null;
    public ClientToClientInformationLeakage finalCCIL = null;

    public EpochHistory eh = null;

    public boolean writeOut(String filePath) throws IOException {
        boolean result = true;

        VagabondSettings settings = VagabondSettings.getInstance();

        FileWriter fw = new FileWriter(filePath);

        fw.write("Settings:\n");
        fw.write(settings.toString().replace(", ", "\n").replace("{", "{\n"));

        fw.write(separator);

        fw.write("Initial Placement:\n");
        fw.write(initialPlacement.getFormatString(PlacementMapDisplayStyle.VAGABOND));

        fw.write(separator);

        fw.write("Initial Client-To-Client Information Leakage:\n");
        fw.write("Max Information Leak of: " + initialCCIL.getMaxInformationLeak() + "\n");
        fw.write("Sum Information Leak of: " + initialCCIL.getSumInformationLeak() + "\n");
        fw.write(initialCCIL.getFormatString(PlacementMapDisplayStyle.VAGABOND, 0));

        fw.write(separator);

        fw.write("Final Placement:\n");
        fw.write(finalPlacement.getFormatString(PlacementMapDisplayStyle.VAGABOND));

        fw.write(separator);

        fw.write("Final Client-To-Client Information Leakage:\n");
        fw.write("Max Information Leak of: " + finalCCIL.getMaxInformationLeak() + "\n");
        fw.write("Sum Information Leak of: " + finalCCIL.getSumInformationLeak() + "\n");
        fw.write(finalCCIL.getFormatString(PlacementMapDisplayStyle.VAGABOND, 0));

        fw.write(separator);

        fw.write("Summation of Client-To-Client Information Leakage over a Sliding Window of " + eh.size() + "\n");
        fw.write("Max Information Leak of: " + eh._sumOfInformationLeakage.getMaxInformationLeak() + "\n");
        fw.write("Do Nothing Max Information Leak of: " + (eh.size() * initialCCIL.getMaxInformationLeak()) + "\n");
        fw.write("Sum Information Leak of: " + eh._sumOfInformationLeakage.getSumInformationLeak() + "\n");
        fw.write("Do Nothing sum Information Leak of: " + (eh.size() * initialCCIL.getSumInformationLeak()) + "\n");
        fw.write(eh._sumOfInformationLeakage.getFormatString(PlacementMapDisplayStyle.VAGABOND, 0));

        fw.close();

        return result;
    }
}
