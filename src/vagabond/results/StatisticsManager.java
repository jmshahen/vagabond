package vagabond.results;

import java.io.*;
import java.util.ArrayList;

import vagabond.pieces.*;
import vagabond.singleton.VagabondSettings;

public class StatisticsManager {
    public String startDTStr;
    public String testName = "";

    public PlacementMap initialPM;
    public ClientToClientInformationLeakage initialCCIL;
    public PlacementMap finalPM;
    public ClientToClientInformationLeakage finalCCIL;
    public EpochHistory eh;

    public ArrayList<Long> epochTimes = new ArrayList<>();
    public ArrayList<Integer> listTIL = new ArrayList<>();
    public ArrayList<Integer> listMCCIL = new ArrayList<>();
    public ArrayList<Integer> listMovesPerEpoch = new ArrayList<>();

    public Long totalTime = null;

    public String comments = "";

    // Calculated
    private Long avgEpochTime = null;

    public StatisticsManager() {
        startDTStr = VagabondSettings.getCurrentDateTimeStr();
    }

    public void writeOut(File statsFile) throws IOException {
        boolean writeHeader = false;
        if (!statsFile.exists()) {
            writeHeader = true;
        }
        FileWriter stats = new FileWriter(statsFile, true); // NEVER OVERWRITE STATS!

        if (writeHeader) {
            String header = getCSVHeader();
            // System.out.println(header);

            stats.write(header);
        }
        String line = getCSVStatsLine();
        // System.out.println(line);

        stats.write(line);

        stats.close();
    }

    public String getCSVHeader() {
        StringBuilder sb = new StringBuilder(1000);

        sb.append("Date Time,");
        sb.append("Test Name,");
        // Metrics Describing the Setup
        sb.append("Machines,");
        sb.append("Clients,");
        sb.append("Slots,");
        sb.append("Max VMs/Client,");
        sb.append("Min VMs/Client,");
        sb.append("Placement,"); // the name and a descriptive value to individualize
        sb.append("Total VMs,");
        sb.append("MigBudget,");
        sb.append("% Filled,");
        sb.append("Total Slots,");
        sb.append("Window,");
        sb.append("Seed,");
        sb.append("# Epochs,");
        sb.append("Reduction,");
        sb.append("Objective,");
        // Metrics describing the Performance or Optimality
        sb.append("Avg Epoch Time(ms),");
        sb.append("Initial TIL,"); // Total Information Leakage (initial Placement)
        sb.append("Final TIL,"); // Total Information Leakage (final placement)
        sb.append("Initial MCCIL,");
        sb.append("Final MCCIL,");
        sb.append("TIL Gain,");
        sb.append("MCCIL Gain,");
        sb.append("TIL Epochs Changed,");
        sb.append("MCCIL Epochs Changed,");
        // Lists (raw Data)
        sb.append("List of Epoch Times,");
        sb.append("Distribution of initial CCIL,");
        sb.append("Distribution of final CCIL,");
        sb.append("List TIL,");
        sb.append("List MCCIL,");
        sb.append("List of # Moves per Epoch,");
        sb.append("Last Epoch with Move,");
        sb.append("Total Test Time(ms),");

        sb.append("Comments\n");

        return sb.toString();
    }

    public String getCSVStatsLine() {
        StringBuilder sb = new StringBuilder(3000);

        VagabondSettings settings = VagabondSettings.getInstance();

        // sb.append("Date Time,");
        sb.append(startDTStr).append(",");
        // sb.append("Test Name,");
        sb.append(testName).append(",");

        /**
         * Metrics Describing the Setup
         */
        // sb.append("Machines,");
        sb.append(settings.numberOfMachines).append(",");
        // sb.append("Clients,");
        sb.append(settings.numberOfClients).append(",");
        // sb.append("Slots,");
        sb.append(settings.numberOfMachineSlots).append(",");
        // sb.append("Max VMs/Client,");
        sb.append(initialPM.getMaxNumberOfVMsPerClient()).append(",");
        // sb.append("Min VMs/Client,");
        sb.append(initialPM.getMinNumberOfVMsPerClient()).append(",");
        // sb.append("Placement,"); // the name and a descriptive value to individualize
        sb.append(settings.getInformativePlacement()).append(",");
        // sb.append("Total VMs,");
        sb.append(initialPM.getNumberOfVMs()).append(",");
        // sb.append("MigBudget,");
        sb.append(settings.migrationBudget).append(",");
        // sb.append("% Filled,");
        sb.append(initialPM.percentFilled()).append(",");
        // sb.append("Total Slots,");
        sb.append(initialPM.getNumberOfTotalSlots()).append(",");
        // sb.append("Window,");
        sb.append(settings.slidingWindow).append(",");
        // sb.append("Seed,");
        sb.append(settings.randomSeed).append(",");
        // sb.append("# Epochs,");
        sb.append(settings.numberOfEpochs).append(",");
        // sb.append("Reduction,");
        sb.append(settings.reductionAlgorithm).append(",");
        // sb.append("Objective,");
        sb.append(settings.getObjectiveStr()).append(",");

        /**
         * Metrics describing the Performance or Optimality
         */
        // sb.append("Avg Epoch Time,");
        sb.append(getAvgEpochTime()).append(",");
        // sb.append("Initial TIL,");
        sb.append(listTIL.get(0)).append(",");
        // sb.append("Final TIL,");
        sb.append(listTIL.get(listTIL.size() - 1)).append(",");
        // sb.append("Initial MCCIL,");
        sb.append(listMCCIL.get(0)).append(",");
        // sb.append("Final MCCIL,");
        sb.append(listMCCIL.get(listMCCIL.size() - 1)).append(",");
        // sb.append("TIL Gain,");
        sb.append(listTIL.get(0) - listTIL.get(listTIL.size() - 1)).append(",");
        // sb.append("MCCIL Gain,");
        sb.append(listMCCIL.get(0) - listMCCIL.get(listMCCIL.size() - 1)).append(",");
        // sb.append("TIL Epochs Changed,");
        sb.append(traceBackList(listTIL, null)).append(",");
        // sb.append("MCCIL Epochs Changed,");
        sb.append(traceBackList(listMCCIL, null)).append(",");

        /**
         * Lists (raw Data)
         */
        // sb.append("List of Epoch Times,");
        sb.append(getListStr(epochTimes, ";")).append(",");
        // sb.append("Distribution of initial CCIL,");
        int[] initialCCILDist = getDistribution(initialCCIL, listMCCIL.get(0));
        sb.append(getArrayStr(initialCCILDist, ";")).append(",");
        // sb.append("Distribution of final CCIL,");
        int[] finalCCILDist = getDistribution(finalCCIL, listMCCIL.get(listMCCIL.size() - 1));
        sb.append(getArrayStr(finalCCILDist, ";")).append(",");
        // sb.append("List TIL,");
        sb.append(getListStr(listTIL, ";")).append(",");
        // sb.append("List MCCIL,");
        sb.append(getListStr(listMCCIL, ";")).append(",");
        // sb.append("List of # Moves per Epoch,");
        sb.append(getListStr(listMovesPerEpoch, ";")).append(",");
        // sb.append("Last Epoch with Move,");
        sb.append(traceBackList(listMovesPerEpoch, 0)).append(",");
        // sb.append("Total Test Time(ms),");
        sb.append(totalTime).append(",");

        // sb.append("Comments");
        sb.append(comments).append("\n");

        return sb.toString();
    }

    public void addEpochTime(Long epochTime) {
        epochTimes.add(epochTime);
    }

    public Long getAvgEpochTime() {
        return getAvgEpochTime(false);
    }

    public Long getAvgEpochTime(boolean recalculate) {
        if (avgEpochTime == null || recalculate) {
            Long sum = (long) 0;
            for (Long epochTime : epochTimes) {
                sum += epochTime;
            }
            avgEpochTime = (long) ((double) sum) / epochTimes.size();
        }
        return avgEpochTime;
    }

    public static int traceBackList(ArrayList<Integer> list, Integer value) {
        for (int i = list.size() - 1; i >= 0; i--) {
            if (value != null) {
                if (!list.get(i).equals(value)) { return i + 1; }
            } else {
                if (i == list.size() - 1) {
                    continue;
                }

                // Check with previous value
                if (!list.get(i).equals(list.get(i + 1))) { return i + 1; }
            }
        }
        return 0;
    }

    public static String getArrayStr(int[] arr, String delim) {
        StringBuilder sb = new StringBuilder(arr.length * 10);

        for (int i = 0; i < arr.length; i++) {
            if (i != 0) {
                sb.append(delim);
            }
            sb.append(i).append("=").append(arr[i]);
        }

        return sb.toString();
    }

    public static int[] getDistribution(ClientToClientInformationLeakage ccil, int maxVal) {
        int[] rtn = new int[maxVal + 1]; // initialized to 0

        for (Integer c0 : ccil._clientIDs) {
            for (Integer c1 : ccil._clientIDs) {
                // DO NOT count the information leakage from a client to itself
                if (c0.equals(c1)) {
                    continue;
                }

                rtn[ccil.get(c0, c1)]++;
            }
        }

        return rtn;
    }

    public static String getListStr(ArrayList<?> list, String delim) {
        StringBuilder sb = new StringBuilder(list.size() * 10);

        for (int i = 0; i < list.size(); i++) {
            if (i != 0) {
                sb.append(delim);
            }
            sb.append(list.get(i).toString());
        }

        return sb.toString();
    }
}
