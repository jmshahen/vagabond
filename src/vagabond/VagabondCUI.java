package vagabond;

import java.io.*;
import java.util.*;

import org.apache.commons.cli.Options;

/**
 * A Console User Interface for Vagabond. If you run this script you will be given the Vagabond help, 
 *  a list of common commands, your previous command, and an interface to enter a custom command to Vagabond.
 * @author Jonathan Shahen
 *
 */
public class VagabondCUI {
    public static String previousCommandFilename = "CUIPreviousCommand.txt";
    public static String previousCmd;

    /**
     * The main program, run this and get a Console User Interface. All arguments are ignored. 
     * @param args Ignored!
     */
    public static void main(String[] args) {
        VagabondInstance inst = new VagabondInstance();
        ArrayList<String> argv = new ArrayList<String>();
        String cmd = "";
        Scanner user_input = new Scanner(System.in);

        Options options = new Options();
        inst.setupOptions(options);
        inst.printHelp(options, 120);

        printCommonCommands();

        System.out.print("Enter Commandline Argument ('!e' to run): ");
        String quotedStr = "";
        StringBuilder fullCommand = new StringBuilder();
        while (true) {
            cmd = user_input.next();
            fullCommand.append(cmd + " ");

            if (quotedStr.isEmpty() && cmd.startsWith("\"")) {
                System.out.println("Starting: " + cmd);
                quotedStr = cmd.substring(1);
                continue;
            }
            if (!quotedStr.isEmpty() && cmd.endsWith("\"")) {
                System.out.println("Ending: " + cmd);
                argv.add(quotedStr + " " + cmd.substring(0, cmd.length() - 1));
                quotedStr = "";
                continue;
            }

            if (!quotedStr.isEmpty()) {
                quotedStr = quotedStr + " " + cmd;
                continue;
            }

            if (cmd.equals("!e")) {
                break;
            }

            if (cmd.equals("!p")) {
                argv.clear();
                argv.addAll(Arrays.asList(previousCmd.split(" ")));
                break;
            }

            argv.add(cmd);
        }
        user_input.close();

        System.out.println("Commands: " + argv);

        try {
            if (!cmd.equals("!p")) {
                FileWriter fw;
                fw = new FileWriter(previousCommandFilename, false);
                fw.write(fullCommand.toString());
                fw.close();
            }
        } catch (IOException e) {
            System.out.println("[ERROR] Unable to write out previous command to: " + previousCommandFilename);
        }

        inst.run(argv.toArray(new String[0]));
    }

    /**
     * Prints out a list of common commands that the developer thought would be convenient to copy and 
     * paste instead of typing.
     */
    public static void printCommonCommands() {
        System.out.println("\n\n--- Common Commands ---");

        System.out.println("-loglevel verbose -settings tests/simpletestILP-Random.properties !e");
        System.out.println("-loglevel verbose -settings tests/simpletestILP-NomadSubOptimal.properties !e");
        System.out.println("-loglevel verbose -settings tests/equalSpread_10.properties !e");
        System.out.println("");
        System.out.println("-loglevel verbose -settings tests/simpletestILP-NomadSubOptimal.properties !e");
        System.out.println("-noplacementmaps -loglevel verbose "
                + "-settings tests/simpletestILP-NomadSubOptimal.properties !e");
        System.out.println("-loglevel verbose -settings tests/simpletestILPNoMigrationBudget.properties !e");
        System.out.println("-loglevel verbose -settings tests/simpletestILPLargeTestNoMig.properties !e");
        System.out.println("");
        System.out.println("-loglevel verbose -settings tests/simpletestCNF.properties !e");
        System.out.println("-loglevel verbose -settings tests/simpletestCNFNoMigrationBudget.properties !e");
        System.out.println("");
        System.out.println("-loglevel verbose -settings tests/simpletestNomad.properties !e");
        System.out.println("-loglevel verbose -settings tests/simpletestNomadNoMigrationBudget.properties !e");
        System.out.println("");
        System.out.println("-loglevel quiet -settings tests/test.properties !e");
        System.out.println("");
        try {
            BufferedReader bfr = new BufferedReader(new FileReader(previousCommandFilename));
            previousCmd = bfr.readLine();
            bfr.close();
            System.out.println("Previous Command ('!p' to use the previous command): " + previousCmd);
        } catch (IOException e) {
            System.out.println("[ERROR] Unable to load previous command!");
        }
    }
}
