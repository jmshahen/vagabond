package vagabond.timing;

import java.io.*;
import java.util.Hashtable;
import java.util.Map;
import java.util.logging.Logger;

import org.apache.commons.lang3.StringEscapeUtils;
import org.apache.commons.lang3.StringUtils;

/**
 * A class to make timing sections of code easier to do.
 * @author Jonathan Shahen
 *
 */
public class TimingManager {
    public final static Logger logger = Logger.getGlobal();
    private Map<String, TimingEvent> timings = new Hashtable<String, TimingEvent>();
    private String lastFinishedTimer = null;
    /**
     * If TRUE then a message will be printed to the console when {@link #stopTimer(String)} is called; 
     * else no message is printed
     */
    public Boolean printOnStop = false;

    public String[] heading = new String[] { "Category", "Start Time", "Start Milliseconds", "Finish Time",
            "Finish Milliseconds", "Timer Key", "Duration (ms)", "Comment" };

    public Map<String, TimingEvent> getTimings() {
        return timings;
    }

    public void blankTimer(String key) {
        timings.put(key, new TimingEvent());
    }

    public void startTimer(String key) {
        TimingEvent t = timings.get(key);
        if (t == null) {
            t = new TimingEvent();
        }
        t.setStartTimeNow();
        timings.put(key, t);
    }

    public Long stopTimer(String key) {
        TimingEvent t = timings.get(key);
        if (t != null) {
            t.setFinishTimeNow();
            timings.put(key, t);
            lastFinishedTimer = key;

            if (printOnStop) {
                System.out.println("[TIMER STOPPED] " + key + ": " + t.duration() + " ms");
            }

            return t.duration();
        }
        return null;
    }

    /**
     * If the key is in the system it will stop the timer, if the key is not in the system it will start a new timer.
     * <br>
     * <pre>
     * {@code
     * TimingManager timing = new TimingManager();
     * timing.toggle("key1");
     * Thread.sleep(4000);
     * timing.toggle("key1");
     * timing.toggle("key2");
     * timing.toggle("key3");
     * Thread.sleep(2000);
     * timing.toggle("key3");
     * Thread.sleep(3000);
     * timing.toggle("key2");
     * } 
     * </pre>
     * The above will have the result of:
     * <pre>
     * key1 = 4 sec
     * key2 = 5 sec
     * key3 = 2 sec
     * </pre>
     * @param key the timer key to start or stop
     */
    public void toggleTimer(String key) {
        TimingEvent t = timings.get(key);

        if (t == null) {
            startTimer(key);
        } else {
            stopTimer(key);
        }
    }

    public String getLastFinishedTimer() {
        return lastFinishedTimer;
    }

    /**
     * Used to indicate that the operation failed after a certain amount of time by calculating the time difference and
     * then multiplying by -1. If the elapsed time is zero then the time will be changed to -1.
     * 
     * @param key The timing key you want to cancel
     * @return TRUE if the key was found; FALSE otherwise
     */
    public Boolean cancelTimer(String key) {
        TimingEvent t = timings.get(key);
        if (t != null) {
            t.setFinishTimeNow();
            t.failed();
            timings.put(key, t);
            lastFinishedTimer = key;

            if (printOnStop) {
                System.out.println(key + ": " + t.duration() + " ms");
            }

            return true;
        }
        return false;
    }

    public void removeTimer(String key) {
        timings.remove(key);
    }

    public Boolean writeOut(File results) throws IOException {
        return writeOut(results, false);
    }

    public Boolean writeOut(File results, Boolean forceOverwrite) throws IOException {
        Boolean writeHeader = forceOverwrite;
        if (!results.exists()) {
            if (!results.createNewFile()) {
                logger.warning("[TIMING] Unable to create the file: " + results.getAbsolutePath());
                return false;
            }
            writeHeader = true;
        }

        if (!results.isFile()) {
            logger.warning("[TIMING] Parameter pointing to something that is not a file: " + results.getAbsolutePath());
            return false;
        }

        FileWriter fw = new FileWriter(results, !forceOverwrite);

        if (writeHeader) {
            writeHeader(fw);
        }

        for (Map.Entry<String, TimingEvent> entry : timings.entrySet()) {
            if (writeOutSingle(fw, entry.getKey()) == false) {
                logger.warning("[TIMING] Unable to write the row: " + entry.toString());
                return false;
            }
        }
        fw.close();

        return true;
    }

    public Boolean writeOutLast(FileWriter fw) throws IOException {
        return writeOutSingle(fw, lastFinishedTimer);
    }

    public Boolean writeOutSingle(FileWriter fw, String key) throws IOException {
        TimingEvent value = timings.get(key);

        if (value == null) { return false; }

        try {
            fw.write(value.category + "," + value.startTime + "," + value.startTimeMilliSec + "," + value.finishTime
                    + "," + value.finishTimeMilliSec + "," + StringEscapeUtils.escapeCsv(key) + "," + value.duration()
                    + "," + value.comment + "\n");
        } catch (NullPointerException e) {
            System.out.println("[ERROR] NullPointer when trying to access: " + key);
            logger.severe("[ERROR] NullPointer when trying to access: " + key);
        }
        fw.flush(); // flushes just in-case this function is run once in a while!
        return true;
    }

    public void writeHeader(FileWriter fw) throws IOException {
        fw.write(StringUtils.join(heading, ",") + "\n");
    }

    public void writeHeaderHumanReadable(FileWriter fw) throws IOException {
        fw.write("Key=duration | Comment\n");
    }

    public Boolean writeOutHumanReadable(File results) throws IOException {
        return writeOutHumanReadable(results, false);
    }

    public Boolean writeOutHumanReadable(File results, Boolean forceOverwrite) throws IOException {
        Boolean writeHeader = forceOverwrite;
        if (!results.exists()) {
            if (!results.createNewFile()) {
                logger.warning("[TIMING] Unable to create the file: " + results.getAbsolutePath());
                return false;
            }
            writeHeader = true;
        }

        if (!results.isFile()) {
            logger.warning("[TIMING] Parameter pointing to something that is not a file: " + results.getAbsolutePath());
            return false;
        }

        FileWriter fw = new FileWriter(results, !forceOverwrite);

        if (writeHeader) {
            writeHeaderHumanReadable(fw);
        }

        for (Map.Entry<String, TimingEvent> entry : timings.entrySet()) {
            if (writeOutSingleHumanReadable(fw, entry.getKey()) == false) {
                logger.warning("[TIMING] Unable to write the row: " + entry.toString());
                return false;
            }
        }
        fw.close();

        return true;
    }

    public Boolean writeOutLastHumanReadable(FileWriter fw) throws IOException {
        return writeOutSingleHumanReadable(fw, lastFinishedTimer);
    }

    public Boolean writeOutSingleHumanReadable(FileWriter fw, String key) throws IOException {
        TimingEvent value = timings.get(key);

        if (value == null) { return false; }

        try {
            fw.write(StringEscapeUtils.escapeCsv(key) + "=" + value.duration() + " | " + value.comment + "\n");
        } catch (NullPointerException e) {
            System.out.println("[ERROR] NullPointer when trying to access: " + key);
            logger.severe("[ERROR] NullPointer when trying to access: " + key);
        }
        fw.flush(); // flushes just in-case this function is run once in a while!
        return true;
    }

    @Override
    public String toString() {
        StringBuilder t = new StringBuilder();
        int i = 0;
        String s;
        for (Map.Entry<String, TimingEvent> entry : timings.entrySet()) {
            t.append(entry.getKey() + ": ");
            try {
                s = entry.getValue().toString();
            } catch (NullPointerException e) {
                s = "Not Finished";
            }
            t.append(s);

            i++;
            if (i != timings.size()) {
                t.append(" - ");
            }
        }
        return t.toString();
    }

    /**
     * Returns the Elapsed Time in Seconds (with decimal places)
     * @return elapsed time in seconds
     */
    public Double getLastElapsedTimeSec() {
        TimingEvent te = timings.get(getLastFinishedTimer());

        return te.durationSec();
    }

    /**
     * Return the Elapsed Time in Milliseconds
     * @return elapsed time in milliseconds
     */
    public Long getLastElapsedTime() {
        TimingEvent te = timings.get(getLastFinishedTimer());

        return te.duration();
    }

    /**
     * Allows for complete TimeEvents to be put in with the other timing events 
     * @param key the key to associate this timing event with
     * @param event the completed timing event; WARNING: "completed" is not enforced!
     */
    public void putRawTimeEvent(String key, TimingEvent event) {
        if (key == null || event == null) { throw new IllegalArgumentException("No inputs are allowed to be null"); }

        if (timings.get(key) != null) {
            logger.severe("That key is already in use! key=" + key);
            throw new IllegalArgumentException("Key is already in use");
        }

        timings.put(key, event);
    }
}
