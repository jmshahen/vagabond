/**
 *
 * A timing manger to keep track of how long certain tasks take; programmer controls when the event recording starts 
 * and stops.
 */
package vagabond.timing;