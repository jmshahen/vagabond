package vagabond.timing.tests;

import org.junit.AfterClass;
import org.junit.Test;

import vagabond.pieces.PlacementMap;
import vagabond.pieces.SumOfClientVMsPerMachine;
import vagabond.placement.RandomPlacement;
import vagabond.timing.TimingManager;

public class PerformanceTests {
    public static TimingManager timing = new TimingManager();

    private static final long MEGABYTE = 1024L * 1024L;

    public static long bytesToMegabytes(long bytes) {
        return bytes / MEGABYTE;
    }

    @Test
    public void largeRandomPlacementMap() {
        String key = "largeRandomPlacementMap::Generate250000VMs";

        Runtime runtime = Runtime.getRuntime();
        RandomPlacement rp = new RandomPlacement();

        long initMem = runtime.totalMemory() - runtime.freeMemory();
        System.out.println("Initial Memory used: " + bytesToMegabytes(initMem) + " MB");

        timing.startTimer(key);
        PlacementMap test = rp.generatePlacementMap(5000, 4, 5000, 2, false);
        timing.stopTimer(key);

        long placeMem = (runtime.totalMemory() - runtime.freeMemory()) - initMem;
        System.out.println("PlacementMap Memory used: " + bytesToMegabytes(placeMem) + " MB");

        key = "largeRandomPlacementMap::SumOfClientVMsPerMachine";
        timing.startTimer(key);
        SumOfClientVMsPerMachine t = new SumOfClientVMsPerMachine(test);
        timing.stopTimer(key);

        long sumMem = (runtime.totalMemory() - runtime.freeMemory()) - placeMem;
        System.out.println("SumOfClientVMsPerMachine Memory used: " + bytesToMegabytes(sumMem) + " MB");

        PlacementMap.TOSTRING_MAXMACHINES = 5;
        SumOfClientVMsPerMachine.TOSTRING_LIMIT = 5;

        System.out.println("Stats: " + test.toString());
        System.out.println("Stats: " + t.toString());
    }

    @AfterClass
    public static void cleanUp() {
        System.out.println(timing.toString());
    }
}
