package vagabond;

import java.io.*;
import java.util.ArrayList;
import java.util.Properties;
import java.util.logging.Level;
import java.util.logging.Logger;

import org.apache.commons.cli.*;
import org.apache.commons.lang3.StringUtils;

import vagabond.helper.ConvertBooleanWithException;
import vagabond.helper.ErrorPrinter;
import vagabond.logging.CSVFileFormatter;
import vagabond.logging.ConsoleFormatter;
import vagabond.pieces.EpochHistory;
import vagabond.pieces.PlacementMap;
import vagabond.placement.*;
import vagabond.reduction.ReduceTo;
import vagabond.reduction.RunSolver;
import vagabond.reduction.cnfsat.ReduceToCNFSAT;
import vagabond.reduction.cnfsat.RunSolverCNFSAT;
import vagabond.reduction.ilp.ReduceToILP;
import vagabond.reduction.ilp.RunSolverILP;
import vagabond.reduction.nomad.ReduceToNomad;
import vagabond.reduction.nomad.RunSolverNomad;
import vagabond.results.StatisticsManager;
import vagabond.singleton.VagabondSettings;
import vagabond.timing.TimingManager;

/**
 * This is an instance of Vagabond, which means that other programs can call Vagabond and it can even be run in 
 * parallel. 
 * @author Jonathan Shahen
 *
 */
public class VagabondInstance {
    private static final String VERSION = "v1.0.1";
    private static final String AUTHORS = "Jonathan Shahen <jmshahen@uwaterloo.ca>";
    // Logger Fields
    public static Logger logger = null;
    /**
     * Shortcut to make for shorter code
     */
    public static final Level FINE = Level.FINE;

    // Timer
    public TimingManager timing = null;

    public StatisticsManager stats = null;

    // Helpers
    private VagabondSettings settings = VagabondSettings.getInstance();

    /**
     * The main function of this class. It takes in commandline arguments (formatted like Java does) and runs 
     * without any help. Call this function with the proper parameters and it runs on its own
     * @param args formatted in the standard Java way for commandline arguments
     * @return 0 if successful, -1 if an error occurred (check the log file for what the error was)
     */
    public int run(String[] args) {
        try {
            CommandLine cmd = init(args);

            // Check to see if the commandline options have been satisfied
            if (cmd == null) { return 0; }

            // Setup the timing manager and start the total time
            timing = settings.timing;
            stats = settings.stats;
            /* Timing */timing.startTimer("totalTime");

            logger.info("Input Settings: " + settings.toString());

            PlacementMap placementMap = null;

            logger.info("Generating Placement Map Using: " + settings.placementAlgorithm);
            RandomPlacement rPlace = new RandomPlacement();
            switch (settings.placementAlgorithm) {
            case PLACEMENT_RANDOM:
                rPlace.randomSeed(settings.randomSeed);

                placementMap = rPlace.generatePlacementMapFromSettings();
                break;
            case PLACEMENT_FILE:
                placementMap = PlacementMapFile.readPlacementMap(settings.inputPlacementMapFilePath);
                break;
            case PLACEMENT_EQUALSPREAD:
                placementMap = PlacementMapExamples.equalSpreadClients(settings.equalSpreadNum);
                break;
            case PLACEMENT_EQUALSPREADHALF:
                placementMap = PlacementMapExamples.equalSpreadClientsHalfFull(settings.equalSpreadNum);
                break;
            case PLACEMENT_EQUALSPREADHALF2:
                placementMap = PlacementMapExamples.equalSpreadClientsHalfFull2(settings.equalSpreadNum);
                break;
            case PLACEMENT_SPECIALTEST1:
                rPlace.randomSeed(settings.randomSeed);
                placementMap = PlacementMapExamples.specialTest1(rPlace, settings.equalSpreadNum);
                break;
            case PLACEMENT_NOMADTEST:
                rPlace.randomSeed(settings.randomSeed);
                placementMap = PlacementMapExamples.nomadTest(rPlace, settings.equalSpreadNum);
                break;
            case PLACEMENT_PERCENTFILL:
                rPlace.randomSeed(settings.randomSeed);
                placementMap = PlacementMapExamples.percentFilled(rPlace, settings.equalSpreadNum);
                break;
            case PLACEMENT_NAHIDTEST:
                placementMap = PlacementMapExamples.nomadSubOptimalRCPlacement();
                break;
            case PLACEMENT_RANDOMHALFFULL:
                rPlace.randomSeed(settings.randomSeed);
                placementMap = PlacementMapExamples.randomHalfFull(rPlace, settings.equalSpreadNum);
                break;

            default:
                logger.severe("No placement algorithm was picked, or one that is not implemented is used. "
                        + "PlacementAlgorithm:" + settings.placementAlgorithm);

                placementMap = null;
            }

            // ERROR CHECKING for creating the Placement Map
            if (placementMap == null) { throw new Exception(
                    "Unable to create a Placement Map with the settings: " + settings.toString()); }

            if (logger.isLoggable(FINE) || settings.displayPlacementMapEveryEpoch) {
                logger.info(placementMap.toString());
            }

            EpochHistory epochHistory = new EpochHistory(settings.slidingWindow);
            // IMPORTANT: add the placementMap to the EpochHistory before starting the reductions
            epochHistory.add(placementMap);

            // STATS
            stats.initialPM = placementMap;
            stats.initialCCIL = epochHistory.get(0);
            stats.listTIL.add(epochHistory.get(0).getSumInformationLeak());
            stats.listMCCIL.add(epochHistory.get(0).getMaxInformationLeak());

            if (logger.isLoggable(FINE)) {
                logger.fine("Summation of Client-to-Client Information Leakage (Z variable):\n"
                        + epochHistory._sumOfInformationLeakage);
            }

            // Results Initial Placement
            settings.results.initialPlacement = placementMap;
            settings.results.initialCCIL = epochHistory.get(0);

            // Reduce the PlacementMap
            ReduceTo reduction = null;
            RunSolver runSolver = null;

            /* Timing */timing.startTimer("VagabondInstance::main()::epochLoop::total");

            /** ******************************************************************************
             *  ******************************************************************************
             *  EPOCH LOOP - START
             *  ******************************************************************************
             */
            String tp;
            for (int epochNum = 0; epochNum < settings.numberOfEpochs; epochNum++) {
                tp = "Epoch_"
                        + StringUtils.leftPad(epochNum + "", (int) Math.floor(Math.log10(settings.numberOfEpochs)) + 1,
                                "0")
                        + "::";
                /* Timing */timing.startTimer(tp + "VagabondInstance::main()::epochLoop::totalTime");
                logger.info("Starting EPOCH " + epochNum + " LOOP");

                switch (settings.reductionAlgorithm) {
                case REDUCTION_SAT:
                    reduction = new ReduceToCNFSAT();
                    runSolver = new RunSolverCNFSAT();
                    break;
                case REDUCTION_ILP:
                    reduction = new ReduceToILP();
                    runSolver = new RunSolverILP();
                    break;
                case REDUCTION_NOMAD:
                    reduction = new ReduceToNomad();
                    runSolver = new RunSolverNomad();
                    break;
                default:
                    logger.severe("No reduction algorithm was picked, or one that is not implemented is used. "
                            + "ReductionAlgorithm:" + settings.reductionAlgorithm);

                    reduction = null;
                }

                // ERROR CHECKING for creating the ReduceTo object
                if (reduction == null) { throw new Exception(
                        "Unable to create a Reduction with the settings: " + settings.toString()); }

                // ERROR CHECKING for creating the SolveTo object
                if (runSolver == null) { throw new Exception(
                        "Unable to create a RunSolver with the settings: " + settings.toString()); }

                // Setup the timer prefix
                reduction.setTimerKeyPrefix(tp);
                runSolver.setTimerKeyPrefix(tp);

                /* Timing */timing.toggleTimer(tp + "VagabondInstance::main()::epochLoop::reduce");
                /**
                 * Reduce the current EpochHistory
                 */
                logger.info("Reducing for EPOCH " + epochNum);
                // ERROR CHECKING if the reduction occurred any problems
                if (reduction.reduce(epochHistory) == false) {
                    String placementMapFilePath = settings.logFolder + File.separator + settings.placementMapFile;

                    logger.severe("Unable to Reduce the PlacementMap with these settings: " + settings.toString());
                    logger.severe("Writing the PlacementMap out to the file: " + placementMapFilePath);

                    PlacementMapFile.writePlacementMap(placementMapFilePath, placementMap,
                            settings.displayStyle, "ERROR: Unable to reduce!");

                    throw new Exception("Unable to reduce with the current PlacementMap");
                }
                /* Timing */timing.toggleTimer(tp + "VagabondInstance::main()::epochLoop::reduce");

                // ##################################################################################

                /* Timing */timing.toggleTimer(tp + "VagabondInstance::main()::epochLoop::load");
                // Load the Solver + Error checking
                logger.info("Loading the Solver for EPOCH " + epochNum);
                if (runSolver.load(reduction) == false) { throw new Exception(
                        "Unable to load the reduction into the runSolver with the reduction: "
                                + reduction.toString()); }
                /* Timing */timing.toggleTimer(tp + "VagabondInstance::main()::epochLoop::load");

                // ##################################################################################

                /* Timing */timing.toggleTimer(tp + "VagabondInstance::main()::epochLoop::run");
                // Run The Solver
                logger.info("Running the Solver for EPOCH " + epochNum);
                EpochHistory newEpochHistory = runSolver.run();
                /* Timing */timing.toggleTimer(tp + "VagabondInstance::main()::epochLoop::run");

                // ##################################################################################

                // ERROR CHECKING for getting the new result
                if (newEpochHistory == null) { throw new Exception(
                        "An error occurred when running the solver: " + runSolver.toString()); }

                // Adding the however many Epochs to the running Epoch History (could be 1 or a whole sliding window)
                epochHistory.add(newEpochHistory);
                if (logger.isLoggable(Level.FINE)) {
                    logger.fine("Summation of Client-to-Client Information Leakage (Z variable):\n"
                            + epochHistory._sumOfInformationLeakage);
                }

                if (settings.displayPlacementMapEveryEpoch) {
                    logger.info(
                            "Placement Map at the END of Epoch " + epochNum + "\n" + epochHistory._latestPlacementMap);
                }

                /* Timing */stats.addEpochTime(timing.stopTimer(tp + "VagabondInstance::main()::epochLoop::totalTime"));

                logger.info("Epoch " + epochNum + " took " + timing.getLastElapsedTimeSec() + " seconds to finish");

                // Must be after the STATS
                if (settings.reductionAlgorithm == VagabondOptionString.REDUCTION_NOMAD) {
                    logger.info("Exiting the EPOCH LOOP because Nomad does its own EPOCH LOOP.");
                    break;
                } else {
                    // STATS
                    stats.listTIL.add(epochHistory.get(0).getSumInformationLeak());
                    stats.listMCCIL.add(epochHistory.get(0).getMaxInformationLeak());
                    stats.listMovesPerEpoch.add(epochHistory._latestNumberOfMoves);
                }
            }
            /** *****************************************************************************
             * ******************************************************************************
             *  EPOCH LOOP - END
             * ******************************************************************************
             */
            /* Timing */timing.stopTimer("VagabondInstance::main()::epochLoop::total");

            /* Timing */stats.totalTime = timing.stopTimer("totalTime");

            logger.info(timing.toString());
            logger.info("Writing Timing Results to " + settings.getTimingFile().getAbsolutePath());
            timing.writeOut(settings.getTimingFile(), true);

            // Fill up the stats manager
            stats.finalPM = epochHistory._latestPlacementMap;
            stats.finalCCIL = epochHistory.get(0);
            stats.eh = epochHistory;

            // Write out all the stats
            try {
                stats.writeOut(settings.getStatsFile());
            } catch (IOException e) {
                logger.severe("Unable to write Stats out to: " + settings.getStatsFile().getAbsolutePath());
                logger.severe(stats.getCSVStatsLine());
            }

            // Results Final Placement
            settings.results.finalPlacement = epochHistory._latestPlacementMap;
            settings.results.finalCCIL = epochHistory.get(0);
            settings.results.eh = epochHistory;

            // Write out the results
            logger.info("Writing Results to " + settings.getResultsFile().getAbsolutePath());
            try {
                settings.results.writeOut(settings.getResultsFile().getAbsolutePath());
            } catch (IOException e) {
                logger.severe("Unable to write Results out to: " + settings.getResultsFile().getAbsolutePath());
                String tmp = settings.getResultsFile().getAbsolutePath() + "." + System.currentTimeMillis() + ".txt";
                logger.severe("Trying again to a random filename: " + tmp);
                settings.results.writeOut(tmp);
            }

        } catch (Exception e) {
            ErrorPrinter ep = new ErrorPrinter(e);
            if (logger != null) {
                logger.severe(ep.toString());
            } else {
                System.out.println(ep.toString());
            }

            // Gracefully shutdown
            settings.shutdown();
            System.exit(-1);
        }

        if (logger != null) {
            logger.info("[EOF] Vagabond Instance done running");
        } else {
            System.out.println("[EOF] Vagabond Instance done running");
        }

        settings.shutdown();

        return 0;
    }

    /**
     * Initialization of Vagabond where all of the settings are read in and verified.
     * @param args standard Java formatted commandline parameters
     * @return the parsed commandline object, or NULL if it has completed all of the actions asked for
     * @throws Exception throws an error if there is a problem with expected values no properly given (check the log)
     */
    private CommandLine init(String[] args) throws Exception {
        Options options = new Options();
        setupOptions(options);

        try {
            CommandLineParser cmdParser = new BasicParser();
            CommandLine cmd = cmdParser.parse(options, args);

            setupLoggerOptions(cmd, options);

            if (setupReturnImmediatelyOptions(cmd, options)) { return null; }

            setupUserPreferenceOptions(cmd, options);
            setupResultOptions(cmd, options);

            setupInputSettings(cmd);

            return cmd;
        } catch (UnrecognizedOptionException e) {
            ErrorPrinter ep = new ErrorPrinter(e);
            System.out.println(ep.toString());

            printHelp(options, 80);
        }

        return null;
    }

    /**
     * Reads in the settings file and parses that file (formatted in standard Java Properties Files) and stores the 
     * settings in the global settings object.
     * @param cmd CommandLine object that holds the parsed command line options
     * @throws IOException is thrown if the settings file does not exist
     */
    private void setupInputSettings(CommandLine cmd) throws IOException {
        if (!cmd.hasOption(VagabondOptionString.SETTINGSFILE.toString())) {
            logger.severe("Must provide a settings file!");

            settings.shutdown();
            System.exit(-1);
            // System.out.println("Do I get run here?");
        }

        InputStream input = null;
        String settingsFile = cmd.getOptionValue(VagabondOptionString.SETTINGSFILE.toString());
        settings.settingsFile = settingsFile;

        try {
            input = new FileInputStream(settingsFile);

            Properties prop = new Properties();
            prop.load(input);

            String value = null;
            ArrayList<String> mustContain = VagabondOptionString.getRequiredControlSettings();

            value = prop.getProperty(VagabondOptionString.EPOCHS.toString(), null);
            if (value != null) {
                mustContain.remove(VagabondOptionString.EPOCHS.toString());
                settings.numberOfEpochs = Integer.valueOf(value);
            }

            value = prop.getProperty(VagabondOptionString.SLIDINGWINDOW.toString(), null);
            if (value != null) {
                mustContain.remove(VagabondOptionString.SLIDINGWINDOW.toString());
                settings.slidingWindow = Integer.valueOf(value);
            }

            value = prop.getProperty(VagabondOptionString.MIGRATIONBUDGET.toString(), null);
            if (value != null) {
                mustContain.remove(VagabondOptionString.MIGRATIONBUDGET.toString());
                settings.migrationBudget = Integer.valueOf(value);
            }

            boolean inputMachinesClientsRequired = false;
            value = prop.getProperty(VagabondOptionString.PLACEMENT.toString(), null);
            if (value != null) {
                mustContain.remove(VagabondOptionString.PLACEMENT.toString());

                if (value.equals(VagabondOptionString.PLACEMENT_RANDOM.toString())) {
                    settings.placementAlgorithm = VagabondOptionString.PLACEMENT_RANDOM;

                    // Required Settings
                    inputMachinesClientsRequired = true;
                } else if (value.equals(VagabondOptionString.PLACEMENT_FILE.toString())) {
                    value = prop.getProperty(VagabondOptionString.PLACEMENTFILEPATH.toString(), null);
                    if (value == null) { throw new IllegalArgumentException("Must provide a placement file path!"); }

                    File f = new File(value);

                    if (!f.exists()) { throw new IllegalArgumentException(
                            "Placement file path must be a valid file path!"); }

                    settings.placementAlgorithm = VagabondOptionString.PLACEMENT_FILE;
                    settings.inputPlacementMapFilePath = value;

                } else if (value.equals(VagabondOptionString.PLACEMENT_EQUALSPREAD.toString())) {
                    value = prop.getProperty(VagabondOptionString.EQUALSPREADNUM.toString(), null);
                    if (value == null) { throw new IllegalArgumentException("Must provide a equalSpreadNum!"); }

                    settings.placementAlgorithm = VagabondOptionString.PLACEMENT_EQUALSPREAD;
                    settings.equalSpreadNum = Integer.valueOf(value);

                } else if (value.equals(VagabondOptionString.PLACEMENT_EQUALSPREADHALF.toString())) {
                    value = prop.getProperty(VagabondOptionString.EQUALSPREADNUM.toString(), null);
                    if (value == null) { throw new IllegalArgumentException("Must provide a equalSpreadNum!"); }

                    settings.placementAlgorithm = VagabondOptionString.PLACEMENT_EQUALSPREADHALF;
                    settings.equalSpreadNum = Integer.valueOf(value);

                } else if (value.equals(VagabondOptionString.PLACEMENT_EQUALSPREADHALF2.toString())) {
                    value = prop.getProperty(VagabondOptionString.EQUALSPREADNUM.toString(), null);
                    if (value == null) { throw new IllegalArgumentException("Must provide a equalSpreadNum!"); }

                    settings.placementAlgorithm = VagabondOptionString.PLACEMENT_EQUALSPREADHALF2;
                    settings.equalSpreadNum = Integer.valueOf(value);

                } else if (value.equals(VagabondOptionString.PLACEMENT_SPECIALTEST1.toString())) {
                    value = prop.getProperty(VagabondOptionString.EQUALSPREADNUM.toString(), null);
                    if (value == null) { throw new IllegalArgumentException("Must provide a equalSpreadNum!"); }

                    settings.placementAlgorithm = VagabondOptionString.PLACEMENT_SPECIALTEST1;
                    settings.equalSpreadNum = Integer.valueOf(value);

                } else if (value.equals(VagabondOptionString.PLACEMENT_NOMADTEST.toString())) {
                    value = prop.getProperty(VagabondOptionString.EQUALSPREADNUM.toString(), null);
                    if (value == null) { throw new IllegalArgumentException("Must provide a equalSpreadNum!"); }

                    settings.placementAlgorithm = VagabondOptionString.PLACEMENT_NOMADTEST;
                    settings.equalSpreadNum = Integer.valueOf(value);

                } else if (value.equals(VagabondOptionString.PLACEMENT_NAHIDTEST.toString())) {
                    value = prop.getProperty(VagabondOptionString.EQUALSPREADNUM.toString(), null);
                    if (value == null) { throw new IllegalArgumentException("Must provide a equalSpreadNum!"); }

                    settings.placementAlgorithm = VagabondOptionString.PLACEMENT_NAHIDTEST;
                    settings.equalSpreadNum = Integer.valueOf(value);

                } else if (value.equals(VagabondOptionString.PLACEMENT_PERCENTFILL.toString())) {
                    value = prop.getProperty(VagabondOptionString.EQUALSPREADNUM.toString(), null);
                    if (value == null) { throw new IllegalArgumentException("Must provide a equalSpreadNum!"); }

                    settings.placementAlgorithm = VagabondOptionString.PLACEMENT_PERCENTFILL;
                    settings.equalSpreadNum = Integer.valueOf(value);

                } else if (value.equals(VagabondOptionString.PLACEMENT_RANDOMHALFFULL.toString())) {
                    value = prop.getProperty(VagabondOptionString.EQUALSPREADNUM.toString(), null);
                    if (value == null) { throw new IllegalArgumentException("Must provide a equalSpreadNum!"); }

                    settings.placementAlgorithm = VagabondOptionString.PLACEMENT_RANDOMHALFFULL;
                    settings.equalSpreadNum = Integer.valueOf(value);

                } else {
                    throw new IllegalArgumentException("Placement Algorithm: " + value + " does not exist!");
                }
            }

            value = prop.getProperty(VagabondOptionString.REDUCTION.toString(), null);
            if (value != null) {
                mustContain.remove(VagabondOptionString.REDUCTION.toString());

                if (value.equals(VagabondOptionString.REDUCTION_SAT.toString())) {
                    settings.reductionAlgorithm = VagabondOptionString.REDUCTION_SAT;
                } else if (value.equals(VagabondOptionString.REDUCTION_ILP.toString())) {
                    settings.reductionAlgorithm = VagabondOptionString.REDUCTION_ILP;
                } else if (value.equals(VagabondOptionString.REDUCTION_NOMAD.toString())) {
                    settings.reductionAlgorithm = VagabondOptionString.REDUCTION_NOMAD;
                } else {
                    throw new IllegalArgumentException("Reduction Algorithm: " + value + " does not exist!");
                }
            }

            // OPTIONAL SETTINGS
            // The optional status of these is dependent on the Placement Algorithm
            if (inputMachinesClientsRequired) {
                value = prop.getProperty(VagabondOptionString.NUMMACHINES.toString(), null);
                if (value != null) {
                    mustContain.remove(VagabondOptionString.NUMMACHINES.toString());
                    settings.numberOfMachines = Integer.valueOf(value);
                }

                value = prop.getProperty(VagabondOptionString.NUMSLOTS.toString(), null);
                if (value != null) {
                    mustContain.remove(VagabondOptionString.NUMSLOTS.toString());
                    settings.numberOfMachineSlots = Integer.valueOf(value);
                }

                value = prop.getProperty(VagabondOptionString.NUMCLIENTS.toString(), null);
                if (value != null) {
                    mustContain.remove(VagabondOptionString.NUMCLIENTS.toString());
                    settings.numberOfClients = Integer.valueOf(value);
                }

                value = prop.getProperty(VagabondOptionString.NUMVMS.toString(), null);
                if (value != null) {
                    mustContain.remove(VagabondOptionString.NUMVMS.toString());
                    settings.numberOfVMsPerClient = Integer.valueOf(value);
                }
            } else {
                mustContain.remove(VagabondOptionString.NUMVMS.toString());
                mustContain.remove(VagabondOptionString.NUMCLIENTS.toString());
                mustContain.remove(VagabondOptionString.NUMSLOTS.toString());
                mustContain.remove(VagabondOptionString.NUMMACHINES.toString());
            }

            value = prop.getProperty(VagabondOptionString.FILLINEMPTY.toString(), null);
            if (value != null) {
                settings.fillInEmptySpots = ConvertBooleanWithException.convert(value);
            }

            value = prop.getProperty(VagabondOptionString.MAXORSUM.toString(), null);
            if (value != null) {
                settings.minimizeMaxClientToClientInfoLeak = ConvertBooleanWithException.convert(value);
            }

            value = prop.getProperty(VagabondOptionString.RANDOMSEED.toString(), null);
            if (value != null) {
                settings.randomSeed = Long.valueOf(value);
            } else {
                settings.randomSeed = System.currentTimeMillis();
            }

            value = prop.getProperty(VagabondOptionString.TESTNAME.toString(), null);
            if (value != null) {
                logger.info("[OPTION] Setting the test name to: " + value);
                settings.stats.testName = value;
            }

            value = prop.getProperty(VagabondOptionString.TESTCOMMENT.toString(), null);
            if (value != null) {
                logger.info("[OPTION] Setting the test comments to: " + value);
                settings.stats.comments = value;
            }

            // ################################################

            // Check for any missing settings values
            if (!mustContain.isEmpty()) { throw new IllegalArgumentException(
                    "The settings file must contain these additional settings: " + mustContain.toString()); }

        } catch (IOException e) {
            ErrorPrinter ep = new ErrorPrinter(e);
            logger.severe(ep.toString());

            throw new IOException("The settings file provided: " + settingsFile + " does not exist!");
        } catch (NumberFormatException e) {
            ErrorPrinter ep = new ErrorPrinter(e);
            logger.severe(ep.toString());

            throw new NumberFormatException("The settings file has an value that cannot be converted to a number!");
        } finally {
            if (input != null) {
                try {
                    input.close();
                } catch (IOException e) {
                    e.printStackTrace();
                }
            }
        }
    }

    /**
     * A way of viewing Vagabonds help message without running the main instance.
     * @param cmd Cannot be null, but can be an empty object.
     * @param options Options object filled by {@link #setupOptions(Options) setupOptions}
     * @throws Exception if the {@link VagabondOptionString#MAXW VagabondOptionString.MAXW} setting is not an integer
     */
    public void printHelp(CommandLine cmd, Options options) throws Exception {
        if (cmd.hasOption(VagabondOptionString.MAXW.toString())) {
            try {
                Integer maxw = Integer.decode(cmd.getOptionValue(VagabondOptionString.MAXW.toString()));
                printHelp(options, maxw);
            } catch (Exception e) {
                printHelp(options, 80);

                e.printStackTrace();
                throw new Exception("An error occured when trying to print out the help options!");
            }
        } else {
            printHelp(options, 80);
        }
    }

    public void printHelp(Options options, int maxw) {
        HelpFormatter f = new HelpFormatter();
        f.printHelp(maxw, "vagabond",
                StringUtils.repeat("-", maxw) + "\nAuthors: " + AUTHORS + "\n" + StringUtils.repeat("-", 20), options,
                StringUtils.repeat("-", maxw), true);
    }

    @SuppressWarnings("static-access")
    public void setupOptions(Options options) {
        // Add Information Options
        options.addOption(VagabondOptionString.HELP.toString(), false, "Print this message");
        options.addOption(VagabondOptionString.AUTHORS.toString(), false, "Prints the authors");
        options.addOption(VagabondOptionString.VERSION.toString(), false,
                "Prints the version (" + VERSION + ") information");
        options.addOption(VagabondOptionString.CHECKMINISAT.toString(), false,
                "Checks that MiniSAT is on the system and displays which version is installed");

        // Add Logging Level Options
        options.addOption(OptionBuilder.withArgName("quiet|debug|verbose")
                .withDescription("quiet - Be extra quiet only errors are shown;\n"
                        + "verbose - extra information is given for Verbose;\n"
                        + "debug - Show debugging information;\n"
                        + "Default is between debug and verbose level")
                .hasArg().create(VagabondOptionString.LOGLEVEL.toString()));
        options.addOption(OptionBuilder.withArgName("logfile|'n'|'u'")
                .withDescription("The filepath where the log file should be created;\n"
                        + "No file will be created when equal to 'n';\n"
                        + "A unique filename will be created when equal to 'u';\n"
                        + "default it creates a log called '" + settings.logFile + "'")
                .hasArg().create(VagabondOptionString.LOGFILE.toString()));
        options.addOption(OptionBuilder.withArgName("folder path")
                .withDescription("The path to the location that the logs should be placed, "
                        + "default it puts the logs in the folder '" + settings.logFolder + "'")
                .hasArg().create(VagabondOptionString.LOGFOLDER.toString()));

        options.addOption(VagabondOptionString.NOHEADER.toString(), false,
                "Does not write the CSV file header to the output log");

        options.addOption(VagabondOptionString.NOPLACEMENTMAPS.toString(), false,
                "Does not write placement maps to the output log");

        options.addOption(
                OptionBuilder.withArgName("csvfile").withDescription("The file where the result should be stored")
                        .hasArg().create(VagabondOptionString.RESULTSFILE.toString()));

        // custom Console Logging Options
        options.addOption(
                OptionBuilder.withArgName("num").withDescription("The maximum width of the console (default 120)")
                        .hasArg().create(VagabondOptionString.MAXW.toString()));
        options.addOption(OptionBuilder.withArgName("string")
                .withDescription("The new line string when wrapping a long line (default '\\n    ')").hasArg()
                .create(VagabondOptionString.LINESTR.toString()));

        // Settings File
        options.addOption(
                OptionBuilder.withArgName("file").withDescription("The file where the settings are stored, requires: " +
                        VagabondOptionString.getRequiredControlSettings().toString())
                        .hasArg().create(VagabondOptionString.SETTINGSFILE.toString()));
    }

    public Level getLoggerLevel() {
        return settings.logLevel;
    }

    public void setLoggerLevel(Level loggerLevel) {
        settings.logLevel = loggerLevel;
    }

    public void setupLoggerOptions(CommandLine cmd, Options options) throws SecurityException, IOException {
        // Logging Level
        setLoggerLevel(Level.FINE);// Default Level
        if (cmd.hasOption(VagabondOptionString.LOGLEVEL.toString())) {
            String loglevel = cmd.getOptionValue(VagabondOptionString.LOGLEVEL.toString());
            if (loglevel.equalsIgnoreCase("quiet")) {
                setLoggerLevel(Level.WARNING);
                settings.displayPlacementMapEveryEpoch = false;
            } else if (loglevel.equalsIgnoreCase("debug")) {
                setLoggerLevel(Level.FINEST);
                settings.displayPlacementMapEveryEpoch = true;
            } else if (loglevel.equalsIgnoreCase("verbose")) {
                setLoggerLevel(Level.INFO);
                settings.displayPlacementMapEveryEpoch = true;
            }
        }

        // Add CSV File Headers
        if (cmd.hasOption(VagabondOptionString.NOHEADER.toString())) {
            settings.WriteCSVFileHeader = false;
        }

        // Override the Log Level specific options if this commandline parameter is present
        if (cmd.hasOption(VagabondOptionString.NOPLACEMENTMAPS.toString())) {
            settings.displayPlacementMapEveryEpoch = false;
        }

        // Set Logger Folder
        if (cmd.hasOption(VagabondOptionString.LOGFOLDER.toString())) {
            settings.logFolder = cmd.getOptionValue(VagabondOptionString.LOGFOLDER.toString());
        }

        // Set File Logger
        if (cmd.hasOption(VagabondOptionString.LOGFILE.toString())) {
            // Check if no log file was requested
            if (cmd.getOptionValue(VagabondOptionString.LOGFILE.toString()).equals("n")) {
                // Create no log file
                settings.logFile = "";
            } else if (cmd.getOptionValue(VagabondOptionString.LOGFILE.toString()).equals("u")) {
                // Create a unique log file
                settings.logFile = "mohawk-log.%u.%g.txt";
            } else {
                try {
                    // Create a log file with a specific name
                    File logfile = new File(
                            settings.logFolder + File.separator
                                    + cmd.getOptionValue(VagabondOptionString.LOGFILE.toString()));

                    if (!logfile.exists()) {
                        logfile.createNewFile();
                    }
                    settings.logFile = logfile.getAbsolutePath();

                    if (settings.WriteCSVFileHeader) {
                        FileOutputStream writer = new FileOutputStream(logfile, true);// Always append!
                        writer.write(CSVFileFormatter.csvHeaders().getBytes());
                        writer.flush();
                        writer.close();
                    }

                } catch (IOException e) {
                    ErrorPrinter ep = new ErrorPrinter(e);
                    logger.severe(ep.toString());
                    return;
                }
            }
        }

        // Calls setupLogger() in settings
        logger = settings.getLogger();
    }

    private Boolean setupReturnImmediatelyOptions(CommandLine cmd, Options options) throws Exception {
        if (cmd.hasOption(VagabondOptionString.HELP.toString()) == true || cmd.getOptions().length == 0) {
            printHelp(cmd, options);
            return true;
        }

        if (cmd.hasOption(VagabondOptionString.VERSION.toString())) {
            // keep it as simple as possible for the version
            System.out.println(VERSION);
            return true;
        }

        if (cmd.hasOption(VagabondOptionString.AUTHORS.toString())) {
            // keep it as simple as possible for the version
            System.out.println(AUTHORS);
            return true;
        }

        return false;
    }

    private void setupUserPreferenceOptions(CommandLine cmd, Options options) {
        // Set the Console's Max Width
        if (cmd.hasOption(VagabondOptionString.MAXW.toString())) {
            logger.fine("[OPTION] Setting the console's maximum width");
            String maxw = "";
            try {
                maxw = cmd.getOptionValue(VagabondOptionString.MAXW.toString());
                ((ConsoleFormatter) settings.consoleHandler.getFormatter()).maxWidth = Integer.decode(maxw);
            } catch (NumberFormatException e) {
                logger.severe("[ERROR] Could not decode 'maxw': " + maxw + ";\n" + e.getMessage());
            }
        } else {
            logger.fine("[OPTION] Default Console Maximum Width Used");
        }

        // Set the Console's Wrap String
        if (cmd.hasOption(VagabondOptionString.LINESTR.toString())) {
            logger.fine("[OPTION] Setting the console's new line string");
            ((ConsoleFormatter) settings.consoleHandler.getFormatter()).newLineStr = cmd
                    .getOptionValue(VagabondOptionString.LINESTR.toString());
        } else {
            logger.fine("[OPTION] Default Line String Used");
        }
    }

    private void setupResultOptions(CommandLine cmd, Options options) {
        if (cmd.hasOption(VagabondOptionString.RESULTSFILE.toString())) {
            settings.resultsFile = cmd.getOptionValue("results");
            logger.info("[OPTION] Changing the results file to: " + settings.resultsFile);
        } else {
            logger.info("[OPTION] Results File: " + settings.resultsFile);
        }
    }
}
