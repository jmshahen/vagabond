package vagabond.testing;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertTrue;

import java.io.File;
import java.io.IOException;

import org.junit.Test;

import vagabond.pieces.EpochHistory;
import vagabond.pieces.PlacementMap;
import vagabond.placement.PlacementMapExamples;
import vagabond.placement.PlacementMapFile;

public class PlacementMapFileTests {

    @Test
    public void testReadEpochHistory() throws IOException {
        EpochHistory eh = PlacementMapFile.readEpochHistory(new File("tests/placementMapHistory.csv"), 5, null);
        PlacementMap pControl = PlacementMapExamples.nomadSubOptimalRCPlacement();

        assertEquals(5, eh.size());
        assertTrue(pControl.equals(eh._latestPlacementMap));
    }

    @Test
    public void testReadPlacementMap() throws IOException {
        PlacementMap pTest = PlacementMapFile.readPlacementMap("tests/nomadSubOptimalRCPlacement.csv");
        PlacementMap pControl = PlacementMapExamples.nomadSubOptimalRCPlacement();

        assertTrue(pControl.equals(pTest));
    }
}
