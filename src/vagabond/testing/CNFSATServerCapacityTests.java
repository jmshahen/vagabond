package vagabond.testing;

import static org.junit.Assert.assertEquals;

import java.util.*;

import org.junit.Test;

import vagabond.circuit.Circuit;
import vagabond.reduction.cnfsat.ReduceToCNFSAT;

public class CNFSATServerCapacityTests {

    @Test
    public void small() throws Exception {
        ReduceToCNFSAT r = new ReduceToCNFSAT();

        int nc = 2;
        List<Integer> nv = new LinkedList<Integer>();
        nv.add(new Integer(2));
        nv.add(new Integer(1));
        int ns = 2;
        int scapa = 2;

        int nvbitsum = 0;
        for (int i = 0; i < nv.size(); i++) {
            nvbitsum += ReduceToCNFSAT.nbits(nv.get(i));
        }

        Circuit.resetIDs();
        Circuit c = new Circuit();
        for (int i = 0; i < ns * nvbitsum; i++) {
            c.addNewInput();
        }

        r.constraintServerCapacity(c, nc, nv, ns, scapa);

        assertEquals(c.getInputs().size(), 6);
        assertEquals(c.getOutputs().size(), 2);

        List<Boolean> lb = new LinkedList<Boolean>();

        // Client 1, Server 1
        lb.add(new Boolean(true));
        lb.add(new Boolean(false));

        // Client 1, Server 2
        lb.add(new Boolean(true));
        lb.add(new Boolean(true));

        // Client 2, Server 1
        lb.add(new Boolean(true));

        // Client 2, Server 2
        lb.add(new Boolean(false));

        List<Boolean> res = c.evaluate(lb);
        assertEquals(res.get(0), Boolean.TRUE);
        assertEquals(res.get(1), Boolean.FALSE);
    }

    @Test
    public void medium() throws Exception {
        ReduceToCNFSAT r = new ReduceToCNFSAT();

        int nc = 5;
        List<Integer> nv = new LinkedList<Integer>();
        nv.add(new Integer(3));
        nv.add(new Integer(4));
        nv.add(new Integer(2));
        nv.add(new Integer(9));
        nv.add(new Integer(11));

        int ns = 4;
        int scapa = 8; // 8 x 4 = 32 -- so just enough to hold 29 VMs total
                       // Can change this value for more interesting tests. E.g., scapa = 50 should
                       // always be true.

        int nvbitsum = 0;
        for (int i = 0; i < nv.size(); i++) {
            nvbitsum += ReduceToCNFSAT.nbits(nv.get(i));
        }

        Circuit.resetIDs();
        Circuit c = new Circuit();
        for (int i = 0; i < ns * nvbitsum; i++) {
            c.addNewInput();
        }

        r.constraintServerCapacity(c, nc, nv, ns, scapa);

        assertEquals(c.getInputs().size(), 60);
        // 60 = 4 x (2 + 3 + 2 + 4 + 4) = 4 x 15
        assertEquals(c.getOutputs().size(), ns);
        // Out of a lessequals circuit per server

        List<List<Boolean>> clnt = new LinkedList<List<Boolean>>();

        Random rd = new Random();
        for (int i = 0; i < nc; i++) {
            List<Boolean> thisclnt = new LinkedList<Boolean>();
            for (int j = 0; j < ns * ReduceToCNFSAT.nbits(nv.get(i)); j++) {
                if (rd.nextInt() % 6 == 0) {
                    thisclnt.add(new Boolean(Boolean.TRUE));
                } else {
                    thisclnt.add(new Boolean(Boolean.FALSE));
                }
            }
            clnt.add(thisclnt);
        }

        List<Boolean> lb = new LinkedList<Boolean>();
        for (int i = 0; i < clnt.size(); i++) {
            lb.addAll(clnt.get(i));
        }

        List<Boolean> res = c.evaluate(lb);
        assertEquals(res.size(), ns);

        // For each server, check the total
        List<Boolean> checkres = new LinkedList<Boolean>();
        for (int i = 0; i < ns; i++) {
            int nvthiss = 0; // # VMs placed on this server
            for (int j = 0; j < nc; j++) {
                List<Boolean> cb = clnt.get(j);
                for (int k = 0; k < ReduceToCNFSAT.nbits(nv.get(j)); k++) {
                    if (cb.get(i * ReduceToCNFSAT.nbits(nv.get(j)) + k)) {
                        nvthiss += (1 << k);
                    }
                }
            }
            // System.out.println("nvthiss, server "+i+": "+nvthiss);
            if (nvthiss <= scapa) {
                checkres.add(new Boolean(Boolean.TRUE));
            } else {
                checkres.add(new Boolean(Boolean.FALSE));
            }
        }

        assertEquals(res, checkres);
        // System.out.println("res: "+res);
        // System.out.println("checkres: "+checkres);
    }
}
