/**
 * 
 */
package vagabond.testing;

import static org.junit.Assert.fail;

import java.io.*;
import java.util.*;

import org.junit.Test;

import vagabond.circuit.*;
import vagabond.pieces.EpochHistory;
import vagabond.pieces.Machine;
import vagabond.placement.PlacementMapExamples;
import vagabond.reduction.cnfsat.*;
import vagabond.singleton.VagabondSettings;

/**
 * @author tripunit
 *
 */
public class PlacementMapFromCNFSATTests {

    @Test
    public void sortmachinesTest() {
        List<Machine> l = new ArrayList<Machine>();

        Random rd = new Random();
        int nmachines = rd.nextInt(50);
        // System.out.print("[");
        for (int i = 0; i < nmachines; i++) {
            Machine m = new Machine(Math.abs(rd.nextInt(1000)), 10);
            l.add(m);
            // System.out.print(m._machineID+" ");
        }
        // System.out.println("]");

        List<Machine> lunsorted = new ArrayList<Machine>();
        lunsorted.addAll(l);

        PlacementMapFromCNFSATSolver.sortMachines(l);
        // System.out.print("[");
        // for(Iterator<Machine> i = l.iterator(); i.hasNext(); ) {
        // System.out.print(i.next()._machineID+" ");
        // }
        // System.out.println("]");

        Machine prevm = null;
        for (Iterator<Machine> i = l.iterator(); i.hasNext();) {
            Machine thism = i.next();
            if (!lunsorted.contains(thism))
                fail();
            if (prevm != null) {
                if (thism._machineID < prevm._machineID)
                    fail();
            }
            prevm = thism;
        }
    }

    // @Test
    // DOES NOT WORK!
    public void getNcvmspermachineTest() throws IOException {
        // NOTE: see nc, ns, nv values. The instance has to correspond to that.
        // Else this test is meaningless.
        // You can generate it by, for example, Running InfoLeakSumTests.equalSpreadTest()
        // But you have to tweak the nc, ns, nv #s below to match the cnf file.

        String solverloc = VagabondSettings.getInstance().getCNFSolverExceutable().getAbsolutePath();
        String cnfloc = VagabondSettings.getInstance().getCNFInstanceFile().getAbsolutePath();

        Process p = Runtime.getRuntime().exec(solverloc + " " + cnfloc);
        BufferedReader solveroutput = new BufferedReader(new InputStreamReader(p.getInputStream()));
        // save the certificate
        List<String> lines = new LinkedList<String>();
        int notprintedyet = 0;
        for (String str = null; (str = solveroutput.readLine()) != null;) {
            lines.add(str);
            if (str.charAt(0) == 'v' && notprintedyet++ < 5) {
                System.out.println(str);
            }
        }

        if (!lines.contains("s SATISFIABLE")) {
            fail();
        }

        // Otherwise, exercise method
        int nc = 5;
        int ns = 5;
        List<Integer> nv = new ArrayList<Integer>();
        for (int i = 0; i < 5; i++) {
            nv.add(new Integer(5));
        }

        List<List<Integer>> res = PlacementMapFromCNFSATSolver.getNcvmspermachine(lines, nc, ns, nv);
        for (int i = 0; i < res.size(); i++) {
            System.out.println(res.get(i));
        }
    }

    // @Test
    // DOES NOT WORK
    public void getPMTest() throws Exception {
        String solverloc = new String("/home/tripunit/Desktop/lingeling");
        String cnfloc = new String("/tmp/instance.cnf");

        VagabondSettings vs = VagabondSettings.getInstance();
        vs.migrationBudget = new Integer(8);
        vs.numberOfClients = new Integer(3);
        vs.numberOfMachineSlots = new Integer(3);
        vs.numberOfEpochs = new Integer(2);
        vs.numberOfMachines = new Integer(3);
        vs.numberOfVMsPerClient = new Integer(3);

        // No meaningful way to do this test except with all the other constraints thrown in
        EpochHistory eh = new EpochHistory(2);
        eh.add(PlacementMapExamples.equalSpreadClients());

        System.out.println(eh._latestPlacementMap.toString());

        ReduceToCNFSAT _r = new ReduceToCNFSAT();
        _r.reduce(eh);

        // System.out.println(_r._circ.getInputs().size());

        RunSolverCNFSAT rs = new RunSolverCNFSAT();
        rs.load(_r);
        Circuit c = _r._circ;

        int nclientpairs = ((_r.getNc() - 1) * (_r.getNc())) / 2;
        Sum s[];
        s = rs.constraintSumInfoLeak(c, _r, nclientpairs);
        LessEquals le[] = new LessEquals[nclientpairs];
        for (int i = 0; i < le.length; i++)
            le[i] = null;
        Circuit.Wire wirestoand[] = new Circuit.Wire[c.getOutputs().size()];
        for (int i = 0; i < wirestoand.length; i++) {
            wirestoand[i] = c.getOutputs().get(i);
        }

        BigAndOr band[] = new BigAndOr[1];
        band[0] = null;
        rs.constraintInfoLeakUb(c, le, band, s, nclientpairs, 6, wirestoand, 0);

        CircuitUtils.cnfSatToFile(c, cnfloc);
        Process p = Runtime.getRuntime().exec(solverloc + " " + cnfloc);
        BufferedReader solveroutput = new BufferedReader(new InputStreamReader(p.getInputStream()));
        // save the certificate
        List<String> lines = new LinkedList<String>();
        for (String str = null; (str = solveroutput.readLine()) != null;) {
            lines.add(str);
        }

        if (!lines.contains("s SATISFIABLE"))
            fail();
        List<List<Integer>> ncvmspermachine = PlacementMapFromCNFSATSolver.getNcvmspermachine(lines, _r.getNc(),
                _r.getNs(), _r.getNv());
        System.out.println(ncvmspermachine);
        // PlacementMap pm = PlacementMapFromCNFSATSolver.getPlacementMap(_r, lines);
        // System.out.println(pm.toString());
    }
}
