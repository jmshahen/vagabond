/**
 * 
 */
package vagabond.testing;

import static org.junit.Assert.assertEquals;

import java.util.LinkedList;
import java.util.List;

import org.junit.Test;

import vagabond.circuit.Circuit;
import vagabond.reduction.cnfsat.ReduceToCNFSAT;

/**
 * @author tripunit
 *
 */
public class TotalNumberOfClientVMsConstraintTests {

    @Test
    public void small() throws Exception {
        ReduceToCNFSAT r = new ReduceToCNFSAT();

        int nc = 2;
        List<Integer> nv = new LinkedList<Integer>();
        nv.add(new Integer(2));
        nv.add(new Integer(1));
        int ns = 2;

        int nvbitsum = 0;
        for (int i = 0; i < nv.size(); i++) {
            nvbitsum += ReduceToCNFSAT.nbits(nv.get(i));
        }

        Circuit.resetIDs();
        Circuit c = new Circuit();
        for (int i = 0; i < ns * nvbitsum; i++) {
            c.addNewInput();
        }

        r.constraintTotalNumberOfClientVMs(c, nc, nv, ns);
        List<Boolean> lb = new LinkedList<Boolean>();
        // Client 1 x ns machines
        lb.add(new Boolean(false));
        lb.add(new Boolean(true));
        lb.add(new Boolean(false));
        lb.add(new Boolean(false));

        // Client 2 x ns machines
        lb.add(new Boolean(false));
        lb.add(new Boolean(true));

        List<Boolean> res = c.evaluate(lb);
        assertEquals(res.size(), 4);
        for (int i = 0; i < res.size(); i++) {
            assertEquals(res.get(i), Boolean.TRUE);
        }
    }

    @Test
    public void medium() throws Exception {
        ReduceToCNFSAT r = new ReduceToCNFSAT();

        int nc = 5;
        List<Integer> nv = new LinkedList<Integer>();

        nv.add(new Integer(3));
        nv.add(new Integer(4));
        nv.add(new Integer(2));
        nv.add(new Integer(9));
        nv.add(new Integer(11));

        int ns = 4;

        int nvbitsum = 0;
        for (int i = 0; i < nv.size(); i++) {
            nvbitsum += ReduceToCNFSAT.nbits(nv.get(i));
        }

        assertEquals(nvbitsum, 15);

        Circuit.resetIDs();
        Circuit c = new Circuit();
        for (int i = 0; i < ns * nvbitsum; i++) {
            c.addNewInput();
        }

        r.constraintTotalNumberOfClientVMs(c, nc, nv, ns);
        assertEquals(c.getInputs().size(), nvbitsum * ns);

        List<Boolean> lb = new LinkedList<Boolean>();

        // Client 1 placement -- 8 bits total, 2 bits/server
        // Server 1
        lb.add(new Boolean(false));
        lb.add(new Boolean(true));
        // Server 2
        lb.add(new Boolean(false));
        lb.add(new Boolean(false));
        // Server 3
        lb.add(new Boolean(false));
        lb.add(new Boolean(false));
        // Server 4
        lb.add(new Boolean(false));
        lb.add(new Boolean(false));

        // Client 2 placement -- 12 bits total, 3 bits/server
        // Server 1
        lb.add(new Boolean(false));
        lb.add(new Boolean(false));
        lb.add(new Boolean(false));
        // Server 2
        lb.add(new Boolean(false));
        lb.add(new Boolean(false));
        lb.add(new Boolean(false));
        // Server 3
        lb.add(new Boolean(false));
        lb.add(new Boolean(true));
        lb.add(new Boolean(false));
        // Server 4
        lb.add(new Boolean(true));
        lb.add(new Boolean(false));
        lb.add(new Boolean(false));

        // Client 3 placement -- 8 bits total, 2 bits/server
        // Server 1
        lb.add(new Boolean(false));
        lb.add(new Boolean(false));
        // Server 2
        lb.add(new Boolean(false));
        lb.add(new Boolean(false));
        // Server 3
        lb.add(new Boolean(true));
        lb.add(new Boolean(false));
        // Server 4
        lb.add(new Boolean(false));
        lb.add(new Boolean(false));

        // Client 4 placement -- 16 bits total, 4 bits/server
        // Server 1
        lb.add(new Boolean(false));
        lb.add(new Boolean(false));
        lb.add(new Boolean(false));
        lb.add(new Boolean(false));
        // Server 2
        lb.add(new Boolean(false));
        lb.add(new Boolean(false));
        lb.add(new Boolean(false));
        lb.add(new Boolean(false));
        // Server 3
        lb.add(new Boolean(false));
        lb.add(new Boolean(false));
        lb.add(new Boolean(false));
        lb.add(new Boolean(false));
        // Server 4
        lb.add(new Boolean(false));
        lb.add(new Boolean(false));
        lb.add(new Boolean(false));
        lb.add(new Boolean(false));

        // Client 5 placement -- 16 bits total, 4 bits/server
        // Server 1
        lb.add(new Boolean(false));
        lb.add(new Boolean(false));
        lb.add(new Boolean(false));
        lb.add(new Boolean(false));
        // Server 2
        lb.add(new Boolean(false));
        lb.add(new Boolean(true));
        lb.add(new Boolean(true));
        lb.add(new Boolean(false));
        // Server 3
        lb.add(new Boolean(false));
        lb.add(new Boolean(true));
        lb.add(new Boolean(false));
        lb.add(new Boolean(false));
        // Server 4
        lb.add(new Boolean(true));
        lb.add(new Boolean(false));
        lb.add(new Boolean(false));
        lb.add(new Boolean(false));

        List<Boolean> res = c.evaluate(lb);

        // Any client's VMs across all machines <= total # client's vms
        for (int i = 0; i < res.size(); i += 2) {
            assertEquals(res.get(i), Boolean.TRUE);
        }

        // Not true that the total of # client's vms <= client's vms across machines
        for (int i = 1; i < res.size(); i += 2) {
            assertEquals(res.get(i), Boolean.FALSE);
        }

        lb.clear();
        // Client 1 placement -- 8 bits total, 2 bits/server
        // Server 1
        lb.add(new Boolean(true));
        lb.add(new Boolean(true));
        // Server 2
        lb.add(new Boolean(false));
        lb.add(new Boolean(false));
        // Server 3
        lb.add(new Boolean(false));
        lb.add(new Boolean(false));
        // Server 4
        lb.add(new Boolean(false));
        lb.add(new Boolean(false));

        // Client 2 placement -- 12 bits total, 3 bits/server
        // Server 1
        lb.add(new Boolean(false));
        lb.add(new Boolean(false));
        lb.add(new Boolean(false));
        // Server 2
        lb.add(new Boolean(true));
        lb.add(new Boolean(false));
        lb.add(new Boolean(false));
        // Server 3
        lb.add(new Boolean(false));
        lb.add(new Boolean(true));
        lb.add(new Boolean(false));
        // Server 4
        lb.add(new Boolean(true));
        lb.add(new Boolean(false));
        lb.add(new Boolean(false));

        // Client 3 placement -- 8 bits total, 2 bits/server
        // Server 1
        lb.add(new Boolean(false));
        lb.add(new Boolean(false));
        // Server 2
        lb.add(new Boolean(false));
        lb.add(new Boolean(false));
        // Server 3
        lb.add(new Boolean(false));
        lb.add(new Boolean(true));
        // Server 4
        lb.add(new Boolean(false));
        lb.add(new Boolean(false));

        // Client 4 placement -- 16 bits total, 4 bits/server
        // Server 1
        lb.add(new Boolean(true));
        lb.add(new Boolean(true));
        lb.add(new Boolean(false));
        lb.add(new Boolean(false));
        // Server 2
        lb.add(new Boolean(true));
        lb.add(new Boolean(false));
        lb.add(new Boolean(false));
        lb.add(new Boolean(false));
        // Server 3
        lb.add(new Boolean(false));
        lb.add(new Boolean(true));
        lb.add(new Boolean(false));
        lb.add(new Boolean(false));
        // Server 4
        lb.add(new Boolean(true));
        lb.add(new Boolean(true));
        lb.add(new Boolean(false));
        lb.add(new Boolean(false));

        // Client 5 placement -- 16 bits total, 4 bits/server
        // Server 1
        lb.add(new Boolean(false));
        lb.add(new Boolean(false));
        lb.add(new Boolean(false));
        lb.add(new Boolean(false));
        // Server 2
        lb.add(new Boolean(false));
        lb.add(new Boolean(true));
        lb.add(new Boolean(false));
        lb.add(new Boolean(false));
        // Server 3
        lb.add(new Boolean(false));
        lb.add(new Boolean(false));
        lb.add(new Boolean(false));
        lb.add(new Boolean(false));
        // Server 4
        lb.add(new Boolean(true));
        lb.add(new Boolean(false));
        lb.add(new Boolean(false));
        lb.add(new Boolean(true));

        res = c.evaluate(lb);

        // client totals are exactly correct
        for (int i = 0; i < res.size(); i++) {
            assertEquals(res.get(i), Boolean.TRUE);
        }
    }
}
