package vagabond.testing;

import static org.junit.Assert.assertEquals;

import org.junit.*;
import org.junit.runners.MethodSorters;

import vagabond.pieces.*;
import vagabond.placement.PlacementMapExamples;
import vagabond.placement.RandomPlacement;
import vagabond.timing.TimingManager;

@FixMethodOrder(MethodSorters.NAME_ASCENDING)
public class ClientToClientInformationLeakageTests {
    public static PlacementMap standardPlacement = null;
    public static PlacementMap optimalPlacement = null;

    @BeforeClass
    public static void setup() {
        standardPlacement = PlacementMapExamples.equalSpreadClients();
        optimalPlacement = PlacementMapExamples.optimalPlacement();
        System.out.println("Standard Placement:\n" + standardPlacement.toString());
        System.out.println("Optimal Placement:\n" + optimalPlacement.toString());
    }

    @Test
    public void A1_simpleTest() {
        ClientToClientInformationLeakage test = new ClientToClientInformationLeakage(standardPlacement);
        // System.out.println(test.toString());

        // loop through the matrix and check that they are all equal to 3, except if c1 == c2 -> equal to 0
        Integer zero = new Integer(0);
        Integer three = new Integer(3);
        for (Integer c1 : test._clientIDs) {
            for (Integer c2 : test._clientIDs) {
                Integer infoLeak = test.get(c1, c2);
                if (c1 == c2) {
                    assertEquals(zero, infoLeak);
                } else {
                    assertEquals(three, infoLeak);
                }
            }
        }

        assertEquals(3, test.getMaxInformationLeak());
    }

    @Test
    public void additionTest1() {
        ClientToClientInformationLeakage s1 = new ClientToClientInformationLeakage(standardPlacement);
        ClientToClientInformationLeakage s2 = new ClientToClientInformationLeakage(standardPlacement);
        // System.out.println(s1);

        // addTo function
        s1.addTo(s2);

        System.out.println(s1);

        // Loop through and make sure they all equal 6
        Integer zero = new Integer(0);
        Integer six = new Integer(6);
        for (Integer c1 : s1._clientIDs) {
            for (Integer c2 : s1._clientIDs) {
                Integer infoLeak = s1.get(c1, c2);
                if (c1 == c2) {
                    assertEquals(zero, infoLeak);
                } else {
                    assertEquals(six, infoLeak);
                }
            }
        }

        assertEquals(6, s1.getMaxInformationLeak());
    }

    @Test
    public void additionTest2() {
        ClientToClientInformationLeakage s1 = new ClientToClientInformationLeakage(optimalPlacement);
        ClientToClientInformationLeakage s2 = new ClientToClientInformationLeakage(standardPlacement);
        // System.out.println(s1);

        // addTo function
        s2.addTo(s1);

        // System.out.println(s1);

        // Loop through and make sure they all equal 3
        Integer zero = new Integer(0);
        Integer three = new Integer(3);
        for (Integer c1 : s2._clientIDs) {
            for (Integer c2 : s2._clientIDs) {
                Integer infoLeak = s2.get(c1, c2);
                if (c1 == c2) {
                    assertEquals(zero, infoLeak);
                } else {
                    assertEquals(three, infoLeak);
                }
            }
        }

        assertEquals(3, s2.getMaxInformationLeak());
        assertEquals(0, s1.getMaxInformationLeak());
    }

    @Test
    public void subtractionTest() {
        ClientToClientInformationLeakage s1 = new ClientToClientInformationLeakage(standardPlacement);
        ClientToClientInformationLeakage s2 = new ClientToClientInformationLeakage(standardPlacement);

        // subtractFrom function
        s1.subtractFrom(s2);

        // System.out.println(s1);

        // Loop through and make sure they all equal 0
        Integer zero = new Integer(0);

        for (Integer c1 : s1._clientIDs) {
            for (Integer c2 : s1._clientIDs) {
                Integer infoLeak = s1.get(c1, c2);

                assertEquals(zero, infoLeak);
            }
        }

        assertEquals(3, s2.getMaxInformationLeak());
        assertEquals(0, s1.getMaxInformationLeak());
    }

    @Test
    public void subtractionTest2() {
        ClientToClientInformationLeakage s1 = new ClientToClientInformationLeakage(standardPlacement);
        ClientToClientInformationLeakage s2 = new ClientToClientInformationLeakage(standardPlacement);

        s1.addTo(s2);
        s1.addTo(s2);

        System.out.println("s1:" + s1);
        System.out.println("s2:" + s2);
        // subtractFrom function
        s1.subtractFrom(s2);

        System.out.println("s1-s2:" + s1);

        // Loop through and make sure they all equal 0
        Integer zero = 0;
        Integer six = 6;

        for (Integer c1 : s1._clientIDs) {
            for (Integer c2 : s1._clientIDs) {
                Integer infoLeak = s1.get(c1, c2);
                System.out.println("c1=" + c1 + "; c2=" + c2 + "; infoLeak=" + infoLeak);

                if (c1.equals(c2)) {
                    assertEquals(zero, infoLeak);
                } else {
                    assertEquals(six, infoLeak);
                }
            }
        }

        assertEquals(3, s2.getMaxInformationLeak());
        assertEquals(6, s1.getMaxInformationLeak());
    }

    @Test
    public void epochHistoryTest() {
        EpochHistory eh = new EpochHistory(5);

        assertEquals(0, eh.size());
        eh.add(standardPlacement);
        assertEquals(1, eh.size());
        eh.add(standardPlacement);
        assertEquals(2, eh.size());

        Integer zero = 0;
        Integer six = 6;
        for (Integer c1 : eh._sumOfInformationLeakage._clientIDs) {
            for (Integer c2 : eh._sumOfInformationLeakage._clientIDs) {
                Integer infoLeak = eh._sumOfInformationLeakage.get(c1, c2);
                // System.out.println("c1=" + c1 + "; c2=" + c2 + "; infoLeak=" + infoLeak);

                if (c1.equals(c2)) {
                    assertEquals(zero, infoLeak);
                } else {
                    assertEquals(six, infoLeak);
                }
            }
        }
        eh.add(standardPlacement);
        assertEquals(3, eh.size());
        eh.add(standardPlacement);
        assertEquals(4, eh.size());

        Integer twelve = 12;
        for (Integer c1 : eh._sumOfInformationLeakage._clientIDs) {
            for (Integer c2 : eh._sumOfInformationLeakage._clientIDs) {
                Integer infoLeak = eh._sumOfInformationLeakage.get(c1, c2);
                // System.out.println("c1=" + c1 + "; c2=" + c2 + "; infoLeak=" + infoLeak);

                if (c1.equals(c2)) {
                    assertEquals(zero, infoLeak);
                } else {
                    assertEquals(twelve, infoLeak);
                }
            }
        }

        eh.add(standardPlacement);
        assertEquals(5, eh.size());

        Integer fifteen = 15;
        for (Integer c1 : eh._sumOfInformationLeakage._clientIDs) {
            for (Integer c2 : eh._sumOfInformationLeakage._clientIDs) {
                Integer infoLeak = eh._sumOfInformationLeakage.get(c1, c2);
                // System.out.println("c1=" + c1 + "; c2=" + c2 + "; infoLeak=" + infoLeak);

                if (c1.equals(c2)) {
                    assertEquals(zero, infoLeak);
                } else {
                    assertEquals(fifteen, infoLeak);
                }
            }
        }

        eh.add(standardPlacement);
        assertEquals(5, eh.size());

        for (Integer c1 : eh._sumOfInformationLeakage._clientIDs) {
            for (Integer c2 : eh._sumOfInformationLeakage._clientIDs) {
                Integer infoLeak = eh._sumOfInformationLeakage.get(c1, c2);
                // System.out.println("c1=" + c1 + "; c2=" + c2 + "; infoLeak=" + infoLeak);

                if (c1.equals(c2)) {
                    assertEquals(zero, infoLeak);
                } else {
                    assertEquals(fifteen, infoLeak);
                }
            }
        }

        eh.add(standardPlacement);
        assertEquals(5, eh.size());

        for (Integer c1 : eh._sumOfInformationLeakage._clientIDs) {
            for (Integer c2 : eh._sumOfInformationLeakage._clientIDs) {
                Integer infoLeak = eh._sumOfInformationLeakage.get(c1, c2);
                // System.out.println("c1=" + c1 + "; c2=" + c2 + "; infoLeak=" + infoLeak);

                if (c1.equals(c2)) {
                    assertEquals(zero, infoLeak);
                } else {
                    assertEquals(fifteen, infoLeak);
                }
            }
        }
    }

    @Test
    public void nomadLargeILPTestSize() {
        TimingManager timing = new TimingManager();

        timing.startTimer("nomadLargeILPTestSize::RandomPlacement");
        RandomPlacement rp = new RandomPlacement();
        PlacementMap place = rp.generatePlacementMapNomadHardILP();
        timing.stopTimer("nomadLargeILPTestSize::RandomPlacement");

        timing.startTimer("nomadLargeILPTestSize::ClientToClientInformationLeakage");
        new ClientToClientInformationLeakage(place);
        timing.stopTimer("nomadLargeILPTestSize::ClientToClientInformationLeakage");

        System.out.println(timing.toString());
    }
}
