package vagabond.testing;

import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertTrue;

import org.junit.Test;

import vagabond.pieces.EpochHistory;
import vagabond.pieces.PlacementMap;
import vagabond.placement.PlacementMapExamples;
import vagabond.reduction.ilp.WriteILPFile;
import vagabond.singleton.VagabondSettings;

public class WriteILPFileTests {

    @Test
    public void standardExampleTest() {
        VagabondSettings settings = VagabondSettings.getInstance();
        settings.migrationBudget = 10; // requires migrationbudget >= 6 to reach optimal

        EpochHistory ep = new EpochHistory(5);
        PlacementMap place = PlacementMapExamples.equalSpreadClients();

        ep.add(place);

        String ilpString = WriteILPFile.toILPString(ep);
        System.out.println(ilpString);

        // Check for proper r
        assertTrue(ilpString.contains("r=3"));

        // Check for proper numMachines
        assertTrue(ilpString.contains("numMachines=3"));

        // Check for proper numClients
        assertTrue(ilpString.contains("numClients=3"));

        // Check for proper migrationBudget
        assertTrue(ilpString.contains("migrationBudget=" + settings.migrationBudget));

        // Check for proper capacity and client VMs
        assertTrue(ilpString.contains("[3,3,3]"));

        // Check for proper L
        assertTrue(ilpString.contains("[0,3,3]"));
        assertTrue(ilpString.contains("[3,0,3]"));
        assertTrue(ilpString.contains("[3,3,0]"));

        // Check for proper p0
        assertTrue(ilpString.contains("[1,1,1]"));
        assertFalse(ilpString.contains("[1,0,1]"));
        assertFalse(ilpString.contains("[1,0,0]"));
    }

}
