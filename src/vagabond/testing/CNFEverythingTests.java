/**
 * 
 */
package vagabond.testing;

import org.junit.Test;

import vagabond.pieces.EpochHistory;
import vagabond.placement.PlacementMapExamples;
import vagabond.reduction.cnfsat.ReduceToCNFSAT;
import vagabond.reduction.cnfsat.RunSolverCNFSAT;
import vagabond.singleton.VagabondSettings;
import vagabond.timing.TimingManager;

/**
 * @author tripunit
 *
 */
public class CNFEverythingTests {

    @Test
    public void equalSpreadTest() throws Exception {
        boolean minMaxCtoCInfoLeak = true;

        VagabondSettings vs = VagabondSettings.getInstance();
        vs.migrationBudget = 3;
        vs.numberOfClients = 3;
        vs.numberOfMachineSlots = 3;
        vs.numberOfEpochs = 5;
        vs.numberOfMachines = 3;
        vs.numberOfVMsPerClient = 3;
        vs.minimizeMaxClientToClientInfoLeak = minMaxCtoCInfoLeak;
        vs.timing = new TimingManager();

        // No meaningful way to do this test except with all the other constraints thrown in
        EpochHistory eh = new EpochHistory(2);
        eh.add(PlacementMapExamples.equalSpreadClients());

        System.out.println("Initial placement: ");
        System.out.println(eh._latestPlacementMap.toString());

        ReduceToCNFSAT _r = new ReduceToCNFSAT();
        _r.reduce(eh);

        // System.out.println(_r._circ.getInputs().size());

        RunSolverCNFSAT rs = new RunSolverCNFSAT();
        rs.load(_r);
        EpochHistory neweh = rs.run();

        System.out.println("Final placement: ");
        System.out.println(neweh._latestPlacementMap.toString());

        eh.add(neweh);

        System.out.println("Timing: " + vs.timing);
    }

    @Test
    public void equalSpreadTestNoMigration() throws Exception {
        boolean minMaxCtoCInfoLeak = true;

        VagabondSettings vs = VagabondSettings.getInstance();
        vs.migrationBudget = new Integer(-1);
        vs.numberOfClients = new Integer(3);
        vs.numberOfMachineSlots = new Integer(3);
        vs.numberOfEpochs = new Integer(5);
        vs.numberOfMachines = new Integer(3);
        vs.numberOfVMsPerClient = new Integer(3);
        vs.minimizeMaxClientToClientInfoLeak = minMaxCtoCInfoLeak;
        vs.timing = new TimingManager();

        // No meaningful way to do this test except with all the other constraints thrown in
        EpochHistory eh = new EpochHistory(2);
        eh.add(PlacementMapExamples.equalSpreadClients());

        System.out.println("Initial placement: ");
        System.out.println(eh._latestPlacementMap.toString());

        ReduceToCNFSAT _r = new ReduceToCNFSAT();
        _r.reduce(eh);

        // System.out.println(_r._circ.getInputs().size());

        RunSolverCNFSAT rs = new RunSolverCNFSAT();
        rs.load(_r);
        EpochHistory neweh = rs.run();

        System.out.println("Final placement: ");
        System.out.println(neweh._latestPlacementMap.toString());

        eh.add(neweh);

        System.out.println("Timing: " + vs.timing);
    }

    @Test
    public void equalSpreadTestSum() throws Exception {
        boolean minMaxCtoCInfoLeak = false;

        VagabondSettings vs = VagabondSettings.getInstance();
        vs.migrationBudget = 3;
        vs.numberOfClients = 3;
        vs.numberOfMachineSlots = 3;
        vs.numberOfEpochs = 5;
        vs.numberOfMachines = 3;
        vs.numberOfVMsPerClient = 3;
        vs.minimizeMaxClientToClientInfoLeak = minMaxCtoCInfoLeak;
        vs.timing = new TimingManager();

        // No meaningful way to do this test except with all the other constraints thrown in
        EpochHistory eh = new EpochHistory(2);
        eh.add(PlacementMapExamples.equalSpreadClients());

        System.out.println("Initial placement: ");
        System.out.println(eh._latestPlacementMap.toString());

        ReduceToCNFSAT _r = new ReduceToCNFSAT();
        _r.reduce(eh);

        // System.out.println(_r._circ.getInputs().size());

        RunSolverCNFSAT rs = new RunSolverCNFSAT();
        rs.load(_r);
        EpochHistory neweh = rs.run();

        System.out.println("Final placement: ");
        System.out.println(neweh._latestPlacementMap.toString());

        eh.add(neweh);

        System.out.println("Timing: " + vs.timing);
    }

    @Test
    public void equalSpreadTestNoMigrationSum() throws Exception {
        boolean minMaxCtoCInfoLeak = false;

        VagabondSettings vs = VagabondSettings.getInstance();
        vs.migrationBudget = new Integer(-1);
        vs.numberOfClients = new Integer(3);
        vs.numberOfMachineSlots = new Integer(3);
        vs.numberOfEpochs = new Integer(5);
        vs.numberOfMachines = new Integer(3);
        vs.numberOfVMsPerClient = new Integer(3);
        vs.minimizeMaxClientToClientInfoLeak = minMaxCtoCInfoLeak;
        vs.timing = new TimingManager();

        // No meaningful way to do this test except with all the other constraints thrown in
        EpochHistory eh = new EpochHistory(2);
        eh.add(PlacementMapExamples.equalSpreadClients());

        System.out.println("Initial placement: ");
        System.out.println(eh._latestPlacementMap.toString());

        ReduceToCNFSAT _r = new ReduceToCNFSAT();
        _r.reduce(eh);

        // System.out.println(_r._circ.getInputs().size());

        RunSolverCNFSAT rs = new RunSolverCNFSAT();
        rs.load(_r);
        EpochHistory neweh = rs.run();

        System.out.println("Final placement: ");
        System.out.println(neweh._latestPlacementMap.toString());

        eh.add(neweh);

        System.out.println("Timing: " + vs.timing);
    }
}
