package vagabond.testing;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNull;

import org.junit.Test;

import vagabond.helper.RotatingArray;
import vagabond.pieces.PlacementMap;

public class RotatingArrayTests {

    @SuppressWarnings("unused")
    @Test
    public void creation() {
        RotatingArray<Integer> t1 = new RotatingArray<>(10);
        RotatingArray<PlacementMap> t2 = new RotatingArray<>(100);
    }

    @Test
    public void checkNull() {
        RotatingArray<Integer> t1 = new RotatingArray<>(10);
        assertNull(t1.get(0));

        t1.pushToFirst(null);
    }

    @Test
    public void insert1() {
        RotatingArray<Integer> t1 = new RotatingArray<>(10);
        t1.pushToFirst(new Integer(12));

        assertEquals(1, t1.size());

        assertEquals(new Integer(12), t1.getFirst());
        assertEquals(new Integer(12), t1.get(0));
    }

    @Test
    public void insert2() {
        RotatingArray<Integer> t1 = new RotatingArray<>(10);
        t1.pushToFirst(new Integer(12));

        assertEquals(1, t1.size());

        assertEquals(new Integer(12), t1.getFirst());

        t1.pushToFirst(new Integer(1));

        assertEquals(2, t1.size());

        assertEquals(new Integer(1), t1.getFirst());
        assertEquals(new Integer(12), t1.get(1));
    }

    @Test
    public void insertFull() {
        RotatingArray<Integer> t1 = new RotatingArray<>(10);

        for (int i = 0; i < 10; i++) {
            t1.pushToFirst(new Integer(i + 1));

            assertEquals(i + 1, t1.size());

            assertEquals(new Integer(i + 1), t1.getFirst());
        }
        assertEquals(10, t1.size());
    }

    @Test
    public void insertOverFull() {
        RotatingArray<Integer> t1 = new RotatingArray<>(10);

        for (int i = 0; i < 17; i++) {
            t1.pushToFirst(new Integer(i + 1));

            assertEquals(new Integer(i + 1), t1.getFirst());
        }
        assertEquals(10, t1.size());
    }

    @Test
    public void getEndOfArray() {
    	RotatingArray<Integer> t1 = new RotatingArray<>(10);

        for (int i = 0; i < 9; i++) {
            t1.pushToFirst(new Integer(i + 1));

            assertEquals(null, t1.getEndOfArray());
        }
        
        for (int i = 0; i < 5; i++) {
            t1.pushToFirst(new Integer(i + 1));

            assertEquals(new Integer(i + 1), t1.getEndOfArray());
        }
        assertEquals(10, t1.size());
    }
}
