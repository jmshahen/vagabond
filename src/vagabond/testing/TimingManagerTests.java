package vagabond.testing;

import org.junit.Test;

import vagabond.timing.TimingManager;

public class TimingManagerTests {

    @Test
    public void toggleTest() throws InterruptedException {
        TimingManager timing = new TimingManager();
        timing.toggleTimer("key1");
        Thread.sleep(40);
        timing.toggleTimer("key1");
        timing.toggleTimer("key2");
        timing.toggleTimer("key3");
        Thread.sleep(20);
        timing.toggleTimer("key3");
        Thread.sleep(30);
        timing.toggleTimer("key2");

        System.out.println(timing.toString());
    }
}
