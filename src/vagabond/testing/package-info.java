/**
 *
 * Stores all of the JUunit tests that should be run before committing any code to the master branch.
 */
package vagabond.testing;