package vagabond.testing;

import static org.junit.Assert.assertEquals;

import java.util.ArrayList;
import java.util.Arrays;

import org.junit.Test;

import vagabond.pieces.ClientToClientInformationLeakage;
import vagabond.pieces.PlacementMap;
import vagabond.placement.PlacementMapExamples;
import vagabond.results.StatisticsManager;

public class StatisticsManagerTests {

    @Test
    public void traceBackListTest() {
        ArrayList<Integer> test1 = new ArrayList<Integer>(Arrays.asList(0, 1, 2, 3, 4, 4, 4, 4, 4, 4, 4));
        assertEquals(4, StatisticsManager.traceBackList(test1, 4));
        assertEquals(4, StatisticsManager.traceBackList(test1, null));
        assertEquals(test1.size(), StatisticsManager.traceBackList(test1, -1));
    }

    @Test
    public void getArrayStrTest() {
        int[] test1 = { 1, 2, 3, 4 };
        String test1Str = StatisticsManager.getArrayStr(test1, ";");

        assertEquals("0=1;1=2;2=3;3=4", test1Str);
    }

    @Test
    public void getListStrTest() {
        ArrayList<Integer> test = new ArrayList<>(Arrays.asList(1, 2, 3, 4));
        String test1Str = StatisticsManager.getListStr(test, ";");

        assertEquals("1;2;3;4", test1Str);
    }

    @Test
    public void getDistributionTest() {
        PlacementMap place = PlacementMapExamples.equalSpreadClients(3);
        ClientToClientInformationLeakage testCCIL = new ClientToClientInformationLeakage(place);

        int[] testDist = StatisticsManager.getDistribution(testCCIL, testCCIL.getMaxInformationLeak());
        int[] expectedDist = { 0, 0, 0, 6 };

        assertEquals(expectedDist.length, testDist.length);

        for (int t = 0; t < expectedDist.length; t++) {
            // System.out.println(t);
            assertEquals(expectedDist[t], testDist[t]);
        }

    }
}
