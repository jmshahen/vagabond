package vagabond.testing;

import static org.junit.Assert.assertEquals;

import org.junit.*;
import org.junit.runners.MethodSorters;

import vagabond.pieces.PlacementMap;
import vagabond.pieces.SumOfClientVMsPerMachine;
import vagabond.placement.PlacementMapExamples;
import vagabond.placement.RandomPlacement;
import vagabond.timing.TimingManager;

/**
 * Tests of the SumOfClientVMsPerMachine class
 * @author Jonathan Shahen
 *
 */
@FixMethodOrder(MethodSorters.NAME_ASCENDING)
public class SumOfClientVMsPerMachineTests {
    /**
     * @see PlacementMapExamples#equalSpreadClients()
     */
    public static PlacementMap standardPlacement = null;

    @BeforeClass
    public static void setup() {
        standardPlacement = PlacementMapExamples.equalSpreadClients();
        System.out.println(standardPlacement.toString());
    }

    @Test
    public void A1_simpleTest() {
        SumOfClientVMsPerMachine test = new SumOfClientVMsPerMachine(standardPlacement);
        System.out.println(test.toString());

        // loop through the matrix and check that they are all equal to 1
        for (int m = 0; m < test.getNumberOfMachines(); m++) {
            for (int c = 0; c < test.getNumberOfClients(); c++) {
                assertEquals(new Integer(1), test.get(m, c));
            }
        }
    }

    @Test
    public void nomadLargeILPTestSize() {
        TimingManager timing = new TimingManager();

        timing.startTimer("nomadLargeILPTestSize::RandomPlacement");
        RandomPlacement rp = new RandomPlacement();
        PlacementMap place = rp.generatePlacementMapNomadHardILP();
        timing.stopTimer("nomadLargeILPTestSize::RandomPlacement");

        timing.startTimer("nomadLargeILPTestSize::SumOfClientVMsPerMachine");
        new SumOfClientVMsPerMachine(place);
        timing.stopTimer("nomadLargeILPTestSize::SumOfClientVMsPerMachine");

        System.out.println(timing.toString());
    }
}
