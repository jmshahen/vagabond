package vagabond.keys;

public class MachineAndClientKey {

    public final int _machineID;
    public final int _clientID;

    public MachineAndClientKey(final int machineID, final int clientID) {
        _machineID = machineID;
        _clientID = clientID;
    }

    public boolean equals(final Object O) {
        if (!(O instanceof MachineAndClientKey))
            return false;
        if (((MachineAndClientKey) O)._machineID != _machineID)
            return false;
        if (((MachineAndClientKey) O)._clientID != _clientID)
            return false;
        return true;
    }

    public int hashCode() {
        return (_clientID << 16) + _machineID;
    }

    @Override
    public String toString() {
        return "<" + _machineID + "," + _clientID + ">";
    }
}
