/**
 *
 * Lots of Hashtables are used in this project to imitate 2 or more dimensioned arrays; this package stores all of 
 * the objects that represent the Hashtable's keys.
 */
package vagabond.keys;