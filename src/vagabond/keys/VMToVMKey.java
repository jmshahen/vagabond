package vagabond.keys;

public class VMToVMKey {

    public final int _lower;
    public final int _higher;

    public VMToVMKey(final int X, final int Y) {
        if (X < Y) {
            this._lower = X;
            this._higher = Y;
        } else {
            this._lower = Y;
            this._higher = X;
        }
    }

    public boolean equals(final Object O) {
        if (!(O instanceof VMToVMKey))
            return false;
        if (((VMToVMKey) O)._lower != _lower)
            return false;
        if (((VMToVMKey) O)._higher != _higher)
            return false;
        return true;
    }

    public int hashCode() {
        return (_higher << 16) + _lower;
    }
}
