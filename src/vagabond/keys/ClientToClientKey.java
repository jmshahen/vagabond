package vagabond.keys;

/**
 * A Hashtable key that links Client 1 and Client 2, but stores them specifically with Client 1 &lt;= Client 2
 * @author Jonathan Shahen
 *
 */
public class ClientToClientKey {

    public final int _lowerClientID;
    public final int _higherClientID;

    public ClientToClientKey(final int clientID1, final int clientID2) {
        if (clientID1 < clientID2) {
            this._lowerClientID = clientID1;
            this._higherClientID = clientID2;
        } else {
            this._lowerClientID = clientID2;
            this._higherClientID = clientID1;
        }
    }

    public ClientToClientKey(ClientToClientKey key) {
        _lowerClientID = key._lowerClientID;
        _higherClientID = key._higherClientID;
    }

    public boolean equals(final Object O) {
        if (!(O instanceof ClientToClientKey))
            return false;
        if (((ClientToClientKey) O)._lowerClientID != _lowerClientID)
            return false;
        if (((ClientToClientKey) O)._higherClientID != _higherClientID)
            return false;
        return true;
    }

    /**
     * Simple hash function to try and spread the Client ID pairs apart
     */
    public int hashCode() {
        return (_higherClientID << 16) + _lowerClientID;
    }

    @Override
    public String toString() {
        StringBuilder sb = new StringBuilder();

        sb.append("ClientToClientKey {").append(_lowerClientID).append(" -> ").append(_higherClientID).append("}");

        return sb.toString();
    }
}
