package vagabond.pieces;

import java.util.ArrayList;

/**
 * Class to contain information related to a single Client, stores: the Client ID, and the VMs owned by this client.
 * @author Jonathan Shahen
 *
 */
public class Client {
    public Integer _clientID;
    public ArrayList<VM> _vms;
    private Integer _nextVMID;

    public Client(Integer id) {
        _clientID = id;
        _vms = new ArrayList<VM>();
        _nextVMID = 0;
    }

    /**
     * Add a VM to a client: there is no limit to how many VMs a client can have
     * @param vm the VM that is fully filled out (has the correct Client ID)
     * @return TRUE if successful, FALSE otherwise
     */
    public boolean addVM(VM vm) {
        _nextVMID = Math.max(_nextVMID + 1, vm._serviceID + 1);
        if (vm._clientID == _clientID) { return _vms.add(vm); }
        return false;
    }

    /**
     * This is a 'best-guess' way to get the Service ID that has not been used; 
     * Service ID is currently not used, and thus an exhaustive search is not required
     * @return the current number of VMs in assigned to this client
     */
    public Integer getNextAvailableServiceID() {
        return _nextVMID;
    }

    /**
     * Returns the number of VMs owned by this Client
     * @return number of vms owned by the client
     */
    public int getNumberOfVMs() {
        return _vms.size();
    }

    @Override
    public boolean equals(Object obj) {
        if (!(obj instanceof Client)) {
            System.out.println("Not of the Client class type");
            return false;
        }

        Client c = (Client) obj;

        if (!_clientID.equals(c._clientID)) {
            System.out.println("Client IDs doe not match");
            return false;
        }

        if (_vms.size() != c._vms.size()) {
            System.out.println("Number of VMs does not match");
            return false;
        }

        for (VM vm : _vms) {
            if (!c._vms.contains(vm)) {
                System.out.println("obj does not contain the VM: " + vm);
                return false;
            }
        }

        return true;
    }
}
