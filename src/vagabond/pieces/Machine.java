package vagabond.pieces;

import java.io.BufferedWriter;
import java.io.IOException;
import java.util.ArrayList;

import vagabond.enums.PlacementMapDisplayStyle;

/**
 * Object that represents a Machine in a system: contains slots that are able to hold 1 VM each, there is a 
 * maximum number of slots
 * @author Jonathan Shahen
 *
 */
public class Machine {
    public static final Integer TOSTRING_MAXVMS = 100;

    public Integer _machineID;
    public ArrayList<VM> _vms;
    public Integer _maxSpots;

    public Machine(Integer machineID, Integer maxSpots) {
        _machineID = machineID;
        _vms = new ArrayList<VM>(maxSpots);
        _maxSpots = maxSpots;
    }

    /**
     * Get the Total Number of VMs in the Machine
     * @return the number of VMs
     */
    public Integer getNumberOfVMs() {
        return _vms.size();
    }

    /**
     * Adds a VM to this machine
     * @param vm the VM to be added
     * @return TRUE if it worked; FALSE if the machine is full or it failed
     */
    public Boolean addVM(VM vm) {
        Boolean added = Boolean.FALSE;

        if (!isFull()) {
            added = _vms.add(vm);
        }

        return added;
    }

    /**
     * Returns if all of the spots are full
     * @return TRUE if there is no space for another VM; FALSE if there is atleast one space
     */
    public Boolean isFull() {
        if (getNumberOfVMs() < _maxSpots) { return Boolean.FALSE; }
        return Boolean.TRUE;
    }

    @Override
    public String toString() {
        StringBuilder sb = new StringBuilder();
        sb.append("Machine ").append(_machineID).append(" {");

        if (_vms.size() > 0) {
            for (int i = 0; i < _vms.size(); i++) {
                if (i != 0) {
                    sb.append(", ");
                }
                sb.append(_vms.get(i).toString());

                if (i == TOSTRING_MAXVMS) {
                    sb.append(" ... Skipping " + (_vms.size() - TOSTRING_MAXVMS) + " VMs");
                    break;
                }
            }
        } else {
            sb.append("[EMPTY]");
        }
        sb.append("}");
        return sb.toString();
    }

    public String getFormatString(PlacementMapDisplayStyle displayStyle) {
        StringBuilder sb = new StringBuilder();

        switch (displayStyle) {
        case NOMAD:
            sb.append("// Machine ").append(_machineID).append("\n").append(_machineID).append(" ");
            break;
        case CSV:
            break;
        default:
            sb.append("Machine ").append(_machineID).append(" ");

        }

        for (int i = 0; i < _vms.size(); i++) {
            if (displayStyle == PlacementMapDisplayStyle.CSV) {
                sb.append(_machineID + ",").append(_vms.get(i).getFormatString(displayStyle)).append("\n");
            } else {
                sb.append(_vms.get(i).getFormatString(displayStyle)).append(" ");
            }
        }
        return sb.toString();
    }

    public void writeOut(BufferedWriter writer, PlacementMapDisplayStyle displayStyle) throws IOException {
        writer.write(getFormatString(displayStyle));
        if (displayStyle != PlacementMapDisplayStyle.CSV) {
            writer.write("\n");
        }
    }

    /**
     * Returns the CSV Header of how the {@link #getFormatString(PlacementMapDisplayStyle)} functions arranges the 
     * Machine data.
     * @return CSV formatted header describing what the columns are for the CSV display style
     */
    public static String getCSVHeaderString() {
        StringBuilder sb = new StringBuilder();

        sb.append("Machine ID,").append(VM.getCSVHeaderString());

        return sb.toString();
    }

    @Override
    public boolean equals(Object obj) {
        if (!(obj instanceof Machine)) {
            System.out.println("Not of the Machine class type");
            return false;
        }

        Machine m = (Machine) obj;

        if (!_machineID.equals(m._machineID)) {
            System.out.println("Machine IDs does not match");
            return false;
        }

        if (!_maxSpots.equals(m._maxSpots)) {
            System.out.println("Max Spots does not match");
            return false;
        }

        if (_vms.size() != m._vms.size()) {
            System.out.println("Number of VMs does not match");
            return false;
        }

        for (VM vm : _vms) {
            if (!m._vms.contains(vm)) {
                System.out.println("obj does not contain the VM: " + vm);
                return false;
            }
        }

        return true;
    }
}
