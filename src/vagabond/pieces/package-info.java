/**
 *
 * An assortment of pieces that make up how Vagabond functions, contains things like: PlacementMap, Client, VM, 
 * Machine, ...
 */
package vagabond.pieces;