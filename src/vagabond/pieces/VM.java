package vagabond.pieces;

import vagabond.enums.PlacementMapDisplayStyle;

public class VM {
    /**
     * Changes the way that toString outputs the _globalID, _clientID, and _serviceID; 
     * change this for better readability or parsing ability
     */
    public static PlacementMapDisplayStyle _displayStyle = PlacementMapDisplayStyle.VAGABOND;

    /**
     * Client ID of the owner of this VM
     */
    public Integer _clientID;
    /**
     * unique VM ID to the above Client
     */
    public Integer _serviceID;
    /**
     * unique VM ID to the Placement Map
     */
    public Integer _globalID;

    public VM(Integer clientID, Integer serviceID, Integer globalID) {
        _clientID = clientID;
        _serviceID = serviceID;
        _globalID = globalID;
    }

    @Override
    public String toString() {
        return getFormatString(_displayStyle);
    }

    public String getFormatString(PlacementMapDisplayStyle displayStyle) {
        StringBuilder sb = new StringBuilder();

        switch (displayStyle) {
        default:
        case VAGABOND:
            sb.append(_globalID).append("<").append(_clientID).append(",").append(_serviceID).append(">");
            break;
        case INFORMATIVE:
            sb.append(_globalID).append("<c").append(_clientID).append(",s").append(_serviceID).append(">");
            break;
        case INFORMATIVE_CAP:
            sb.append(_globalID).append("<C").append(_clientID).append(",S").append(_serviceID).append(">");
            break;
        case CSV: // Comma Separated Value (CSV) format
            sb.append(_globalID).append(",").append(_clientID).append(",").append(_serviceID);
            break;
        case NOMAD: // Lose the Global ID
            sb.append(_clientID).append("_").append(_serviceID);
            break;
        }

        return sb.toString();
    }

    /**
     * Returns the CSV Header of how the {@link VM#getFormatString(PlacementMapDisplayStyle)} functions arranges the 
     * VM data.
     * @return CSV formatted header describing what the columns are for the CSV display style
     */
    public static String getCSVHeaderString() {
        return "Global ID,Client ID,Service ID";
    }

    @Override
    public int hashCode() {
        return toString().hashCode();
    }

    @Override
    public boolean equals(Object obj) {
        return equals(obj, false);
    }

    public boolean equals(Object obj, boolean writeOutReason) {
        if (!(obj instanceof VM)) {
            if (writeOutReason) {
                System.out.println("Not of the VM class type");
            }
            return false;
        }
        VM v = (VM) obj;

        if (!_clientID.equals(v._clientID)) {
            if (writeOutReason) {
                System.out.println("Client IDs do not match; this." + _clientID + " != " + v._clientID);
            }
            return false;
        }

        if (!_serviceID.equals(v._serviceID)) {
            if (writeOutReason) {
                System.out.println("Service IDs do not match; this." + _serviceID + " != " + v._serviceID);
            }
            return false;
        }

        if (!_globalID.equals(v._globalID)) {
            if (writeOutReason) {
                System.out.println("Global IDs do not match; this." + _globalID + " != " + v._globalID);
            }
            return false;
        }

        return true;
    }
}
