package vagabond.pieces;

import java.util.ArrayList;
import java.util.Hashtable;
import java.util.Map.Entry;

import ilog.concert.IloException;
import ilog.concert.IloIntMap;
import vagabond.enums.PlacementMapDisplayStyle;
import vagabond.keys.MachineAndClientKey;
import vagabond.singleton.VagabondSettings;
import vagabond.timing.TimingManager;

/**
 * Provides matrix like access where a Machine ID and a Client ID maps to how many VMs of that Client reside on 
 * that Machine.
 * @author Jonathan Shahen
 *
 */
public class SumOfClientVMsPerMachine {
    /**
     * Setting to make toString O(1) time; it limits the maximum number of machines/clients that can be printed out.
     * Protects performance from accidentally writing this out in a log message.
     */
    public static Integer TOSTRING_LIMIT = 20;

    private TimingManager timing = null;
    private VagabondSettings settings = null;
    private Hashtable<MachineAndClientKey, Integer> _matrix = null;

    private Integer _numberOfMachines = null;
    private Integer _numberOfClients = null;
    private Integer _numberOfVMs = null;

    /**
     * Calculates the sum for each client on each machine here
     * @param place the PlacementMap that holds where VMs are stored
     */
    public SumOfClientVMsPerMachine(PlacementMap place) {
        settings = VagabondSettings.getInstance();
        timing = settings.timing;

        /* Timing */timing.startTimer("SumOfClientVMsPerMachine::constructor(PlacementMap)");

        _numberOfClients = place.getNumberOfClients();
        _numberOfMachines = place.getNumberOfMachines();
        _numberOfVMs = place.getNumberOfVMs();
        _matrix = new Hashtable<>(_numberOfMachines * _numberOfClients);

        for (Machine machine : place._machines) {
            for (VM vm : machine._vms) {
                MachineAndClientKey key = new MachineAndClientKey(machine._machineID, vm._clientID);
                Integer sum = _matrix.getOrDefault(key, 0);
                sum++;
                _matrix.put(key, sum);
            }
        }

        /* Timing */timing.stopTimer("SumOfClientVMsPerMachine::constructor(PlacementMap)");
    }

    public SumOfClientVMsPerMachine(IloIntMap cplexP1, ArrayList<Integer> sortedClientIDs) throws IloException {
        settings = VagabondSettings.getInstance();
        timing = settings.timing;

        /* Timing */timing.startTimer("SumOfClientVMsPerMachine::constructor(IloIntVarMap,ArrayList<Integer>)");

        if (sortedClientIDs.size() != cplexP1.getSub(1).getSize()) { throw new IllegalArgumentException(
                "Sorted Client IDs must be the same size as the CPLEX P1 variable!"); }

        _numberOfClients = sortedClientIDs.size();
        _numberOfMachines = cplexP1.getSize();
        _matrix = new Hashtable<>(_numberOfMachines * _numberOfClients);

        int vmCount = 0;
        for (int m = 1; m <= _numberOfMachines; m++) {
            IloIntMap row = cplexP1.getSub(m);
            for (int c = 1; c <= _numberOfClients; c++) {
                MachineAndClientKey key = new MachineAndClientKey(m - 1, sortedClientIDs.get(c - 1));
                _matrix.put(key, row.get(c));
                vmCount += row.get(c);
            }
        }

        _numberOfVMs = vmCount;

        /* Timing */timing.stopTimer("SumOfClientVMsPerMachine::constructor(IloIntVarMap,ArrayList<Integer>)");
    }

    public Integer getNumberOfMachines() {
        return _numberOfMachines;
    }

    public Integer getNumberOfClients() {
        return _numberOfClients;
    }

    public Integer getNumberOfVMs() {
        return _numberOfVMs;
    }

    /**
     * Returns the number of VMs owned by clientID that reside on machineID.
     * @param machineID the ID of the Machine to look on 
     * @param clientID the ID of the Client who own VMs
     * @return number of VMs owned by clientID on Machine machineID; default is 0
     */
    public Integer get(Integer machineID, Integer clientID) {
        if (machineID == null
                || clientID == null) { throw new IllegalArgumentException("Machine ID and Client Id cannot be null"); }

        return _matrix.getOrDefault(new MachineAndClientKey(machineID, clientID), 0);
    }

    public Integer get(MachineAndClientKey key) {
        if (key == null) { throw new IllegalArgumentException("The Key cannot be null"); }

        return _matrix.getOrDefault(key, 0);
    }

    public Integer put(Integer machineID, Integer clientID, Integer value) {
        if (machineID == null || clientID == null
                || value == null) { throw new IllegalArgumentException("MAchine ID and Client Id cannot be null"); }

        return _matrix.put(new MachineAndClientKey(machineID, clientID), value);
    }

    @Override
    public String toString() {
        StringBuilder sb = new StringBuilder();

        sb.append("VM Sum PlacementMap(").append(_numberOfMachines).append(" Machines; ")
                .append(_numberOfClients)
                .append(" Clients; ").append(_numberOfVMs).append(" Total VMS)\n");

        sb.append(getFormatString(PlacementMapDisplayStyle.VAGABOND, TOSTRING_LIMIT));

        return sb.toString();
    }

    public String getFormatString(PlacementMapDisplayStyle displayStyle, int limit) {
        StringBuilder sb = new StringBuilder();

        String sep = "\t";

        int limitMachines = _numberOfMachines;
        int limitClients = _numberOfClients;
        String limitClientsStr = "";
        String limitMachinesStr = "";

        if (limit != 0) {
            if (limit < _numberOfClients) {
                limitClients = limit;
                limitClientsStr = "... Skipping " + (_numberOfClients - limit) + " Clients";
            }
            if (limit < _numberOfMachines) {
                limitMachines = limit;
                limitMachinesStr = "... Skipping " + (_numberOfMachines - limit) + " Machines";
            }
        }

        if (displayStyle == PlacementMapDisplayStyle.CSV) {
            sep = ",";
        }
        // Display a header
        sb.append("Machines").append(sep);

        for (int c = 0; c < limitClients; c++) {
            sb.append("C").append(c).append(sep);
        }
        sb.append(limitClientsStr).append("\n");

        for (int m = 0; m < limitMachines; m++) {
            if (displayStyle != PlacementMapDisplayStyle.CSV) {
                sb.append("Machine ");
            }
            sb.append(m).append(sep);

            for (int c = 0; c < limitClients; c++) {
                sb.append(get(m, c)).append(sep);
            }
            sb.append("\n");
        }
        sb.append(limitMachinesStr);

        return sb.toString();
    }

    public String toMatrixString(ArrayList<Integer> clientIDs) {
        StringBuilder sb = new StringBuilder();

        sb.append("[\n");

        boolean firstLoop;
        for (int m = 0; m < _numberOfMachines; m++) {
            firstLoop = true;
            for (Integer c : clientIDs) {
                if (firstLoop) {
                    sb.append("\t[");
                } else {
                    sb.append(",");
                }
                sb.append(get(m, c));

                // firstLoop event only happens once
                firstLoop = false;
            }
            sb.append("]\n");
        }

        sb.append("\n]");

        return sb.toString();
    }

    protected SumOfClientVMsPerMachine() {
    }

    public static SumOfClientVMsPerMachine empty(Integer numberOfMachines, Integer numberOfClients) {
        SumOfClientVMsPerMachine sum = new SumOfClientVMsPerMachine();
        sum.settings = VagabondSettings.getInstance();
        sum.timing = sum.settings.timing;

        sum._numberOfClients = numberOfClients;
        sum._numberOfMachines = numberOfMachines;
        sum._numberOfVMs = 0;
        sum._matrix = new Hashtable<>(sum._numberOfMachines * sum._numberOfClients);

        return sum;
    }

    @Override
    public boolean equals(Object obj) {
        if (!(obj instanceof SumOfClientVMsPerMachine)) {
            System.out.println("Not the proper object class");
            return false;
        }

        SumOfClientVMsPerMachine s = (SumOfClientVMsPerMachine) obj;

        if (s._numberOfClients != _numberOfClients) {
            System.out.println("Number of Clients are not the same");
            return false;
        }
        if (s._numberOfMachines != _numberOfMachines) {
            System.out.println("Number of Machines are not the same");
            return false;
        }
        if (s._numberOfVMs != _numberOfVMs) {
            System.out.println("Number of VMs are not the same");
            return false;
        }

        for (Entry<MachineAndClientKey, Integer> sEntry : s._matrix.entrySet()) {
            if (!sEntry.getValue().equals(get(sEntry.getKey()))) {
                System.out.println("[s to this] " + sEntry.getKey() + ": " + sEntry.getValue() + " != "
                        + get(sEntry.getKey()));
                return false;
            }
        }

        for (Entry<MachineAndClientKey, Integer> sEntry : _matrix.entrySet()) {
            if (!sEntry.getValue().equals(s.get(sEntry.getKey()))) {
                System.out.println("[this to s] " + sEntry.getKey() + ": " + sEntry.getValue() + " != "
                        + s.get(sEntry.getKey()));
                return false;
            }
        }

        return true;
    }
}
