package vagabond.pieces;

import java.util.logging.Logger;

import vagabond.helper.RotatingArray;
import vagabond.singleton.VagabondSettings;

/**
 * Contains a list of EPOCHs and uses this to determine the information leakage. 
 * To save computer memory only the most recent EPOCHS are saved (determined by the Sliding Window setting).
 * @author Jonathan Shahen
 *
 */
public class EpochHistory {
    public static Logger logger;
    // public RotatingArray<PlacementMap> _placementMapHistory;
    public PlacementMap _latestPlacementMap = null;
    public RotatingArray<ClientToClientInformationLeakage> _informationLeakageHistory;
    public ClientToClientInformationLeakage _sumOfInformationLeakage = null;
    public Integer _slidingWindow = null;

    public Integer _latestNumberOfMoves = null;

    public EpochHistory(Integer slidingWindow) {
        logger = VagabondSettings.getInstance().getLogger();
        // _placementMapHistory = new RotatingArray<>(slidingWindow);
        _informationLeakageHistory = new RotatingArray<>(slidingWindow);
        _slidingWindow = slidingWindow;
    }

    /**
     * Get the ClientToClientInformationLeakage stored at a relative epoch: epoch=0 is the current epoch, 
     * epoch=2 is 2 epochs ago.
     * <p>
     * Please note: ClientToClientInformationLeakage get pushed back in time every time {@link #add} is called and thus
     * erases the last element in the list, when the list is full.
     * @param epoch The relative epoch that the PlacementMap was stored.
     * @return the PlacementMap at the relative epoch (0 is the top, 1 is 1 epoch into the past)
     */
    public ClientToClientInformationLeakage get(int epoch) {
        return _informationLeakageHistory.get(epoch);
    }

    /**
     * Adds a PlacementMap to the start of the history (to the most recent slot)
     * @param placementMap complete PlacementMap to be added (cannot be NULL)
     */
    public void add(PlacementMap placementMap) {
        if (placementMap == null) { throw new IllegalArgumentException("PlacementMap cannot be NULL!"); }

        /**
         * MUST DO THIS BEFORE add(ClientToClientInformationLeakage)
         */
        if (_latestPlacementMap != null) {
            _latestNumberOfMoves = _latestPlacementMap.countNumberOfMoves(placementMap);
            // System.out.println("Number of Moves: " + _latestNumberOfMoves);
        } else {
            _latestNumberOfMoves = 0;
            // System.out.println("[NULL] Number of Moves: " + _latestNumberOfMoves);
        }
        // System.out.println("_latest: " + _latestPlacementMap);
        // System.out.println("placementMap: " + placementMap);

        ClientToClientInformationLeakage tmp = new ClientToClientInformationLeakage(placementMap);

        add(tmp); // nullifies _latestPlacementMap
        _latestPlacementMap = placementMap;
    }

    /**
     * Adds a ClientToClientInformationLeakage to the start of the history and adds it to the sum of information leaks
     * and removes the last element in the history (taken from all of the capacity)
     * @param tmp Client To Client Information Leakage to be added to the history
     */
    public void add(ClientToClientInformationLeakage tmp) {
        if (tmp == null) { throw new IllegalArgumentException("ClientToClientInformationLeakage cannot be NULL!"); }
        _latestPlacementMap = null;

        // System.out.println("_sumOfInformationLeakage:\n" + _sumOfInformationLeakage);

        if (_sumOfInformationLeakage == null) {
            _sumOfInformationLeakage = new ClientToClientInformationLeakage(tmp);
        } else {
            _sumOfInformationLeakage.addTo(tmp);
        }

        // System.out.println("_sumOfInformationLeakage.addTo:\n" + _sumOfInformationLeakage);

        ClientToClientInformationLeakage last = _informationLeakageHistory.getEndOfArray();

        if (last != null) {
            // System.out.println("last:\n" + last);
            _sumOfInformationLeakage.subtractFrom(last);
            // System.out.println("_sumOfInformationLeakage.subtractFrom:\n" + _sumOfInformationLeakage);
        }

        _informationLeakageHistory.pushToFirst(tmp);
    }

    /**
     * Adds all of the PlacementMaps from the input EpochHistory into this instance. 
     * Adds the PlacementMaps in reverse order so that they get stored in the same orientation as the input 
     * EpochHistory.
     * @param epochHistory rotating array of PlacementMaps to add into this instance of EpochHistory
     */
    public void add(EpochHistory epochHistory) {
        if (epochHistory == null) { throw new IllegalArgumentException("PlacementMap cannot be NULL!"); }

        if (epochHistory.size() == 0) {
            logger.info("Adding 0 PlacementMaps to this EpochHistory object because there are no PlacementMap"
                    + " to add from this EpochHistory object: " + epochHistory);
        }

        /**
         * MUST DO THIS BEFORE add(ClientToClientInformationLeakage)
         */
        if (_latestPlacementMap != null && epochHistory.size() == 1) {
            _latestNumberOfMoves = _latestPlacementMap.countNumberOfMoves(epochHistory._latestPlacementMap);
            System.out.println("Number of Moves: " + _latestNumberOfMoves);
        } else {
            _latestNumberOfMoves = epochHistory._latestNumberOfMoves;
            System.out.println(
                    "[COPY] Number of Moves: " + _latestNumberOfMoves + "; epochHistory.size()=" + epochHistory.size());
        }

        // Add each ClientToClientInformationLeakage, but in reverse order to store it the same way in this EpochHistory
        for (int i = epochHistory.size() - 1; i >= 0; i--) {
            add(epochHistory.get(i));
        }

        _latestPlacementMap = epochHistory._latestPlacementMap;
    }

    /**
     * Returns the number of PlacmentMaps it has stored
     * @return the number of PlacementMaps stored
     */
    public int size() {
        return _informationLeakageHistory.size();
    }
}
