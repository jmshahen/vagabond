package vagabond.pieces;

import java.util.ArrayList;
import java.util.Hashtable;
import java.util.logging.Logger;

import vagabond.enums.PlacementMapDisplayStyle;
import vagabond.keys.ClientToClientKey;
import vagabond.singleton.VagabondSettings;

public class ClientToClientInformationLeakage {
    /**
     * Setting to make toString O(1) time; it limits the maximum number of clients that can be printed out.
     * Protects performance from accidentally writing this out in a log message.
     */
    public static final Integer TOSTRING_LIMIT = 20;

    private static Logger logger;
    private VagabondSettings settings = null;
    private Hashtable<ClientToClientKey, Integer> _matrix;

    private Integer _numberOfClients = null;

    public PlacementMap _ref = null;
    public ArrayList<Integer> _clientIDs = null;

    private int _maxInformationLeak = -1;
    private ClientToClientKey _maxInformationLeakKey = null;

    /**
     * Calculates the sum for each client on each machine here
     * @param place the PlacementMap that holds where VMs are stored
     */
    public ClientToClientInformationLeakage(PlacementMap place) {
        settings = VagabondSettings.getInstance();
        logger = settings.getLogger();

        _ref = place;
        _numberOfClients = place.getNumberOfClients();
        _matrix = new Hashtable<>(_numberOfClients * _numberOfClients);

        if (_numberOfClients < 1) { throw new IllegalArgumentException(
                "The number of Clients must be greater than 0!"); }

        switch (settings.informationLeakageType) {
        case ReplicationAndCollaboration:
            calculateRC_ClientToClientInfoLeak();
            break;
        case NoReplicationAndCollaboration:
            // Unimplemented
            calculateNRC_ClientToClientInfoLeak();
            // break; // TODO remove the comment once calculateNRC_ClientToClientInfoLeak is implemented
        case NoReplicationAndNoCollaboration:
            // Unimplemented
            calculateNRNC_ClientToClientInfoLeak();
            // break; // TODO remove the comment once calculateNRNC_ClientToClientInfoLeak is implemented
        case ReplicationAndNoCollaboration:
            // Unimplemented
            calculateRNC_ClientToClientInfoLeak();
            // break; // TODO remove the comment once calculateRNC_ClientToClientInfoLeak is implemented
        default:
            logger.severe("Picked an unimplemented information leakage type");
            throw new IllegalArgumentException("Picked an unimplemented information leakage type");
        }

    }

    /**
     * Performs a Deep copy of an existing ClientToClientInformationLeakage
     * @param tmp item to copy into a new ClientToClientInformationLeakage
     */
    public ClientToClientInformationLeakage(ClientToClientInformationLeakage tmp) {
        settings = VagabondSettings.getInstance();
        logger = settings.getLogger();

        _ref = tmp._ref;
        _numberOfClients = tmp._numberOfClients;
        _maxInformationLeak = tmp._maxInformationLeak;
        _maxInformationLeakKey = new ClientToClientKey(tmp._maxInformationLeakKey);
        _matrix = new Hashtable<>(_numberOfClients * _numberOfClients);

        // Deep Copy the Client IDs
        _clientIDs = new ArrayList<>(_numberOfClients);
        for (Integer c : tmp._clientIDs) {
            _clientIDs.add(new Integer(c));
        }

        for (ClientToClientKey key : tmp._matrix.keySet()) {
            _matrix.put(key, new Integer(tmp._matrix.get(key)));
        }
    }

    /**
     * Calculates the &lt;R,C&gt; information leakage
     */
    private void calculateRC_ClientToClientInfoLeak() {
        SumOfClientVMsPerMachine socvpm = new SumOfClientVMsPerMachine(_ref);

        _clientIDs = new ArrayList<>(_ref._clients.keySet());
        for (int c1 = 0; c1 < _clientIDs.size() - 1; c1++) {
            for (int c2 = c1 + 1; c2 < _clientIDs.size(); c2++) {
                ClientToClientKey key = new ClientToClientKey(_clientIDs.get(c1), _clientIDs.get(c2));
                int infoLeak = 0;

                for (int m = 0; m < socvpm.getNumberOfMachines(); m++) {
                    infoLeak += socvpm.get(m, c1) * socvpm.get(m, c2);
                }

                if (infoLeak > _maxInformationLeak) {
                    _maxInformationLeak = infoLeak;
                    _maxInformationLeakKey = key;
                }

                _matrix.put(key, infoLeak);
            }
        }
    }

    /**
     * Get the Client To Client Information Leakage that is stored; O(1) look up.
     * @param clientID1 the normal client ID
     * @param clientID2 the potential adversary client ID
     * @return the stored information leakage from clientID1 to clientID2
     */
    public Integer get(Integer clientID1, Integer clientID2) {
        if (clientID1 == null || clientID2 == null) { throw new IllegalArgumentException("Client ID cannot be null"); }

        return _matrix.getOrDefault(new ClientToClientKey(clientID1, clientID2), 0);
    }

    /**
     * Returns the number of clients; Client IDs typically range from [0,getNumberOfClients())  
     * @return number of clients
     */
    public Integer getNumberOfClients() {
        return _numberOfClients;
    }

    /**
     * Returns the maximum information leakage; will be greater than or equal to 0.
     * @return max information leakage &gt;= 0
     */
    public int getMaxInformationLeak() {
        return _maxInformationLeak;
    }

    /**
     * Returns the ClientToClient Key of the maximum Information Leak
     * @return Key that stores c1 -&gt; c2 for the information leak
     */
    public ClientToClientKey getMaxInformationLeakKey() {
        return _maxInformationLeakKey;
    }

    /**
     * Calculates or returns the stored Sum/Total Information Leakage
     * @return sum/total information leakage
     */
    public int getSumInformationLeak() {

        int sum = 0;
        for (Integer c0 : _clientIDs) {
            for (Integer c1 : _clientIDs) {
                sum += get(c0, c1);
            }
        }

        return sum;
    }

    /**
     * Returns a formatted string, see {@link #getFormatString(PlacementMapDisplayStyle, int)}, with a limit of 
     * {@link #TOSTRING_LIMIT} and the style of {@link PlacementMapDisplayStyle#VAGABOND}
     */
    @Override
    public String toString() {
        StringBuilder sb = new StringBuilder();

        sb.append("Client To Client Information Leakage (").append(_numberOfClients).append(" Clients, Max Info Leak: ")
                .append(_maxInformationLeak).append(", for the client pair: ")
                .append(_maxInformationLeakKey.toString()).append(")\n");

        sb.append(getFormatString(PlacementMapDisplayStyle.VAGABOND, TOSTRING_LIMIT));

        return sb.toString();
    }

    /**
     * Returns a formatted string of all of the stored Information Leakage values for each client to client pair
     * @param displayStyle the specific style to format the returned string
     * @param limit an optional limit to reduce resources when there are a lot of clients; 0 for no limit otherwise the
     * it will limit the number of clients it loops
     * @return a specifically formatted string containing the information leakage for each client to client pair within
     * the specified limit
     */
    public String getFormatString(PlacementMapDisplayStyle displayStyle, int limit) {
        StringBuilder sb = new StringBuilder();

        String sep = "\t";

        int limitClients = _numberOfClients;

        if (limit != 0) {
            limitClients = (limit < _numberOfClients) ? limit : _numberOfClients;
        }

        if (displayStyle == PlacementMapDisplayStyle.CSV) {
            sep = ",";
        }
        // Display a header
        sb.append("  ").append(sep);

        for (int c = 0; c < limitClients; c++) {
            sb.append("C").append(c).append(sep);
        }

        sb.append("\n");

        for (int c1 = 0; c1 < limitClients; c1++) {
            if (displayStyle != PlacementMapDisplayStyle.CSV) {
                sb.append("C");
            }
            sb.append(c1).append(sep);

            for (int c2 = 0; c2 < limitClients; c2++) {
                sb.append(get(c1, c2)).append(sep);
            }
            sb.append("\n");
        }

        return sb.toString();
    }

    /**
     * Adds all of the non-zero information leakage keys from s2 to s1.
     * <pre>
     * {@code
     * for(ClientToClientKey key : s2._matrix.keySet()) {
     *     _matrix[key] += s2._matrix[key];
     * }
     * }
     * </pre>
     * @param s2 Information Leakage to add cell by cell (non-zero only for performance boost)
     */
    public void addTo(ClientToClientInformationLeakage s2) {
        if (s2._numberOfClients != _numberOfClients) { throw new IllegalArgumentException(
                "ClientToClientInformationLeakage s2 must be of the same size: " + _numberOfClients + " != "
                        + s2._numberOfClients); }

        Integer val2;
        for (ClientToClientKey key : s2._matrix.keySet()) {
            val2 = s2._matrix.get(key);

            if (!val2.equals(0)) {
                val2 = _matrix.merge(key, val2, Integer::sum);

                // Update the max Information Leak
                if (val2 > _maxInformationLeak) {
                    _maxInformationLeak = val2;
                    _maxInformationLeakKey = key;
                }
            }
        }
    }

    /**
     * Subtracts all of the non-zero information leakage keys from s2 to this
     * <pre>
     * {@code
     * for(ClientToClientKey key : s2._matrix.keySet()) {
     *     _matrix[key] -= s2._matrix[key];
     * }
     * }
     * </pre>
     * @param s2 Information Leakage to add cell by cell (non-zero only for performance boost)
     */
    public void subtractFrom(ClientToClientInformationLeakage s2) {
        if (s2._numberOfClients != _numberOfClients) { throw new IllegalArgumentException(
                "ClientToClientInformationLeakage s2 must be of the same size: " + _numberOfClients + " != "
                        + s2._numberOfClients); }

        Integer val1;
        Integer val2;

        // Reset the maximum
        _maxInformationLeak = -1;
        _maxInformationLeakKey = null;

        for (ClientToClientKey key : s2._matrix.keySet()) {
            val2 = s2._matrix.get(key);

            if (!val2.equals(0)) {
                val1 = _matrix.get(key) - val2;
                _matrix.put(key, val1);
            } else {
                val1 = _matrix.get(key);
            }

            if (val1 > _maxInformationLeak) {
                _maxInformationLeak = val1;
                _maxInformationLeakKey = key;
            }
        }
    }

    public String toMatrixString(ArrayList<Integer> clientIDs) {
        StringBuilder sb = new StringBuilder();

        sb.append("[\n");

        boolean firstLoop;
        for (Integer c0 : clientIDs) {
            firstLoop = true;
            for (Integer c1 : clientIDs) {
                if (firstLoop) {
                    sb.append("\t[");
                } else {
                    sb.append(",");
                }
                sb.append(get(c0, c1));

                // firstLoop event only happens once
                firstLoop = false;
            }
            sb.append("]\n");
        }

        sb.append("\n]");

        return sb.toString();
    }

    /**
     * Calculates the &lt;R,NC&gt; information leakage
     */
    private void calculateRNC_ClientToClientInfoLeak() {
        // TODO Create this method
    }

    /**
     * Calculates the &lt;NR,C&gt; information leakage
     */
    private void calculateNRC_ClientToClientInfoLeak() {
        // TODO Create this method
    }

    /**
     * Calculates the &lt;NR,NC&gt; information leakage
     */
    private void calculateNRNC_ClientToClientInfoLeak() {
        // TODO Create this method
    }
}
