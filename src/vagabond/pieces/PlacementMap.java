package vagabond.pieces;

import java.io.BufferedWriter;
import java.io.IOException;
import java.util.*;

import vagabond.enums.PlacementMapDisplayStyle;

/**
 * A Cluster is a list of machines and VMs that need to placed and migrated according to certain conditions
 * @author Jonathan Shahen
 *
 */
public class PlacementMap {
    /**
     * Setting to make toString O(1) time; it limits the maximum number of machines that can be printed out.
     * Protects performance from accidentally writing this out in a log message.
     */
    public static Integer TOSTRING_MAXMACHINES = 100;

    /**
     * A list of all Machines in the system. Stored with the Machine ID as its index 
     */
    public ArrayList<Machine> _machines;
    /**
     * A list of all of the Clients in the system. Stored in this format: Client ID =&gt; Client Object
     */
    public Hashtable<Integer, Client> _clients;
    /**
     * A list of all of the VMs in the system; Stored with this format: Global VM ID =&gt; VM Object
     */
    public Hashtable<Integer, VM> _vms;
    /**
     * Stores the next available Global VM ID (there are functions to deal with this)
     */
    private Integer _nextAvailableVMID = 0;

    /**
     * Creates a PlacementMap and initializes all of the machines
     * @param numOfMachines The number of machines that the system will have; must be greater than 0
     * @param slotsPerMachines The number of slots that each machine will have (machines are homogeneous for now); must be greater than 0
     * @throws IllegalArgumentException If numOfMachines &lt; 1 or slotsPerMachines &lt; 1
     */
    public PlacementMap(Integer numOfMachines, Integer slotsPerMachines) throws IllegalArgumentException {
        // ERROR CHECKING
        if (numOfMachines < 1) { throw new IllegalArgumentException("numOfMachines cannot be less than 1!"); }
        if (slotsPerMachines < 1) { throw new IllegalArgumentException("slotsPerMachines cannot be less than 1!"); }

        _machines = new ArrayList<Machine>();
        _clients = new Hashtable<Integer, Client>();
        _vms = new Hashtable<Integer, VM>();

        for (int m = 0; m < numOfMachines; m++) {
            _machines.add(new Machine(m, slotsPerMachines));
        }
    }

    /**
     * Returns the number of Machines in the system
     * @return number of machines
     */
    public Integer getNumberOfMachines() {
        return _machines.size();
    }

    /**
     * Returns the number of VMs in the system
     * @return number of VMs
     */
    public Integer getNumberOfVMs() {
        return _vms.size();
    }

    /**
     * Returns the number of clients in the system
     * @return the number of clients
     */
    public Integer getNumberOfClients() {
        return _clients.size();
    }

    /**
     * Take a peak at the next available VM ID, this DOES NOT change the value
     * @return the integer that will be used for the next VM ID
     */
    public Integer peakNextAvailableVMID() {
        return _nextAvailableVMID;
    }

    /**
     * Get the next available global VM ID and increase the value 
     * @return the VM ID
     */
    public Integer getNextAvailableVMID() {
        int out = _nextAvailableVMID;
        _nextAvailableVMID++;
        return out;
    }

    /**
     * Simple function to check if a machine is full
     * 
     * @param machineID The Machine ID
     * @return TRUE if the machine is full; FALSE if there is space for 1 or more VMs
     * @throws IllegalArgumentException Throws if the machineID is &lt; 0 or the machineID &gt; the greatest Machine ID
     */
    public Boolean isMachineFull(Integer machineID) throws IllegalArgumentException {
        if (machineID >= _machines.size() || machineID < 0) { throw new IllegalArgumentException(
                "No machine exists with that ID(" + machineID + "), it must be in the range of " +
                        "0 (inclusive) and " + _machines.size() + "(exclusive)"); }

        return _machines.get(machineID).isFull();
    }

    /**
     * Returns if all machines are full
     * @return TRUE if all machines are full; FALSE if there exists at least 1 spot in 1 machine
     */
    public Boolean isFull() {
        Boolean result = Boolean.TRUE;
        for (Machine m : _machines) {
            result = result && m.isFull();
        }
        return result;
    }

    /**
     * Display a readable Placement Map, runs in O(1) time as it is limited to the number of machines
     * @return A constant time string that is small enough to be included in the log file without affecting performance
     */
    @Override
    public String toString() {
        StringBuilder sb = new StringBuilder();

        sb.append("PlacementMap(").append(getNumberOfMachines()).append(" Machines; ").append(getNumberOfClients())
                .append(" Clients; ").append(getNumberOfVMs()).append(" Total VMS)\n");

        int i = 0;
        for (Machine m : _machines) {
            sb.append(m.toString()).append("\n");

            i++;
            if (i == TOSTRING_MAXMACHINES) {
                sb.append("... Not Displaying " + (getNumberOfMachines() - TOSTRING_MAXMACHINES) + " Machines");
                break;
            }
        }

        return sb.toString();
    }

    /**
     * Adds a newly create VM to a machine in the system
     * @param vm the newly created VM; should have all of its values already set
     * @param machineID the machine ID that the VM will be stuck in
     * @throws IllegalArgumentException Is thrown if any input is NULL or if the machine is Full or if the VM ID is already in use in the system 
     */
    public void addNewVMToMachine(VM vm, Integer machineID) throws IllegalArgumentException {
        if (vm == null || machineID == null) { throw new IllegalArgumentException("Arguments cannot be null!"); }
        if (isMachineFull(machineID)) { throw new IllegalArgumentException(
                "The machine " + machineID + " is full, you cannot add VMs to it!"); }

        if (_vms.containsKey(vm._globalID)) { throw new IllegalArgumentException(
                "VM with the ID " + vm._globalID + " already exists"); }

        // Add the VM to the Client's list of VMs
        Client c = getClient(vm._clientID);
        c.addVM(vm);

        // Add the VM to the Global List of VMs
        _vms.put(vm._globalID, vm);

        // Add the VM to the Machine
        _machines.get(machineID).addVM(vm);
    }

    /**
     * Returns a client that is attached to the ID clientID or creates a new Client and adds it to the system
     * @param clientID unique client identifier for a client
     * @return the client (a newly created client if one is not found)
     */
    public Client getClient(Integer clientID) {
        Client c = _clients.get(clientID);

        // Setup a new client
        if (c == null) {
            c = new Client(clientID);
            _clients.put(clientID, c);
        }
        return c;
    }

    /**
     * VMs are created with a Global VM ID, Client ID, and a Service ID which is a unique VM ID within a client (usually just counts up)
     * @param clientID The ID that identifies the client; will create a client if one doesn't exists
     * @return The Service ID that should be used to create a VM
     */
    public Integer getNextAvailableServiceID(Integer clientID) {
        Client c = getClient(clientID);
        return c.getNextAvailableServiceID();
    }

    /**
     * Finds the first machine that is not full
     * @return machine ID or null if they are all full
     */
    public Integer getFirstFreeMachineID() {
        for (int m = 0; m < _machines.size(); m++) {
            if (!_machines.get(m).isFull()) { return m; }
        }
        return null;
    }

    /**
     * Get a formatted string that is not limited. Used for writing to the file disk.
     * WARNING: this is a potentially expensive operation.
     * @param displayStyle The display style that the string will be formatted with
     * @return formatted PlacementMap string in a particular style
     */
    public String getFormatString(PlacementMapDisplayStyle displayStyle) {
        StringBuilder sb = new StringBuilder();

        if (displayStyle == PlacementMapDisplayStyle.CSV) {
            sb.append(Machine.getCSVHeaderString()).append("\n");
        }

        for (Machine m : _machines) {
            sb.append(m.getFormatString(displayStyle));
            if (displayStyle != PlacementMapDisplayStyle.CSV) {
                sb.append("\n");
            }
        }

        return sb.toString();
    }

    /**
     * To prevent memory from becoming a problem this method writes out chunks for the PlacementMap to file 
     * @param writer The writer that must be opened beforehand
     * @param displayStyle The style of the written PlacementMap
     * @throws IOException Occurs from BufferedWriter
     */
    public void writeOut(BufferedWriter writer, PlacementMapDisplayStyle displayStyle) throws IOException {
        if (displayStyle == PlacementMapDisplayStyle.CSV) {
            writer.write(Machine.getCSVHeaderString());
            writer.write("\n");
        }

        for (Machine m : _machines) {
            m.writeOut(writer, displayStyle);
        }
    }

    /**
     * Get the VMs of a client on this machine. Return's null if no such machine exists.
     * 
     * @param mID the machine's ID
     * @param cID the client's ID
     * @return a list of VMs
     */
    public List<VM> getClientVMsOnMachine(Integer mID, Integer cID) {
        Machine theMachine = null;
        for (Iterator<Machine> i = _machines.iterator(); i.hasNext();) {
            Machine m = i.next();
            if (m._machineID.equals(mID)) {
                theMachine = m;
                break;
            }
        }

        if (theMachine == null)
            return null;

        List<VM> ret = new LinkedList<VM>();
        for (Iterator<VM> i = theMachine._vms.iterator(); i.hasNext();) {
            VM avm = i.next();
            if (avm._clientID.equals(cID))
                ret.add(avm);
        }

        return ret;
    }

    /**
     * Returns the maximum number of VMs any Client has
     * @return max number of vms any client has
     */
    public int getMaxNumberOfVMsPerClient() {
        int max = 0;
        for (Map.Entry<Integer, Client> c : _clients.entrySet()) {
            if (max < c.getValue().getNumberOfVMs()) {
                max = c.getValue().getNumberOfVMs();
            }
        }
        return max;
    }

    public int getMinNumberOfVMsPerClient() {
        int min = Integer.MAX_VALUE;
        for (Map.Entry<Integer, Client> c : _clients.entrySet()) {
            if (min > c.getValue().getNumberOfVMs()) {
                min = c.getValue().getNumberOfVMs();
            }
        }
        return min;
    }

    /**
     * Returns the maximum number of slots any Machine has
     * @return max number of slots any machine has
     */
    public int getMaxNumberOfSlotsPerMachine() {
        int max = 0;
        for (Machine m : _machines) {
            if (max < m._maxSpots) {
                max = m._maxSpots;
            }
        }
        return max;
    }

    /**
     * Returns an ordered list of Client IDs, this allows for placing client information in reproducible areas
     * <p>
     * An example is converting Clients into a matrix of client verses client information leakage, the position of the 
     * columns and rows have specific means to which clients they are referring to, if the order is not preserved, then 
     * the information is jumbled. 
     * @return a sorted ArrayList of Client IDs
     */
    public ArrayList<Integer> getSortedClientIDs() {
        ArrayList<Integer> sortedList = new ArrayList<>(_clients.keySet());
        Collections.sort(sortedList);
        return sortedList;
    }

    public static PlacementMap sameSizeAs(PlacementMap p) {
        PlacementMap place = new PlacementMap(p.getNumberOfMachines(), p.getMaxNumberOfSlotsPerMachine());
        return place;
    }

    @Override
    public boolean equals(Object obj) {
        if (!(obj instanceof PlacementMap)) {
            System.out.println("Not the proper PlacementMap object class");
            return false;
        }

        PlacementMap p = (PlacementMap) obj;

        if (p._clients.size() != getNumberOfClients()) {
            System.out.println("Number of Clients are not the same");
            return false;
        }
        if (p._machines.size() != getNumberOfMachines()) {
            System.out.println("Number of Machines are not the same");
            return false;
        }
        if (p._vms.size() != getNumberOfVMs()) {
            System.out.println("Number of VMs are not the same");
            return false;
        }

        for (int m = 0; m < getNumberOfMachines(); m++) {
            if (!_machines.get(m).equals(p._machines.get(m))) {
                System.out.println("Machine ID " + m + " is not equal!");
                System.out.println("Our Machine: " + _machines.get(m));
                System.out.println("Their Machine: " + p._machines.get(m));

                return false;
            }
        }

        for (Integer cID : _clients.keySet()) {
            if (!p._clients.containsKey(cID)) {
                System.out.println("obj Does not have the Client ID: " + cID);
                return false;
            }

            if (!_clients.get(cID).equals(p._clients.get(cID))) {
                System.out.println("Client ID " + cID + " is not equal");
                return false;
            }
        }

        for (Integer vmID : _vms.keySet()) {
            if (!p._vms.containsKey(vmID)) {
                System.out.println("obj Does not contain the VM ID: " + vmID);
                return false;
            }
            if (!_vms.get(vmID).equals(p._vms.get(vmID))) {
                System.out.println("VM ID " + vmID + " is not equal");
                return false;
            }
        }

        return true;
    }

    /**
     * Counts the number of moves that occur between this placement map to get to the placement map provided
     * @param placementMap the end placement map to calculate the number of moves made
     * @return how many moves it takes to get to the provided placement
     */
    public Integer countNumberOfMoves(PlacementMap placementMap) {
        // VM Global ID => Machine ID
        Hashtable<Integer, Integer> vmLocation1 = new Hashtable<Integer, Integer>(getNumberOfVMs());
        Hashtable<Integer, Integer> vmLocation2 = new Hashtable<Integer, Integer>(placementMap.getNumberOfVMs());

        // Fill vmLocation1
        for (Machine m : _machines) {
            for (VM vm : m._vms) {
                vmLocation1.put(vm._globalID, m._machineID);
            }
        }

        // Fill vmLocation2
        for (Machine m : placementMap._machines) {
            for (VM vm : m._vms) {
                vmLocation2.put(vm._globalID, m._machineID);
            }
        }

        if (vmLocation1.size() != vmLocation2
                .size()) { throw new IllegalArgumentException(
                        "Placement Maps must have the same number of VMs\nthis=" + toString() + "\ninput="
                                + placementMap); }

        Integer numMoves = 0;
        Integer tmp;
        for (Integer vmID : vmLocation1.keySet()) {
            tmp = vmLocation2.get(vmID);

            if (tmp == null) { throw new IllegalArgumentException(
                    "Unable to find VM ID " + vmID + " in the input placement map!\nthis=" + toString() + "\ninput="
                            + placementMap); }

            if (vmLocation1.get(vmID) != tmp) {
                numMoves++;
            }
        }

        return numMoves;
    }

    public double percentFilled() {
        return (double) getNumberOfVMs() / getNumberOfTotalSlots();
    }

    public int getNumberOfTotalSlots() {
        int sum = 0;

        for (Machine m : _machines) {
            sum += m._maxSpots;
        }

        return sum;
    }
}
