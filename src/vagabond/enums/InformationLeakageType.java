package vagabond.enums;

/**
 * A class that contains all of the types of Information Leakage that can be calculated
 * @author Jonathan Shahen
 *
 */
public enum InformationLeakageType {
    ReplicationAndCollaboration("R,C"),
    ReplicationAndNoCollaboration("R,NC"),
    NoReplicationAndCollaboration("NR,C"),
    NoReplicationAndNoCollaboration("NR,NC");

    String _name;

    private InformationLeakageType(String name) {
        _name = name;
    }

    @Override
    public String toString() {
        return "<" + _name + ">";
    }
}
