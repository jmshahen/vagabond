/**
 *
 * All of the TYPES and ENUMS are stored here.
 * <p>
 * To make navigating the code easier all of the ENUMS and TYPES that are used internally are stored here.
 */
package vagabond.enums;