package vagabond.enums;

/**
 * Enumeration of the possible Display Styles that a Placement Map can be written in.
 * @author Jonathan Shahen
 *
 */
public enum PlacementMapDisplayStyle {
    /**
     * The NOMAD style that was written by Jonathan Shahen for NOMAD. Loses the Global VM ID.
     */
    NOMAD,
    /**
     * A clean style that is default to Vagabond. (A favourite by the developer)
     */
    VAGABOND,
    /**
     * Comma Separate Value with each row being a VM and to following columns: Machine ID, VM ID, Client ID, Service ID
     */
    CSV,
    /**
     * Similar to the Vagabond style but with 'c' and 's' to indicate which number is the Client ID and which is the 
     * Service ID.
     */
    INFORMATIVE,
    /**
     * Similar to the Informative style but with capital letters 'C' and 'S'.
     */
    INFORMATIVE_CAP
}
