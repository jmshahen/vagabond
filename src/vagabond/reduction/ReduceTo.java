package vagabond.reduction;

import vagabond.pieces.EpochHistory;

/**
 * Interface to enforce that all Reductions comply with these standard functions.
 * @author Jonathan Shahen
 *
 */
public interface ReduceTo {
    /**
     * A timer key prefix to distinguish between multiple runs (i.e. the current Epoch number)
     * @param keyPrefix the text prefix to place at the start of every timing key
     */
    public void setTimerKeyPrefix(String keyPrefix);

    /**
     * Reduces the EpochHistory into the problem space; stores a reduced form internally 
     * @param eh EpochHistory that will be reduced
     * @return TRUE if successful, FALSE if an error occurred
     * @throws Exception will throw an exception if any error occurs during the reduction
     */
    public boolean reduce(EpochHistory eh) throws Exception;

    /**
     * A informative way of portraying information contained within; Must be O(1) time
     * @return string to print to the log
     */
    public String toString();
}
