package vagabond.reduction.nomad;

import java.io.IOException;
import java.util.ArrayList;
import java.util.concurrent.TimeUnit;
import java.util.logging.Logger;

import vagabond.pieces.EpochHistory;
import vagabond.placement.PlacementMapFile;
import vagabond.reduction.ReduceTo;
import vagabond.reduction.RunSolver;
import vagabond.singleton.VagabondSettings;
import vagabond.timing.TimingManager;

public class RunSolverNomad implements RunSolver {
    public static VagabondSettings settings;
    public static Logger logger;
    public static TimingManager timing;

    public String tp = "Epoch_Unknown";
    public ReduceToNomad rt;
    public ArrayList<Integer> listTIL;
    public ArrayList<Integer> listMCCIL;
    public ArrayList<Integer> listMovesPerEpoch;

    public RunSolverNomad() {
        settings = VagabondSettings.getInstance();
        logger = settings.getLogger();
        timing = settings.timing;
    }

    @Override
    public void setTimerKeyPrefix(String keyPrefix) {
        tp = keyPrefix;
    }

    @Override
    public boolean load(ReduceTo r) {
        rt = (ReduceToNomad) r;

        if (rt.nomadPlacement.exists() && rt.nomadSettings.exists()) { return true; }
        return false;
    }

    @Override
    public boolean checkIfSolverIsReachable() {
        return settings.getNomadExceutable().exists();
    }

    @Override
    public EpochHistory run() throws IOException, InterruptedException {
        if (!checkIfSolverIsReachable()) { throw new IOException(
                "Nomad Executable not found! Should be here: " + rt.nomadProgram.getAbsolutePath()); }

        /*TIMING*/timing.toggleTimer(tp + "RunSolverNomad::run::totalTime");

        // Delete any old results file
        if (rt.nomadResultsFile.exists()) {
            if (!rt.nomadResultsFile.delete()) { throw new IOException(
                    "Could not delete the previous Nomad results! Please delete them manually."); }
        }

        /*TIMING*/timing.toggleTimer(tp + "RunSolverNomad::run::start()");
        ProcessBuilder pb = new ProcessBuilder(rt.nomadProgram.getAbsolutePath());
        pb.directory(rt.nomadProgram.getParentFile());
        Process process = pb.start();

        logger.info("Running Nomad: " + pb.command() + " in the folder:" + pb.directory().getAbsolutePath());

        int success;

        if (settings.numberOfSecondsSolverCanRunPerEpoch > 0) {
            process.waitFor(settings.numberOfSecondsSolverCanRunPerEpoch, TimeUnit.SECONDS);
            success = process.exitValue();
        } else {
            success = process.waitFor();
        }
        /*TIMING*/timing.toggleTimer(tp + "RunSolverNomad::run::start()");

        if (success == 0) {
            // Check to make sure that the Results file was created
            if (!rt.nomadResultsFile.exists()) {
                /*TIMING*/timing.cancelTimer(tp + "RunSolverNomad::run::totalTime");
                logger.severe("ProcessBuilder: " + pb);
                logger.severe("Process: " + process);
                throw new IOException("Nomad did not produce any results file! It should be located here: "
                        + rt.nomadResultsFile.getAbsolutePath());
            }

            /*TIMING*/timing.toggleTimer(tp + "RunSolverNomad::run::readEpochHistory");
            EpochHistory eh = PlacementMapFile.readEpochHistory(rt.nomadResultsFile, settings.slidingWindow,
                    settings.stats);
            /*TIMING*/timing.toggleTimer(tp + "RunSolverNomad::run::readEpochHistory");

            /*TIMING*/timing.toggleTimer(tp + "RunSolverNomad::run::totalTime");
            return eh;
        } else {
            process.destroyForcibly();
            /*TIMING*/timing.cancelTimer(tp + "RunSolverNomad::run::totalTime");
            throw new IOException("Nomad exited with an exit value of " + success);
        }
    }

}
