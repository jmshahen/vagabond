/**
 * 
 */
package vagabond.reduction.cnfsat.tests;

import java.util.LinkedList;
import java.util.List;

import vagabond.circuit.DiffTwo;

/**
 * @author tripunit
 *
 */
public class DiffCircuitTest {

    /**
     * @param args args
     * @throws Exception bad things can happen
     */
    public static void main(String[] args) throws Exception {
        DiffTwo d = new DiffTwo(4, 6);
        List<Boolean> lb = new LinkedList<Boolean>();

        lb.add(new Boolean(true));
        lb.add(new Boolean(false));
        lb.add(new Boolean(false));
        lb.add(new Boolean(false));
        lb.add(new Boolean(true));
        lb.add(new Boolean(false));
        lb.add(new Boolean(false));
        lb.add(new Boolean(true));
        lb.add(new Boolean(false));
        lb.add(new Boolean(false));

        List<Boolean> result = d.evaluate(lb);
        System.out.println(result);

        d = new DiffTwo(7, 6);
        lb.clear();
        lb.add(new Boolean(false));
        lb.add(new Boolean(false));
        lb.add(new Boolean(false));
        lb.add(new Boolean(false));
        lb.add(new Boolean(false));
        lb.add(new Boolean(false));
        lb.add(new Boolean(true));
        lb.add(new Boolean(false));
        lb.add(new Boolean(false));
        lb.add(new Boolean(false));
        lb.add(new Boolean(true));
        lb.add(new Boolean(true));
        lb.add(new Boolean(true));

        result = d.evaluate(lb);
        System.out.println(result);
    }
}
