/**
 * 
 */
package vagabond.reduction.cnfsat.tests;

import java.util.LinkedList;
import java.util.List;

import vagabond.circuit.Circuit;
import vagabond.reduction.cnfsat.RunSolverCNFSAT;

/**
 * @author tripunit
 *
 */
public class MultCircTest {

    /**
     * @param args args
     * @throws Exception bad things may happen
     */
    public static void main(String[] args) throws Exception {
        Circuit c = RunSolverCNFSAT.multCirc(4, 6);
        List<Boolean> bl = new LinkedList<Boolean>();

        bl.clear();
        bl.add(new Boolean(false));
        bl.add(new Boolean(true));
        bl.add(new Boolean(false));
        bl.add(new Boolean(false));

        bl.add(new Boolean(false));
        bl.add(new Boolean(false));
        bl.add(new Boolean(true));
        bl.add(new Boolean(false));
        bl.add(new Boolean(true));
        bl.add(new Boolean(false));

        List<Boolean> res = c.evaluate(bl);
        System.out.println(res);

        bl.clear();
        bl.add(new Boolean(true));
        bl.add(new Boolean(true));
        bl.add(new Boolean(false));
        bl.add(new Boolean(false));

        bl.add(new Boolean(true));
        bl.add(new Boolean(false));
        bl.add(new Boolean(true));
        bl.add(new Boolean(false));
        bl.add(new Boolean(true));
        bl.add(new Boolean(true));

        res = c.evaluate(bl);
        System.out.println(res);

        c = RunSolverCNFSAT.multCirc(5, 4);
        bl.clear();
        bl.add(new Boolean(true));
        bl.add(new Boolean(true));
        bl.add(new Boolean(false));
        bl.add(new Boolean(false));
        bl.add(new Boolean(true));

        bl.add(new Boolean(false));
        bl.add(new Boolean(true));
        bl.add(new Boolean(false));
        bl.add(new Boolean(true));

        res = c.evaluate(bl);
        System.out.println(res);
    }
}
