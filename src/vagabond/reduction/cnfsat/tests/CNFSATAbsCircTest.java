/**
 * 
 */
package vagabond.reduction.cnfsat.tests;

import java.util.LinkedList;
import java.util.List;

import vagabond.circuit.Circuit;
import vagabond.reduction.cnfsat.ReduceToCNFSAT;

/**
 * @author tripunit
 * @deprecated
 */
public class CNFSATAbsCircTest {

    /**
     * @param args args
     * @throws Exception bad things can happen
     */
    public static void main(String[] args) throws Exception {
        Circuit a = ReduceToCNFSAT.absCirc(5);
        assert a.getOutputs().size() == 4;

        List<Boolean> lb = new LinkedList<Boolean>();
        lb.clear();
        lb.add(new Boolean(true));
        lb.add(new Boolean(true));
        lb.add(new Boolean(true));
        lb.add(new Boolean(true));
        lb.add(new Boolean(true));
        List<Boolean> res = a.evaluate(lb);
        System.out.println(res);

        lb.clear();
        lb.add(new Boolean(false));
        lb.add(new Boolean(false));
        lb.add(new Boolean(false));
        lb.add(new Boolean(false));
        lb.add(new Boolean(false));
        res = a.evaluate(lb);
        System.out.println(res);

        lb.clear();
        lb.add(new Boolean(false));
        lb.add(new Boolean(false));
        lb.add(new Boolean(false));
        lb.add(new Boolean(false));
        lb.add(new Boolean(true));
        res = a.evaluate(lb);
        System.out.println(res);

        a = ReduceToCNFSAT.absCirc(8);
        lb.clear();
        lb.add(new Boolean(true));
        lb.add(new Boolean(false));
        lb.add(new Boolean(false));
        lb.add(new Boolean(true));
        lb.add(new Boolean(true));
        lb.add(new Boolean(false));
        lb.add(new Boolean(true));
        lb.add(new Boolean(false));
        res = a.evaluate(lb);
        System.out.println(res);

        lb.clear();
        lb.add(new Boolean(true));
        lb.add(new Boolean(false));
        lb.add(new Boolean(false));
        lb.add(new Boolean(true));
        lb.add(new Boolean(true));
        lb.add(new Boolean(false));
        lb.add(new Boolean(true));
        lb.add(new Boolean(true));
        res = a.evaluate(lb);
        System.out.println(res);
    }

}
