package vagabond.reduction.cnfsat.tests;

import static org.junit.Assert.assertEquals;

import java.util.*;

import org.junit.Test;

import vagabond.circuit.Circuit;
import vagabond.circuit.CircuitUtils;
import vagabond.pieces.SumOfClientVMsPerMachine;
import vagabond.placement.PlacementMapExamples;
import vagabond.placement.RandomPlacement;
import vagabond.reduction.cnfsat.ReduceToCNFSAT;
import vagabond.reduction.cnfsat.RunSolverCNFSAT;
import vagabond.singleton.VagabondSettings;

public class CNFSATMigrationBudgetTests {

    @Test
    public void equalSpread() throws Exception {
        ReduceToCNFSAT r = new ReduceToCNFSAT();

        int nc = 3;
        List<Integer> nv = new LinkedList<Integer>();
        nv.add(new Integer(3));
        nv.add(new Integer(3));
        nv.add(new Integer(3));
        int ns = 3;
        int mig = 6;

        int nvbitsum = 0;
        for (int i = 0; i < nv.size(); i++) {
            nvbitsum += ReduceToCNFSAT.nbits(nv.get(i));
        }

        Circuit.resetIDs();
        Circuit c = new Circuit();
        for (int i = 0; i < ns * nvbitsum; i++) {
            c.addNewInput();
        }

        SumOfClientVMsPerMachine currp = new SumOfClientVMsPerMachine(PlacementMapExamples.equalSpreadClients());
        r.constraintMigrationBudget(c, nc, nv, ns, currp, mig);

        assertEquals(c.getInputs().size(), ns * nvbitsum);
        assertEquals(c.getOutputs().size(), 1);

        // Set up new placement via inputs to c
        // New placement is as follows. Format is: #VMs x Client ID
        /*
           Machine 0 {2 x 0, 0 x 1, 1 x 2}
           Machine 1 {0 x 0, 3 x 1, 0 x 2}
           Machine 2 {1 x 0, 0 x 1, 2 x 2}
        */

        List<Boolean> lb = new LinkedList<Boolean>();

        // Client 0
        // Machine 0
        lb.add(new Boolean(false));
        lb.add(new Boolean(true));
        // Machine 1
        lb.add(new Boolean(false));
        lb.add(new Boolean(false));
        // Machine 2
        lb.add(new Boolean(true));
        lb.add(new Boolean(false));

        // Client 1
        // Machine 0
        lb.add(new Boolean(false));
        lb.add(new Boolean(false));
        // Machine 1
        lb.add(new Boolean(true));
        lb.add(new Boolean(true));
        // Machine 2
        lb.add(new Boolean(false));
        lb.add(new Boolean(false));

        // Client 2
        // Machine 0
        lb.add(new Boolean(true));
        lb.add(new Boolean(false));
        // Machine 1
        lb.add(new Boolean(false));
        lb.add(new Boolean(false));
        // Machine 2
        lb.add(new Boolean(false));
        lb.add(new Boolean(true));

        List<Boolean> res = c.evaluate(lb);
        // System.out.println(res);
        assertEquals(res.get(0), (mig >= 4));

        // New placement is as follows. Format is: #VMs x Client ID
        // NOTE: following placement does not respect server capacity. But we don't care about
        // that constraint in this test.
        /*
           Machine 0 {0 x 0, 3 x 1, 0 x 2}
           Machine 1 {3 x 0, 0 x 1, 0 x 2}
           Machine 2 {0 x 0, 0 x 1, 3 x 2}
        */
        lb.clear();
        // Client 0
        // Machine 0
        lb.add(new Boolean(false));
        lb.add(new Boolean(false));
        // Machine 1
        lb.add(new Boolean(true));
        lb.add(new Boolean(true));
        // Machine 2
        lb.add(new Boolean(false));
        lb.add(new Boolean(false));

        // Client 1
        // Machine 0
        lb.add(new Boolean(false));
        lb.add(new Boolean(false));
        // Machine 1
        lb.add(new Boolean(true));
        lb.add(new Boolean(true));
        // Machine 2
        lb.add(new Boolean(false));
        lb.add(new Boolean(false));

        // Client 2
        // Machine 0
        lb.add(new Boolean(false));
        lb.add(new Boolean(false));
        // Machine 1
        lb.add(new Boolean(false));
        lb.add(new Boolean(false));
        // Machine 2
        lb.add(new Boolean(true));
        lb.add(new Boolean(true));

        res = c.evaluate(lb);
        System.out.println(res);
        assertEquals(res.get(0), (mig >= 6));
    }

    @Test
    public void equalSpreadWithClientConstraintTest() throws Exception {
        ReduceToCNFSAT r = new ReduceToCNFSAT();

        int nc = 3;
        List<Integer> nv = new LinkedList<Integer>();
        nv.add(new Integer(3));
        nv.add(new Integer(3));
        nv.add(new Integer(3));
        int ns = 3;
        int mig = 6;

        int nvbitsum = 0;
        for (int i = 0; i < nv.size(); i++) {
            nvbitsum += ReduceToCNFSAT.nbits(nv.get(i));
        }

        Circuit.resetIDs();
        Circuit c = new Circuit();
        for (int i = 0; i < ns * nvbitsum; i++) {
            c.addNewInput();
        }

        r.constraintTotalNumberOfClientVMs(c, nc, nv, ns);
        SumOfClientVMsPerMachine currp = new SumOfClientVMsPerMachine(PlacementMapExamples.equalSpreadClients());
        r.constraintMigrationBudget(c, nc, nv, ns, currp, mig);

        Circuit.Wire wirestoand[] = new Circuit.Wire[c.getOutputs().size()];
        for (int i = 0; i < wirestoand.length; i++) {
            wirestoand[i] = c.getOutputs().get(i);
        }

        // System.out.println(c.getOutputs().size());
        (new RunSolverCNFSAT()).addBigAnd(c, wirestoand);
        System.out.println(c.getOutputs().size());
        CircuitUtils.cnfSatToFile(c, VagabondSettings.getInstance().getCNFInstanceFile().getAbsolutePath());

    }

    @Test
    public void randomTest() throws Exception {
        int ns = 4; // # servers/machines
        int nslots = 6; // slots/machine
        int nc = 8; // # clients
        int nvperclient = 3; // #vms per client

        List<Integer> nv = new LinkedList<Integer>();
        for (int i = 0; i < nc; i++) {
            nv.add(new Integer(nvperclient));
        }

        int nvbitsum = 0;
        for (int i = 0; i < nv.size(); i++) {
            nvbitsum += ReduceToCNFSAT.nbits(nv.get(i));
        }

        assertEquals(nvbitsum, nc * ReduceToCNFSAT.nbits(nvperclient));

        Circuit.resetIDs();
        Circuit c = new Circuit();
        for (int i = 0; i < ns * nvbitsum; i++) {
            c.addNewInput();
        }

        int mig = (new Random()).nextInt((nc * nvperclient));
        SumOfClientVMsPerMachine currp = new SumOfClientVMsPerMachine(
                (new RandomPlacement()).generatePlacementMap(ns, nslots, nc, nvperclient, false));
        (new ReduceToCNFSAT()).constraintMigrationBudget(c, nc, nv, ns, currp, mig);

        CircuitUtils.cnfSatToFile(c, VagabondSettings.getInstance().getCNFInstanceFile().getAbsolutePath());

        assertEquals(c.getInputs().size(), ns * nvbitsum);
        assertEquals(c.getOutputs().size(), 1);

        // new random placement with same parameters
        SumOfClientVMsPerMachine newp = new SumOfClientVMsPerMachine(
                (new RandomPlacement()).generatePlacementMap(ns, nslots, nc, nvperclient, false));

        List<Boolean> in = new LinkedList<Boolean>();
        int neededmig = 0;
        for (int i = 0; i < nc; i++) {
            for (int j = 0; j < ns; j++) {
                neededmig += Math.abs(newp.get(j, i) - currp.get(j, i));
                for (int k = 0; k < ReduceToCNFSAT.nbits(nv.get(i)); k++) {
                    if ((newp.get(j, i).intValue() >> k) % 2 == 1) {
                        in.add(new Boolean(Boolean.TRUE));
                    } else {
                        in.add(new Boolean(Boolean.FALSE));
                    }
                }
            }
        }

        neededmig /= 2;
        System.out.println("mig = " + mig + ", need = " + neededmig);

        assertEquals(in.size(), ns * nvbitsum);

        List<Boolean> res = c.evaluate(in);
        System.out.println("res = " + res);
        assertEquals(res.get(0), mig >= neededmig);
    }
}
