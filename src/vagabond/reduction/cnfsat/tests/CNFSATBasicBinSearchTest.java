/**
 * 
 */
package vagabond.reduction.cnfsat.tests;

import java.util.LinkedList;
import java.util.List;

import vagabond.circuit.*;

/**
 * @author tripunit
 *
 */
public class CNFSATBasicBinSearchTest {

    /**
     * @param args args
     * @throws Exception bad things may happen
     */
    public static void main(String[] args) throws Exception {
        final int theval = 2000012;
        final int maxbits = 24;
        // Binary search for an integer
        Circuit.resetIDs();
        Circuit c = new Circuit();
        IntegerAsCircuit ic = new IntegerAsCircuit(theval);
        c.union(ic);
        for (int j = 0; j < ic.getOutputs().size(); j++) {
            c.removeAsOutput(ic.getOutputs().get(j));
        }

        LessEquals le = null;

        int hi = (1 << maxbits) - 1;
        int lo = 0;

        /*
        // First check if true for hi
        IntegerAsCircuit h = new IntegerAsCircuit(hi);
        c.union(h);
        for(int j = 0; j < h.getOutputs().size(); j++) {
        	c.fuse(h.getOutputs().get(j), le.getInputs().get(j + ic.getOutputs().size()));
        }
        
        List<Boolean> in = new LinkedList<Boolean>();
        List<Boolean> res = c.evaluate(in);
        
        if(res.get(0) == Boolean.FALSE) {
        	System.out.println("hi of "+hi+" is too low.");
        	return;
        }
        */

        int sol = -1;
        for (int mid = hi; hi >= lo; mid = (hi + lo) / 2) {
            System.out.print("try = " + mid + ", ");

            // Remove any existing outputs of circuit
            while (!c.getOutputs().isEmpty())
                c.removeAsOutput(c.getOutputs().get(0));

            IntegerAsCircuit m = new IntegerAsCircuit(mid);
            c.union(m);

            if (le != null) {
                c.gates.removeAll(le.gates);
                c.wires.removeAll(le.wires);
                c.inputs.removeAll(le.wires);
                c.outputs.removeAll(le.wires);
            }

            le = new LessEquals(ic.getOutputs().size(), maxbits);
            c.union(le);
            for (int j = 0; j < ic.getOutputs().size(); j++) {
                c.fuse(ic.getOutputs().get(j), le.getInputs().get(j));
            }
            for (int j = 0; j < maxbits; j++) {
                if (j < m.getOutputs().size()) {
                    c.fuse(m.getOutputs().get(j), le.getInputs().get(j + ic.getOutputs().size()));
                    c.removeAsOutput(m.getOutputs().get(j));
                } else {
                    ZeroOne z = ZeroOne.getZero();
                    c.fuse(z.getOutputs().get(0), le.getInputs().get(j + ic.getOutputs().size()));
                    c.removeAsOutput(z.getOutputs().get(0));
                }
            }

            assert c.getOutputs().size() == 1;

            List<Boolean> in = new LinkedList<Boolean>();
            List<Boolean> res = c.evaluate(in);
            res = c.evaluate(in);
            System.out.println("res: " + res);
            if (res.get(0).equals(Boolean.TRUE)) {
                sol = mid;
                hi = mid - 1;
            } else {
                lo = mid + 1;
            }
        }

        System.out.println("sol = " + sol);
    }

}
