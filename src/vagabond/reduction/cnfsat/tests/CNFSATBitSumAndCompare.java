package vagabond.reduction.cnfsat.tests;

import static org.junit.Assert.assertEquals;

import java.util.LinkedList;
import java.util.List;

import org.junit.Test;

import vagabond.circuit.*;

/**
 * @author tripunit
 * 
 * Simple test of circuit
 * @deprecated
 */
public class CNFSATBitSumAndCompare {
    /**
     * @param i the number of bit-inputs to treat as integer 0 or 1.
     * @param c the number against which to compare the sum of those i bits. c should be an upper-bound for that sum.
     * @throws Exception bad things may happen.
     * @return the CNF in DIMACS format.
     */
    public static String sumAsCnfString(int i, int c) throws Exception {
        Circuit.resetIDs();

        if (i <= 0 || c < 0) { throw new Exception("i <= 0 || c < 0 not acceptable."); }

        Circuit r = new Circuit();
        for (int j = 0; j < i; j++) {
            r.addNewInput();
        }

        List<Integer> iList = new LinkedList<Integer>();
        for (int j = 0; j < i; j++) {
            iList.add(new Integer(1));
        }

        Sum s = new Sum(iList);
        int nsoutputs = s.getOutputs().size();
        IntegerAsCircuit iac = new IntegerAsCircuit(c);
        int niacoutputs = iac.getOutputs().size();
        LessEquals leq = new LessEquals(nsoutputs, niacoutputs);

        r.union(s);
        r.union(iac);
        r.union(leq);

        for (int j = 0; j < i; j++) {
            r.fuse(r.getInputs().get(j), s.getInputs().get(j));
        }

        for (int j = 0; j < nsoutputs + niacoutputs; j++) {
            if (j < nsoutputs) {
                r.fuse(s.getOutputs().get(j), leq.getInputs().get(j));
                r.removeAsOutput(s.getOutputs().get(j));
            } else {
                r.fuse(iac.getOutputs().get(j - nsoutputs), leq.getInputs().get(j));
                r.removeAsOutput(iac.getOutputs().get(j - nsoutputs));
            }
        }

        return CircuitUtils.createCNF(r);
    }

    @Test
    void testSimpleSums() {
        assertEquals(1, 1);
        // TODO put real values and the expected output for the below
        // assertEquals(12, sumAsCnfString(12, 10));
    }

    /**
     * The main program.
     * @throws Exception bad things may happen.
     * @param args args to main
     */
    public static void main(String[] args) throws Exception {
        int i = Integer.parseInt(args[0]);
        int c = Integer.parseInt(args[1]);

        String s = sumAsCnfString(i, c);
        System.out.println(s);
    }
}
