package vagabond.reduction.cnfsat;

import java.util.*;

import vagabond.pieces.*;
import vagabond.singleton.VagabondSettings;

/**
 * Takes as input the result from the CNF-SAT solver and converts that into a PlacementMap 
 * @author Mahesh Tripunitara
 *
 */
public class PlacementMapFromCNFSATSolver {
    /**
     * 
     * @param cnfcert
     * @param nc
     * @param ns
     * @param nv
     * @return
     */
    public static List<List<Integer>> getNcvmspermachine(List<String> cnfcert, int nc, int ns, List<Integer> nv) {
        List<Integer> vars = new ArrayList<Integer>();
        int varshouldbe = 1;
        for (int i = cnfcert.indexOf("s SATISFIABLE") + 1; i < cnfcert.size(); i++) {
            String line = cnfcert.get(i);
            if (line.charAt(0) != 'v')
                break;
            String[] strvars = line.split("\\s+");
            for (int j = 0; j < strvars.length; j++) {
                try {
                    int var = Integer.parseInt(strvars[j]);
                    if (var != 0) {
                        vars.add(new Integer(var));
                        assert Math.abs(var) == varshouldbe++;
                    }
                } catch (Exception e) {
                    // continue quietly
                }
            }
        }

        List<List<Integer>> ret = new ArrayList<List<Integer>>();
        int var = 0;
        for (int client = 0; client < nc; client++) {
            List<Integer> thisclientnvm = new ArrayList<Integer>();
            for (int machine = 0; machine < ns; machine++) {
                int nvbits;
                try {
                    nvbits = ReduceToCNFSAT.nbits(nv.get(client));
                } catch (Exception e) {
                    e.printStackTrace();
                    return null;
                }

                int nvmsthismachine = 0;
                for (int i = 0; i < nvbits; i++) {
                    if (vars.get(var++) > 0) {
                        nvmsthismachine += (1 << i);
                    }
                }
                thisclientnvm.add(new Integer(nvmsthismachine));
            }
            ret.add(thisclientnvm);
        }

        return ret;
    }

    public static void sortMachines(List<Machine> l) {
        // Quick insertion sort of machines by ID
        int nmachines = l.size();
        for (int i = 1; i < nmachines; i++) {
            Machine curr = l.remove(i);
            for (int j = i; j >= 0; j--) {
                if (j == 0)
                    l.add(0, curr);
                else if (l.get(j - 1)._machineID.intValue() <= curr._machineID.intValue()) {
                    l.add(j, curr);
                    break;
                }
            }
        }
    }

    /**
     * Creates a PlacementMap by deciphering the raw output from the CNF-SAT Solver and using whatever internal
     * structures were used to map Clients and VMs in the reduction to CNF-SAT Solver's language 
     * @param reduction reference to access internal structure to help link Clients and VMs to their original IDs
     * @param rawOutputFromCNFSATSolver the textual output from the CNF-SAT solver
     * @return PlacementMap that has the same Client, VM, and machine IDs as past the input to the reduction had
     */
    public static PlacementMap getPlacementMap(ReduceToCNFSAT reduction, List<String> rawOutputFromCNFSATSolver) {

        List<List<Integer>> ncvmspermachine = getNcvmspermachine(rawOutputFromCNFSATSolver, reduction.nc, reduction.ns,
                reduction.nv);

        // System.out.println(ncvmspermachine);

        SumOfClientVMsPerMachine curr = new SumOfClientVMsPerMachine(reduction._eh._latestPlacementMap);
        PlacementMap pm = new PlacementMap(reduction.ns, VagabondSettings.getInstance().numberOfMachineSlots);

        List<Integer> clientIDs = new ArrayList<Integer>(reduction._eh._latestPlacementMap._clients.keySet());
        Collections.sort(clientIDs);
        assert clientIDs.size() == reduction.nc;

        ArrayList<Machine> allmachines = new ArrayList<Machine>();
        allmachines.addAll(reduction._eh._latestPlacementMap._machines);
        sortMachines(allmachines);

        List<List<VM>> clientpool = new ArrayList<List<VM>>(); // A pool for VMs per client to dole out to needy
                                                               // machines
        for (int client = 0; client < reduction.nc; client++) {
            Integer cid = clientIDs.get(client);
            List<VM> tothisclientpool = new ArrayList<VM>(); // need this to be concrete VMs, not just a number of them
            List<Integer> futurenvmspermachine = ncvmspermachine.get(client);
            assert futurenvmspermachine.size() == reduction.ns;
            for (int machine = 0; machine < reduction.ns; machine++) {
                // System.out.println("curr.get("+machine+","+cid+") = "+curr.get(machine,cid));
                // System.out.println("futurenvmspermachine.get("+machine+") = "+futurenvmspermachine.get(machine));
                if (futurenvmspermachine.get(machine) < curr.get(machine, cid)) {
                    // We can grab some VMs from this machine for the pool.
                    int ngrab = curr.get(machine, cid) - futurenvmspermachine.get(machine);
                    Machine themachine = allmachines.get(machine);
                    for (Iterator<VM> it = themachine._vms.iterator(); it.hasNext() && ngrab > 0;) {
                        VM v = it.next();
                        if (clientIDs.indexOf(v._clientID) != client)
                            continue;
                        tothisclientpool.add(v);
                        ngrab--;
                        // System.out.println("Adding to pool for client "+v._clientID+" the VM "+v._globalID);
                    }
                }
            }
            clientpool.add(tothisclientpool);
        }

        List<Machine> newmachines = pm._machines; // new machine config
        sortMachines(newmachines);
        for (int client = 0; client < reduction.nc; client++) {
            Integer cid = clientIDs.get(client);
            List<Integer> futurenvmspermachine = ncvmspermachine.get(client);
            for (int machine = 0; machine < reduction.ns; machine++) {
                Machine oldmachine = allmachines.get(machine);

                if (futurenvmspermachine.get(machine) >= curr.get(machine, cid)) {
                    // First add all the ones it keeps
                    for (Iterator<VM> it = oldmachine._vms.iterator(); it.hasNext();) {

                        VM v = it.next();
                        if (clientIDs.indexOf(v._clientID) != client)
                            continue;
                        pm.addNewVMToMachine(v, oldmachine._machineID);
                    }

                    // Then give some more
                    int ngive = futurenvmspermachine.get(machine) - curr.get(machine, cid);
                    List<VM> thisclientpool = clientpool.get(client);

                    while (ngive-- > 0) {
                        assert thisclientpool.size() > 0;
                        VM v = thisclientpool.remove(0);
                        pm.addNewVMToMachine(v, oldmachine._machineID);
                        // System.out.println("Added VM "+v._globalID+" to machine "+oldmachine._machineID);
                    }
                } else {
                    // You don't get to keep all of the old VMs for this client
                    int ntake = curr.get(machine, cid) - futurenvmspermachine.get(machine);
                    for (Iterator<VM> it = oldmachine._vms.iterator(); it.hasNext();) {
                        // IMPORTANT NOTE: we leverage the fact that the _vms are maintained as a **list**
                        // and not a **set**. So we assume that the iterator returns them in order!!

                        VM thevm = it.next();
                        if (clientIDs.indexOf(thevm._clientID) != client)
                            continue;

                        if (--ntake < 0) {
                            pm.addNewVMToMachine(thevm, oldmachine._machineID);
                            // System.out.println("Added VM "+thevm._globalID+" to machine "+oldmachine._machineID);
                        }
                    }
                }
            }
        }

        return pm;
    }

}
