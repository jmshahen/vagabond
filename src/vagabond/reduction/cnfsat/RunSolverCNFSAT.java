package vagabond.reduction.cnfsat;

import java.io.*;
import java.util.*;
import java.util.concurrent.TimeUnit;
import java.util.logging.Level;
import java.util.logging.Logger;

import org.apache.commons.lang3.StringUtils;

import vagabond.circuit.*;
import vagabond.pieces.EpochHistory;
import vagabond.reduction.ReduceTo;
import vagabond.reduction.RunSolver;
import vagabond.singleton.VagabondSettings;
import vagabond.timing.TimingManager;

public class RunSolverCNFSAT implements RunSolver {
    public static VagabondSettings settings;
    public TimingManager timing = null;
    public String tp = "Epoch_Unknown";
    public static Logger logger;

    String solverloc;
    String cnfloc;// OLD VALUE = new String("/home/tripunit/Desktop/instance.cnf");
    ReduceToCNFSAT _r = null;
    int EACH_DECISION_INSTANCE_TIMEOUT = -1; // Seconds. -1 means infinity
    private String commands = "";// "-t 8 ";

    public RunSolverCNFSAT() throws IOException {
        settings = VagabondSettings.getInstance();
        timing = settings.timing;
        logger = settings.getLogger();
        solverloc = settings.getCNFSolverExceutable().getAbsolutePath();
        cnfloc = settings.getCNFInstanceFile().getAbsolutePath();
    }

    @Override
    public void setTimerKeyPrefix(String keyPrefix) {
        tp = keyPrefix;
    }

    @Override
    public boolean checkIfSolverIsReachable() {
        return (new File(solverloc)).exists();
    }

    @Override
    public boolean load(ReduceTo r) {
        // Confirm that the timing manager is setup
        if (timing == null) {
            timing = VagabondSettings.getInstance().timing;
        }

        // Store a reference to r in this object.
        _r = (ReduceToCNFSAT) r;
        return true;
    }

    @Override
    public EpochHistory run() throws Exception {
        /*TIMING*/ timing.toggleTimer(tp + "RunSolverCNFSAT::run::totalTime");

        // Get CNF-SAT instance from ReduceTo argument to load().
        // Run the solver. Get raw string result. Then invoke
        // PlacementMapFromCNFSATSolver.getPlacementMap() to get a placement map.
        // Convert the placement map to an EpochHistory with 1 entry, which is that placement map.

        // Constraint Line (1) -- optimization objective
        // Setup for binary search

        Circuit c = _r._circ;
        // for each pair of clients a sum circuit
        int nclientpairs = ((_r.nc - 1) * (_r.nc)) / 2;
        Sum s[];

        try {
            s = constraintSumInfoLeak(c, _r, nclientpairs);
        } catch (Exception e) {
            e.printStackTrace();
            /*TIMING*/ timing.cancelTimer(tp + "RunSolverCNFSAT::run::totalTime");
            return null;
        }

        // At this point, we know how many output wires to AND with the <= circuit's output that's coming up
        Circuit.Wire wirestoand[];
        int orignoutputs = c.getOutputs().size();

        if (VagabondSettings.getInstance().minimizeMaxClientToClientInfoLeak) {
            wirestoand = new Circuit.Wire[orignoutputs + nclientpairs];
        } else {
            wirestoand = new Circuit.Wire[orignoutputs + 1];
        }
        for (int i = 0; i < orignoutputs; i++) {
            wirestoand[i] = c.getOutputs().get(0);
            c.removeAsOutput(wirestoand[i]);
        }

        // All the sum circuits whose outputs we want to compare against are now in s[]
        int hi;
        if (VagabondSettings.getInstance().minimizeMaxClientToClientInfoLeak) {
            int maxclientnv = -1;
            for (int i = 0; i < _r.nv.size(); i++) {
                if (_r.nv.get(i) > maxclientnv) {
                    maxclientnv = _r.nv.get(i);
                }
            }

            hi = _r._eh._sumOfInformationLeakage.getMaxInformationLeak() + maxclientnv * maxclientnv;
        } else {
            hi = _r._eh._sumOfInformationLeakage.getSumInformationLeak() + _r._eh.get(0).getSumInformationLeak();
        }

        int lo = 0;
        LessEquals le[];

        if (VagabondSettings.getInstance().minimizeMaxClientToClientInfoLeak) {
            le = new LessEquals[nclientpairs];
            for (int i = 0; i < nclientpairs; i++) {
                le[i] = null;
            }
        } else {
            le = new LessEquals[1];
            le[0] = null;
        }

        BigAndOr band[] = new BigAndOr[1];
        band[0] = null;

        List<String> cnfcert = null;
        // certificate from cnf-sat
        // Binary search
        /*TIMING*/ timing.toggleTimer(tp + "RunSolverCNFSAT::run::binarySearch::totalTime");
        String searchVal;
        int loopCount = 0;
        for (int mid = hi; hi >= lo; mid = (hi + lo) / 2) {
            searchVal = StringUtils.leftPad(loopCount + "", (int) Math.floor(Math.log10(hi)) + 1, "0") + "::mid::"
                    + StringUtils.leftPad(mid + "", (int) Math.floor(Math.log10(hi)) + 1, "0");
            /*TIMING*/ timing.toggleTimer(tp + "RunSolverCNFSAT::run::binarySearch::loopVal::" + searchVal);
            // System.out.println("mid = "+mid); System.out.flush();
            try {
                constraintInfoLeakUb(c, le, band, s, nclientpairs, mid, wirestoand, orignoutputs);

                /*TIMING*/ timing.toggleTimer(tp + "RunSolverCNFSAT::run::binarySearch::cnfSatToFile::" + searchVal);
                // Now convert to cnf, invoke solver and adjust mid as needed.
                CircuitUtils.cnfSatToFile(c, cnfloc);
                /*TIMING*/ timing.toggleTimer(tp + "RunSolverCNFSAT::run::binarySearch::cnfSatToFile::" + searchVal);

                // TODO Delete objects?

                /*TIMING*/ timing.toggleTimer(tp + "RunSolverCNFSAT::run::binarySearch::exec::" + searchVal);

                ArrayList<String> exec_cmd = new ArrayList<>();
                exec_cmd.add(solverloc);
                if (!commands.isEmpty()) {
                    exec_cmd.add(commands);
                }
                exec_cmd.add(cnfloc);
                logger.log(Level.INFO, "[CNF Solver " + tp + "] Executing command: " + exec_cmd);

                // OLD COMMAND
                // Process p = Runtime.getRuntime().exec(exec_cmd);

                ProcessBuilder pb = new ProcessBuilder(exec_cmd);
                Process p = pb.start();

                // Activate the following 'if,' if we want to put in a time-limit for every decision instance.
                if (mid < hi && EACH_DECISION_INSTANCE_TIMEOUT > 0
                        && !p.waitFor(EACH_DECISION_INSTANCE_TIMEOUT, TimeUnit.SECONDS)) {
                    // Treat this as unsat
                    p.destroyForcibly();

                    logger.info("[CNF Solver " + tp + "] Binary Search TIMOUT for: mid = " + mid + "; high=" + hi
                            + "; low=" + lo);

                    lo = mid + 1;
                    /*TIMING*/ timing.toggleTimer(tp + "RunSolverCNFSAT::run::binarySearch::exec::" + searchVal);
                    continue;
                }

                BufferedReader solveroutput = new BufferedReader(new InputStreamReader(p.getInputStream()));
                // save the certificate
                List<String> lines = new LinkedList<String>();
                boolean recordlines = false;
                int lineCount = 0;
                for (String str = ""; (str = solveroutput.readLine()) != null;) {
                    lineCount++;
                    // System.out.println(str); // DEBUG LINE
                    if (recordlines) {
                        lines.add(str);
                        if (str.contains(Integer.toString(c.getInputs().size()))) {
                            logger.fine("[CNF Solver " + tp + "] Breaking at: " + str + ", #inputs = "
                                    + c.getInputs().size());
                            break;
                        }
                        // else
                        continue;
                    }

                    // else
                    if (str.contains("s SATISFIABLE")) {
                        // Start to record sufficient # of variables
                        recordlines = true;
                    }
                }
                logger.fine("[CNF Solver " + tp + "] Counted " + Integer.toString(lineCount)
                        + " lines of output from solver.");

                p.destroyForcibly();
                /*TIMING*/ timing.toggleTimer(tp + "RunSolverCNFSAT::run::binarySearch::exec::" + searchVal);

                if (!recordlines) {
                    logger.info("[CNF Solver " + tp + "] Binary Search UNSAT for: mid = " + mid + "; high=" + hi
                            + "; low=" + lo + "; Took " + timing.getLastElapsedTimeSec() + " seconds");
                    lo = mid + 1; // unsat
                } else {
                    logger.info("[CNF Solver " + tp + "] Binary Search SAT for: mid = " + mid + "; high=" + hi
                            + "; low=" + lo + "; Took " + timing.getLastElapsedTimeSec() + " seconds");
                    cnfcert = lines;
                    hi = mid - 1;
                }

                // /*TIMING*/ timing.toggleTime("RunSolverCNFSAT::run::binarySearch::searchSatisfiable"+searchVal);
                // if (lines.contains("s SATISFIABLE")) {
                // cnfcert = lines;
                // hi = mid - 1;
                // } else {
                // lo = mid + 1;
                // }
                // /*TIMING*/ timing.toggleTime("RunSolverCNFSAT::run::binarySearch::searchSatisfiable"+searchVal);
                // /*TIMING*/
                // timing.writeOutSingle(fw,"RunSolverCNFSAT::run::binarySearch::searchSatisfiable"+searchVal);
            } catch (Exception e) {
                e.printStackTrace();
                /*TIMING*/ timing.cancelTimer(tp + "RunSolverCNFSAT::run::totalTime");
                return null;
            }
            /*TIMING*/ timing.toggleTimer(tp + "RunSolverCNFSAT::run::binarySearch::loopVal::" + searchVal);
        }
        /*TIMING*/ timing.toggleTimer(tp + "RunSolverCNFSAT::run::binarySearch::totalTime");

        // Build an epochhistory data structure as required by this function's signature
        if (cnfcert == null) {
            logger.log(Level.WARNING, "The cnfcert is Null!");
            /*TIMING*/ timing.cancelTimer(tp + "RunSolverCNFSAT::run::totalTime");
            return null;
        }
        EpochHistory ret = new EpochHistory(1);
        ret.add(PlacementMapFromCNFSATSolver.getPlacementMap(_r, cnfcert));

        /*TIMING*/ timing.toggleTimer(tp + "RunSolverCNFSAT::run::totalTime");

        return ret;
    }

    /**
     * A circuit that multiplies two non-negative integers. The first encoded in n bits, and the second in m bits.
     * 
     * @param n bits that correspond to first integer. Assumed to encode a non-negative integer.
     * @param m bits that correspond to the second integer. Assumed to encode a non-negative integer.
     * @return the multiplication circuit
     * @throws Exception bad things may happen
     */
    public static Circuit multCirc(int n, int m) throws Exception {
        Circuit c = new Circuit();
        for (int i = 0; i < n + m; i++) {
            c.addNewInput();
        }

        Circuit.Wire a[][] = new Circuit.Wire[m][n + m - 1]; // Each row treated as bit-string. The bit-strings are then
                                                             // summed.
        // Initialize a[][] with zeroes
        c.union(ZeroOne.getZero());
        c.removeAsOutput(ZeroOne.getZero().getOutputs().get(0));
        for (int i = 0; i < m; i++) {
            for (int j = 0; j < n + m - 1; j++) {
                a[i][j] = ZeroOne.getZero().getOutputs().get(0);
            }
        }

        for (int i = 0; i < m; i++) {
            for (int j = 0; j < n; j++) {
                BigAndOr b = new BigAndOr(false, 2);
                c.union(b);
                c.removeAsOutput(b.getOutputs().get(0));
                c.fuse(c.getInputs().get(n + i), b.getInputs().get(0));
                c.fuse(c.getInputs().get(j), b.getInputs().get(1));
                a[i][i + j] = b.getOutputs().get(0);
            }
        }

        List<Integer> il = new LinkedList<Integer>();
        for (int i = 0; i < m; i++) {
            il.add(new Integer(n + m - 1));
        }

        Sum s = new Sum(il);
        c.union(s);
        for (int i = 0; i < m; i++) {
            for (int j = 0; j < n + m - 1; j++) {
                c.fuse(a[i][j], s.getInputs().get(i * (n + m - 1) + j));
            }
        }

        return c;
    }

    public List<Circuit.Wire> getInWires(Circuit r, int k, int c, int nc, int ns, List<Integer> nv) throws Exception {

        List<Circuit.Wire> ret = new LinkedList<Circuit.Wire>();
        int prevcnv = 0;
        for (int i = 0; i < nc; i++) {
            if (i == c) {
                int nvbits = ReduceToCNFSAT.nbits(nv.get(i));
                for (int j = 0; j < nvbits; j++) {
                    int index = prevcnv * ns + k * nvbits + j;
                    Circuit.Wire e = r.getInputs().get(index);
                    ret.add(e);
                }
                break;
            }
            prevcnv += ReduceToCNFSAT.nbits(nv.get(i));
        }
        return ret;
    }

    public Sum[] constraintSumInfoLeak(Circuit c, ReduceToCNFSAT _r, int nclientpairs) throws Exception {
        Sum s[] = new Sum[nclientpairs];
        int sidx = 0;
        for (int c0 = 0; c0 < _r.nc; c0++) {
            for (int c1 = c0 + 1; c1 < _r.nc; c1++) {
                List<List<Circuit.Wire>> multcoutputs = new LinkedList<List<Circuit.Wire>>();

                for (int k = 0; k < _r.ns; k++) {
                    try {
                        List<Circuit.Wire> c0kw, c1kw;
                        Circuit m;

                        c0kw = getInWires(c, k, c0, _r.nc, _r.ns, _r.nv);
                        c1kw = getInWires(c, k, c1, _r.nc, _r.ns, _r.nv);
                        m = multCirc(c0kw.size(), c1kw.size());
                        multcoutputs.add(m.getOutputs());
                        c.union(m);
                        for (int l = 0; l < m.getOutputs().size(); l++) {
                            c.removeAsOutput(m.getOutputs().get(l));
                        }
                        for (int l = 0; l < c0kw.size(); l++) {
                            c.fuse(c0kw.get(l), m.getInputs().get(l));
                        }
                        for (int l = 0; l < c1kw.size(); l++) {
                            c.fuse(c1kw.get(l), m.getInputs().get(l + c0kw.size()));
                        }
                    } catch (Exception e) {
                        e.printStackTrace();
                        return null;
                    }
                }

                assert multcoutputs.size() == _r.ns;

                // Now two sums
                try {
                    List<Integer> li = new LinkedList<Integer>();
                    for (int k = 0; k < _r.ns; k++) {
                        li.add(new Integer(multcoutputs.get(k).size()));
                    }
                    Sum innersum = new Sum(li);
                    c.union(innersum);
                    for (int l = 0; l < innersum.getOutputs().size(); l++) {
                        c.removeAsOutput(innersum.getOutputs().get(l));
                    }
                    int nprevwires = 0;
                    for (int k = 0; k < _r.ns; k++) {
                        List<Circuit.Wire> ml = multcoutputs.get(k);
                        for (int l = 0; l < ml.size(); l++) {
                            c.fuse(ml.get(l), innersum.getInputs().get(l + nprevwires));
                        }
                        nprevwires += ml.size();
                    }

                    int zc0c1 = _r._eh._sumOfInformationLeakage.get(c0, c1);
                    // System.out.println("z["+c0+","+c1+"]: "+zc0c1);
                    IntegerAsCircuit circ_zc0c1 = new IntegerAsCircuit(zc0c1);
                    c.union(circ_zc0c1);
                    for (int l = 0; l < circ_zc0c1.getOutputs().size(); l++) {
                        c.removeAsOutput(circ_zc0c1.getOutputs().get(l));
                    }
                    li.clear();
                    li.add(circ_zc0c1.getOutputs().size());
                    li.add(innersum.getOutputs().size());
                    Sum outersum = new Sum(li);
                    c.union(outersum);
                    for (int l = 0; l < outersum.getOutputs().size(); l++) {
                        c.removeAsOutput(outersum.getOutputs().get(l));
                    }

                    for (int l = 0; l < circ_zc0c1.getOutputs().size(); l++) {
                        c.fuse(circ_zc0c1.getOutputs().get(l), outersum.getInputs().get(l));
                    }
                    for (int l = 0; l < innersum.getOutputs().size(); l++) {
                        c.fuse(innersum.getOutputs().get(l),
                                outersum.getInputs().get(l + circ_zc0c1.getOutputs().size()));
                    }

                    s[sidx++] = outersum;
                } catch (Exception e) {
                    e.printStackTrace();
                    return null;
                }
            }
        }

        // If we're doing total info. leak, we should add all of values in s[] up, and return a single
        // sum circuit
        if (!VagabondSettings.getInstance().minimizeMaxClientToClientInfoLeak) {
            List<Integer> li = new ArrayList<Integer>();
            for (int i = 0; i < s.length; i++) {
                li.add(new Integer(s[i].getOutputs().size()));
            }

            Sum tot = new Sum(li);
            c.union(tot);
            int totinidx = 0;
            for (int i = 0; i < s.length; i++) {
                for (int j = 0; j < s[i].getOutputs().size(); j++) {
                    c.fuse(s[i].getOutputs().get(j), tot.getInputs().get(totinidx++));
                }
            }

            for (int i = 0; i < tot.getOutputs().size(); i++) {
                c.removeAsOutput(tot.getOutputs().get(i));
            }

            s = new Sum[1];
            s[0] = tot;
        }

        return s;
    }

    public void constraintInfoLeakUb(Circuit c, LessEquals le[], BigAndOr band[], Sum s[], int nclientpairs, int ub,
            Circuit.Wire wirestoand[], int orignoutputs) throws Exception {

        for (int i = 0; i < c.getOutputs().size(); i++)
            c.removeAsOutput(c.getOutputs().get(0));

        if (band[0] != null) {
            c.removeAsOutput(band[0].getOutputs().get(0));
            c.gates.removeAll(band[0].gates);
            c.wires.removeAll(band[0].wires);
            c.inputs.removeAll(band[0].wires);
            c.outputs.removeAll(band[0].wires);
        }

        IntegerAsCircuit midc = new IntegerAsCircuit(ub);
        c.union(midc);
        for (int j = 0; j < midc.getOutputs().size(); j++) {
            c.removeAsOutput(midc.getOutputs().get(j));
        }

        for (int i = 0; i < le.length; i++) {
            if (le[i] != null) {
                c.removeAsOutput(le[i].getOutputs().get(0));
                c.gates.removeAll(le[i].gates);
                c.wires.removeAll(le[i].wires);
                c.inputs.removeAll(le[i].wires);
                c.outputs.removeAll(le[i].wires);
            }

            le[i] = new LessEquals(s[i].getOutputs().size(), midc.getOutputs().size());
            c.union(le[i]);
            for (int j = 0; j < s[i].getOutputs().size(); j++) {
                c.fuse(s[i].getOutputs().get(j), le[i].getInputs().get(j));
            }
            for (int j = 0; j < midc.getOutputs().size(); j++) {
                c.fuse(midc.getOutputs().get(j), le[i].getInputs().get(j + s[i].getOutputs().size()));
            }

            wirestoand[orignoutputs + i] = le[i].getOutputs().get(0);
        }

        band[0] = addBigAnd(c, wirestoand);
    }

    /**
     * Big AND of all the outputs of a circuit
     * 
     * @param c the circuit whose outputs we should AND. This is an in-out param. I.e., c is changed up on return.
     * @return
     */
    public BigAndOr addBigAnd(Circuit c, Circuit.Wire wirestoand[]) {
        BigAndOr band = null;
        try {
            band = new BigAndOr(false, wirestoand.length);
            c.union(band);
            for (int i = 0; i < wirestoand.length; i++) {
                c.fuse(wirestoand[i], band.getInputs().get(i));
                c.removeAsOutput(wirestoand[i]);
            }
        } catch (Exception e) {
            e.printStackTrace();
        }

        return band;
    }

}
