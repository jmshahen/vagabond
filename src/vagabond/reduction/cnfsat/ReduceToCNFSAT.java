package vagabond.reduction.cnfsat;

import java.util.*;

import vagabond.circuit.*;
import vagabond.pieces.*;
import vagabond.reduction.ReduceTo;
import vagabond.singleton.VagabondSettings;

public class ReduceToCNFSAT implements ReduceTo {
    public String tp = "Epoch_Unknown";

    @Override
    public void setTimerKeyPrefix(String keyPrefix) {
        tp = keyPrefix;
    }

    EpochHistory _eh = null; // Input epoch history; package private
    public Circuit _circ = null; // Circuit with 3 of the 4 constraints. The optimization objective is added as part of
                                 // the binary search in RunSolver...run().
    int nc = -1;
    int ns = -1; // # servers
    List<Integer> nv = null; // #VMs per client

    @Override
    public String toString() {
        StringBuilder sb = new StringBuilder();

        sb.append("ReduceToSAT {");

        if (_circ != null) {
            sb.append(_circ.toString());
        }

        sb.append("}");

        return sb.toString();
    }

    public void setEh(EpochHistory e) {
        _eh = e;
        nc = _eh._latestPlacementMap._clients.size();
        ns = _eh._latestPlacementMap._machines.size();

        List<Integer> cids = new LinkedList<Integer>(_eh._latestPlacementMap._clients.keySet());
        Collections.sort(cids);
        nv = new LinkedList<Integer>();
        for (int i = 0; i < cids.size(); i++) {
            Client c = _eh._latestPlacementMap._clients.get(cids.get(i));
            nv.add(c._vms.size());
        }
    }

    public EpochHistory getEh() {
        return _eh;
    }

    public void setNc(int c) {
        nc = c;
    }

    public int getNc() {
        return nc;
    }

    public void setNs(int s) {
        ns = s;
    }

    public int getNs() {
        return ns;
    }

    public void setNv(List<Integer> v) {
        nv = v;
    }

    public List<Integer> getNv() {
        return nv;
    }

    public void setCirc(Circuit c) {
        _circ = c;
    }

    public Circuit getCirc() {
        return _circ;
    }

    /**
     * Add the constraint to the circuit, that for each client, its total # VMs should be exactly
     * what we expect.
     * 
     * @param c the circuit to which we add this constraint
     * @param nc the number of clients
     * @param nv list of integers; #VMs per client
     * @param ns the number of servers (machines)
     * @throws Exception bad things may happen
     */
    public void constraintTotalNumberOfClientVMs(Circuit c, int nc, List<Integer> nv, int ns) throws Exception {
        int nvBitsPrevClients = 0; // convenience variable -- total #VM bits of all prior clients

        for (int i = 0; i < nc; i++) {
            int nvbits = nbits(nv.get(i)); // convenience variable -- #VM bits for this client

            // For each client, a Sum circuit
            List<Integer> li = new LinkedList<Integer>();
            for (int j = 0; j < ns; j++) {
                li.add(nvbits);
            }

            Sum s = new Sum(li);
            c.union(s);

            for (int j = 0; j < nvbits * ns; j++) {
                c.fuse(c.getInputs().get(nvBitsPrevClients * ns + j), s.getInputs().get(j));
            }

            // Then two LessThanEquals circuits
            // The first one:
            LessEquals le = new LessEquals(s.getOutputs().size(), nvbits);
            c.union(le);
            for (int j = 0; j < s.getOutputs().size(); j++) {
                c.fuse(s.getOutputs().get(j), le.getInputs().get(j));
                c.removeAsOutput(s.getOutputs().get(j));
            }

            IntegerAsCircuit iac = new IntegerAsCircuit(nv.get(i));
            assert iac.getOutputs().size() == nvbits;
            c.union(iac);

            for (int j = 0; j < iac.getOutputs().size(); j++) {
                c.fuse(iac.getOutputs().get(j), le.getInputs().get(j + s.getOutputs().size()));
                c.removeAsOutput(iac.getOutputs().get(j));
            }

            /*
            if(iac.getOutputs().size() < nvbits) {
            	// Hmm....should this ever happen? -- see assert above
            	for(int j = s.getOutputs().size() + iac.getOutputs().size(); j < s.getOutputs().size() + nvbits; j++) {
            		ZeroOne z = ZeroOne.getZero();
            		c.union(z);
            		c.fuse(z.getOutputs().get(0), le.getInputs().get(j));
            		c.removeAsOutput(z.getOutputs().get(0));
            	}
            }
            */

            // The second LessEquals circuit:
            le = new LessEquals(nvbits, s.getOutputs().size());
            c.union(le);
            for (int j = 0; j < iac.getOutputs().size(); j++) {
                c.fuse(iac.getOutputs().get(j), le.getInputs().get(j));
                c.removeAsOutput(iac.getOutputs().get(j)); // Redundant
            }

            /*
            if(iac.getOutputs().size() < nvbits) {
            	// Hmm....should this ever happen? -- see assert above
            	for(int j = iac.getOutputs().size(); j < nvbits; j++) {
            		ZeroOne z = ZeroOne.getZero();
            		c.union(z);
            		c.fuse(z.getOutputs().get(0), le.getInputs().get(j));
            		c.removeAsOutput(z.getOutputs().get(0));
            	}
            }
            */

            for (int j = 0; j < s.getOutputs().size(); j++) {
                c.fuse(s.getOutputs().get(j), le.getInputs().get(j + nvbits));
            }

            // At this point, the only two outputs of c should be the outputs of the le circuits above
            // Assume that those are AND-ed later

            assert c.getOutputs().size() == 2 * (i + 1);

            nvBitsPrevClients += nvbits;
        }
    }

    /**
     * Add the constraint to the circuit that the server's capacity should not be exceed.
     * 
     * @param c the circuit to which we add this constraint
     * @param nc the number of clients
     * @param nv list of integers; #VMs per client
     * @param ns number of servers (machines)
     * @param scapa the capacity of each server
     * @throws Exception bad things may happen
     */
    public void constraintServerCapacity(Circuit c, int nc, List<Integer> nv, int ns, int scapa) throws Exception {
        for (int i = 0; i < ns; i++) {
            // For each server, a Sum circuit
            List<Integer> li = new LinkedList<Integer>();
            for (int j = 0; j < nc; j++) {
                li.add(new Integer(nbits(nv.get(j))));
            }

            Sum s = new Sum(li);
            c.union(s);
            for (int j = 0; j < s.getOutputs().size(); j++) {
                c.removeAsOutput(s.getOutputs().get(j));
            }

            // Now hook the input wires of c to the input wires of s
            int nvbitsprevclients = 0;
            for (int j = 0; j < nc; j++) {
                int nvbits = nbits(nv.get(j));
                // Wires for this client start at nvbitsprevclients*ns
                // So, wires for this server for this client start at nvbitsprevclients*ns + i*nvbits

                for (int k = 0; k < nvbits; k++) {
                    c.fuse(c.getInputs().get(k + nvbitsprevclients * ns + i * nvbits),
                            s.getInputs().get(nvbitsprevclients + k));
                }

                nvbitsprevclients += nvbits;
            }

            // Now an le circuit
            Circuit le = new LessEquals(s.getOutputs().size(), nbits(scapa));
            c.union(le);
            for (int j = 0; j < s.getOutputs().size(); j++) {
                c.fuse(s.getOutputs().get(j), le.getInputs().get(j));
            }
            IntegerAsCircuit iac = new IntegerAsCircuit(scapa);
            assert iac.getOutputs().size() == nbits(scapa);
            c.union(iac);
            for (int j = 0; j < iac.getOutputs().size(); j++) {
                c.removeAsOutput(iac.getOutputs().get(j));
            }

            for (int j = 0; j < nbits(scapa); j++) {
                c.fuse(iac.getOutputs().get(j), le.getInputs().get(j + s.getOutputs().size()));
            }
        }
    }

    /**
     * Constructs and return a circuit that is the absolute value of the input.
     * The input is interpreted as representing an integer in two's complement. I.e., the
     * leading bit is the sign bit. Note that this matches the output of the DiffTwo circuit.
     * 
     * @param n the # input wires
     * @return a circuit which outputs the absolute value. It's # output wires should be n-1
     * @throws Exception bad things can happen
     */
    public static Circuit absCirc(int n) throws Exception {
        if (n < 2) { throw new Exception("n must be > 1"); }
        Circuit c = new Circuit(); // result
        for (int i = 0; i < n; i++) {
            c.addNewInput();
        }

        int x = 1 << (n - 1);
        IntegerAsCircuit xc = new IntegerAsCircuit(x);
        c.union(xc);
        DiffTwo d = new DiffTwo(xc.getOutputs().size(), n - 1);
        c.union(d);
        for (int i = 0; i < xc.getOutputs().size(); i++) {
            c.fuse(xc.getOutputs().get(i), d.getInputs().get(i));
        }
        for (int i = 0; i < n - 1; i++) {
            c.fuse(c.getInputs().get(i), d.getInputs().get(i + xc.getOutputs().size()));
        }

        // Need two Choose circuits
        Choose ch1 = new Choose(n - 1);
        c.union(ch1);
        c.fuse(c.getInputs().get(n - 1), ch1.getInputs().get(0));
        for (int i = 0; i < n - 1; i++) {
            c.fuse(d.getOutputs().get(i), ch1.getInputs().get(i + 1));
        }

        Choose ch2 = new Choose(n - 1);
        c.union(ch2);
        Circuit.Gate g = c.addNewGate(Circuit.GateType.NOT);
        Circuit.Wire twow[] = new Circuit.Wire[2];
        twow[0] = c.getInputs().get(n - 1);
        twow[1] = null;
        Circuit.Wire outwire = c.addNewWire();
        c.connect(g, twow, outwire);

        c.fuse(outwire, ch2.getInputs().get(0));
        for (int i = 0; i < n - 1; i++) {
            c.fuse(c.getInputs().get(i), ch2.getInputs().get(i + 1));
        }

        // Remove all of c's outputs
        while (!c.getOutputs().isEmpty()) {
            c.removeAsOutput(c.getOutputs().get(0));
        }

        // Finally a sum circuit
        List<Integer> li = new LinkedList<Integer>();
        li.add(new Integer(n - 1));
        li.add(new Integer(n - 1));
        Sum s = new Sum(li);
        c.union(s);

        for (int i = 0; i < n - 1; i++) {
            c.fuse(ch1.getOutputs().get(i), s.getInputs().get(i));
        }

        for (int i = 0; i < n - 1; i++) {
            c.fuse(ch2.getOutputs().get(i), s.getInputs().get(i + n - 1));
        }

        /* We know that any wires above n-1 are 0 */
        while (c.getOutputs().size() > n - 1) {
            c.removeAsOutput(c.getOutputs().get(n - 1));
        }

        return c;
    }

    /**
     * Add the constraint of migration budget to the circuit.
     * @param r the circuit to which we add this constraint
     * @param nc number of clients
     * @param nv list of integers; #VMs per client
     * @param ns number of servers (machines)
     * @param currp current placement (# of vms per client on each machine)
     * @param mig migration budget
     * @throws Exception bad things may happen
     */
    public void constraintMigrationBudget(Circuit r, int nc, List<Integer> nv, int ns,
            SumOfClientVMsPerMachine currp, int mig) throws Exception {

        List<Integer> il = new LinkedList<Integer>(); // This is the input list for the big sum circuit after the
                                                      // following nested loop
        List<Circuit.Wire> wirestosum = new LinkedList<Circuit.Wire>();
        for (int k = 0; k < ns; k++) {

            // System.out.println("k = "+k+", ");
            // For each machine -- outer summation

            int nvprevclients = 0;
            for (int c = 0; c < nc; c++) {
                // For each client, inner summation
                int nvbits = nbits(nv.get(c));

                int p0kc = currp.get(k, c);
                IntegerAsCircuit cp0kc = new IntegerAsCircuit(p0kc);
                r.union(cp0kc);
                DiffTwo d = new DiffTwo(cp0kc.getOutputs().size(), nvbits);
                r.union(d);
                Circuit ad = absCirc(d.getOutputs().size());
                r.union(ad);

                il.add(new Integer(ad.getOutputs().size()));
                wirestosum.addAll(ad.getOutputs());

                for (int i = 0; i < d.getOutputs().size(); i++) {
                    r.fuse(d.getOutputs().get(i), ad.getInputs().get(i));
                    r.removeAsOutput(d.getOutputs().get(i));
                }

                for (int i = 0; i < cp0kc.getOutputs().size(); i++) {
                    r.fuse(cp0kc.getOutputs().get(i), d.getInputs().get(i));
                    r.removeAsOutput(cp0kc.getOutputs().get(i));
                }

                for (int i = 0; i < nvbits; i++) {
                    r.fuse(r.getInputs().get(nvprevclients * ns + k * nvbits + i),
                            d.getInputs().get(i + cp0kc.getOutputs().size()));
                }

                // System.out.println("\t\t c = "+c+", #inputs = "+r.getInputs().size());
                nvprevclients += nvbits;
            }

            // System.out.println("\t #inputs = "+r.getInputs().size());
        }

        // Now a big sum
        Sum s = new Sum(il);
        assert s.getInputs().size() == wirestosum.size();
        r.union(s);
        for (int i = 0; i < s.getInputs().size(); i++) {
            r.fuse(wirestosum.get(i), s.getInputs().get(i));
            r.removeAsOutput(wirestosum.get(i));
        }

        // System.out.println("After sum: #inputs = "+r.getInputs().size());

        // Now comparison with migration budget
        IntegerAsCircuit mc = new IntegerAsCircuit(2 * mig);
        r.union(mc);

        LessEquals le = new LessEquals(s.getOutputs().size(), mc.getOutputs().size());
        r.union(le);

        for (int i = 0; i < s.getOutputs().size(); i++) {
            r.fuse(s.getOutputs().get(i), le.getInputs().get(i));
            r.removeAsOutput(s.getOutputs().get(i));
        }

        for (int i = 0; i < mc.getOutputs().size(); i++) {
            r.fuse(mc.getOutputs().get(i), le.getInputs().get(i + s.getOutputs().size()));
            r.removeAsOutput(mc.getOutputs().get(i));
        }

        // System.out.println("After le: #inputs = "+r.getInputs().size());
    }

    /**
     * # of bits to encode a non-negative integer
     * @param x the integer whose # bits we want to know
     * @return the # bits to encode x
     * @throws Exception bad things may happen, e.g., you pass in a negative integer
     */
    public static int nbits(int x) throws Exception {
        if (x < 0) { throw new Exception("x must be non-negative"); }

        int ret = 0;
        for (int i = x; i > 0; i >>= 1)
            ret++;
        return ret;
    }

    @Override
    public boolean reduce(EpochHistory eh) throws Exception {
        // We'll store eh so we can map back the CNF-SAT certificate
        // to a placement map.

        this._eh = eh;

        nc = eh._latestPlacementMap.getNumberOfClients();
        ns = eh._latestPlacementMap._machines.size();
        nv = new LinkedList<Integer>(); // #VMs per client
        List<Integer> clIdSet = new LinkedList<Integer>(eh._latestPlacementMap._clients.keySet());
        Collections.sort(clIdSet);
        for (int i = 0; i < nc; i++) {
            Client aclient = eh._latestPlacementMap._clients.get(clIdSet.get(i));
            nv.add(new Integer(aclient._vms.size()));
        }

        Circuit.resetIDs();
        ZeroOne.reset();
        Circuit c = new Circuit();

        /*
         *  Our input wires are organized as:
         *  	- for each client starting at 0, #VM-bits per server
         */
        for (int i = 0; i < nc; i++) {
            int nvbits;
            try {
                nvbits = nbits(nv.get(i).intValue());
            } catch (Exception e) {
                e.printStackTrace();
                return false;
            }

            for (int j = 0; j < ns * nvbits; j++) {
                c.addNewInput();
            }
        }

        // Constraint Line (4)
        try {
            constraintTotalNumberOfClientVMs(c, nc, nv, ns);
        } catch (Exception e) {
            // Catastrophic

            e.printStackTrace();
            return false;
        }

        // Constraint Line (5)
        try {
            int scapa = VagabondSettings.getInstance().numberOfMachineSlots;
            constraintServerCapacity(c, nc, nv, ns, scapa);
        } catch (Exception e) {
            // Catastrophic

            e.printStackTrace();
            return false;
        }

        // Constraint Line (3)
        try {
            int mig = VagabondSettings.getInstance().migrationBudget;
            if (mig >= 0) {
                SumOfClientVMsPerMachine currp = new SumOfClientVMsPerMachine(eh._latestPlacementMap);
                constraintMigrationBudget(c, nc, nv, ns, currp, mig);
            }
        } catch (Exception e) {
            // Catastrophic

            e.printStackTrace();
            return false;
        }

        // Now store the circuit in this object
        this._circ = c;

        return true;
    }
}
