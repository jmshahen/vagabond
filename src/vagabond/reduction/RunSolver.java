package vagabond.reduction;

import vagabond.pieces.EpochHistory;

public interface RunSolver {
    /**
     * A timer key prefix to distinguish between multiple runs (i.e. the current Epoch number)
     * @param keyPrefix the text prefix to place at the start of every timing key
     */
    public void setTimerKeyPrefix(String keyPrefix);

    /**
     * Loads up the reduction and performs any necessary operations to be ready to run the operation through the solver.
     * @param r Reduction that contains the necessary information to write a testcase file to run through a solver
     * @return TRUE if there were no errors; FALSE otherwise
     */
    public boolean load(ReduceTo r);

    /**
     * Checks to see if the solver is reachable on the current path
     * @return TRUE if the solver is reachable; FALSE otherwise
     */
    public boolean checkIfSolverIsReachable();

    public EpochHistory run() throws Exception;
}
