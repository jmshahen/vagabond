/**
 *
 * Stores the files related to the reduction to ILP; MUST HAVE IBM CPLEX ALREADY INSTALLED!
 * <p>
 * In order to run the CPLEX ILP reduction in Vagabond:
 * <ol>
 * <li>Must have the <tt>lib/cplex.jar</tt> linked to the Vagabond instance</li>
 * <li>Must have the native IBM CPLEX binaries installed on the computer</li>
 * <li>MIGHT require the CPLEX bin to be on the PATH</li>
 * </ol>
 */
package vagabond.reduction.ilp;