package vagabond.reduction.ilp;

import java.util.ArrayList;

import vagabond.pieces.*;
import vagabond.singleton.VagabondSettings;

/**
 * Writes out the ILP file in the IBM ILOG CPLEX format
 * @author Jonathan Shahen
 *
 */
public class WriteILPFile {

    public static String toILPString(EpochHistory ep) {
        return toILPString(ep, new SumOfClientVMsPerMachine(ep._latestPlacementMap));
    }

    public static String toILPString(EpochHistory ep, SumOfClientVMsPerMachine sum) {
        StringBuilder sb = new StringBuilder();

        // Header
        sb.append("/* Created by Vagabond.reduction.ilp.WriteILPFile */\n");

        PlacementMap place = ep._latestPlacementMap;
        VagabondSettings settings = VagabondSettings.getInstance();
        ArrayList<Integer> clientIDSorted = place.getSortedClientIDs();

        Integer r = Math.min(place.getMaxNumberOfVMsPerClient(), place.getMaxNumberOfSlotsPerMachine());
        sb.append("r=").append(r).append(";\n");

        sb.append("numMachines=").append(place.getNumberOfMachines())
                .append(";\nnumClients=").append(place.getNumberOfClients())
                .append(";\nmigrationBudget=").append(settings.migrationBudget)
                .append(";\n\n");

        sb.append("vmsPerClient=[");
        boolean firstLoop = true;
        for (Integer i : clientIDSorted) {
            if (!firstLoop) {
                sb.append(",");
            }
            sb.append(place.getClient(i).getNumberOfVMs());

            firstLoop = false;
        }
        sb.append("];\n\n");

        sb.append("machineCapacity=[");
        firstLoop = true;
        for (Machine m : place._machines) {
            if (!firstLoop) {
                sb.append(",");
            }
            sb.append(m._maxSpots);

            firstLoop = false;
        }
        sb.append("];\n\n");

        sb.append("// Client ID 1 x Client ID 2 ==> Information Leakage from Client ID 1 to Client ID 2 ")
                .append("over the past X epochs\n")
                .append("L=").append(ep._sumOfInformationLeakage.toMatrixString(clientIDSorted)).append(";\n\n");
        sb.append("// Machine ID x Client ID ==> Sum of VMs on Machine ID that belong to Client ID\n")
                .append("p0=").append(new SumOfClientVMsPerMachine(place).toMatrixString(clientIDSorted))
                .append(";\n\n");

        return sb.toString();
    }

}
