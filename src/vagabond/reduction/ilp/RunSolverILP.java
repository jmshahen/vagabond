package vagabond.reduction.ilp;

import java.util.logging.Logger;

import ilog.concert.IloException;
import vagabond.pieces.EpochHistory;
import vagabond.pieces.PlacementMap;
import vagabond.reduction.ReduceTo;
import vagabond.reduction.RunSolver;
import vagabond.singleton.VagabondSettings;
import vagabond.timing.TimingManager;

public class RunSolverILP implements RunSolver {
    public ReduceToILP rt;
    private static Logger logger = null;
    public static TimingManager timing;

    public String tp = "Epoch_Unknown";

    public RunSolverILP() {
        logger = VagabondSettings.getInstance().getLogger();
        timing = VagabondSettings.getInstance().timing;
    }

    @Override
    public void setTimerKeyPrefix(String keyPrefix) {
        tp = keyPrefix;
    }

    @Override
    public boolean load(ReduceTo r) {
        rt = (ReduceToILP) r;
        return true;
    }

    @Override
    public boolean checkIfSolverIsReachable() {
        // TODO Auto-generated method stub
        return false;
    }

    @Override
    public EpochHistory run() throws IloException {
        /*TIMING*/timing.toggleTimer(tp + "RunSolverILP::run");
        if (rt.cplex.solve()) {
            logger.info("CPLEX OBJECTIVE: " + rt.cplex.getObjValue());
            logger.info("CPLEX Time: " + rt.cplex.getCplexTime());
            logger.info("CPLEX Status: " + rt.cplex.getStatus());
            rt.opl.postProcess();

            // Convert to placement Map
            PlacementMap place = PlacementMapFromILPSolver.getPlacementMap(rt);

            EpochHistory rtn = new EpochHistory(rt._history._slidingWindow);
            rtn.add(place);

            // End the model (clean up the resources)
            rt.oplF.end();

            /*TIMING*/timing.toggleTimer(tp + "RunSolverILP::run");
            return rtn;
        } else {
            rt.oplF.end();
            logger.severe("CPLEX could find no solution!");

            /*TIMING*/timing.cancelTimer(tp + "RunSolverILP::run");
            return null;
        }
    }

}
