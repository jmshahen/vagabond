package vagabond.reduction.ilp;

import java.io.FileWriter;
import java.io.IOException;
import java.util.logging.Level;
import java.util.logging.Logger;

import ilog.concert.IloException;
import ilog.opl.*;
import vagabond.pieces.*;
import vagabond.reduction.ReduceTo;
import vagabond.singleton.VagabondSettings;
import vagabond.timing.TimingManager;

public class ReduceToILP implements ReduceTo {
    public static VagabondSettings settings;
    public static Logger logger;
    public static TimingManager timing;

    public String tp = "Epoch_Unknown";

    public EpochHistory _history = null;
    public PlacementMap _currentPlacement = null;
    public SumOfClientVMsPerMachine _sumPlacement = null;

    public IloOplFactory oplF = null;
    public IloCplex cplex = null;
    public IloOplModel opl = null;

    public ReduceToILP() {
        settings = VagabondSettings.getInstance();
        logger = settings.getLogger();
        timing = settings.timing;
    }

    @Override
    public void setTimerKeyPrefix(String keyPrefix) {
        tp = keyPrefix;
    }

    @Override
    public String toString() {
        StringBuilder sb = new StringBuilder();

        sb.append("ReduceToILP {");

        if (oplF == null) {
            sb.append("Have not reduced yet!");
        } else {
            sb.append(opl.toString());
        }

        sb.append("}");

        return sb.toString();
    }

    @Override
    public boolean reduce(EpochHistory eh) throws IloException {
        /*TIMING*/timing.startTimer(tp + "ReduceToILP::reduce");

        _history = eh;
        _currentPlacement = _history._latestPlacementMap;
        _sumPlacement = new SumOfClientVMsPerMachine(_currentPlacement);

        try {
            if (logger.isLoggable(Level.FINE)) {
                FileWriter writeOutILP = new FileWriter(settings.getIlpFile());
                String ilpString = WriteILPFile.toILPString(eh, _sumPlacement);
                writeOutILP.write(ilpString);
                writeOutILP.close();
            }
        } catch (IOException e) {
            e.printStackTrace();
        }

        // Changes between production mode and debugging mode
        if (logger.isLoggable(Level.FINE)) {
            IloOplFactory.setDebugMode(true);
        } else {
            IloOplFactory.setDebugMode(false);
            IloOplFactory.setDebugModeWarning(false);
        }
        oplF = new IloOplFactory();
        IloOplErrorHandler errHandler = oplF.createOplErrorHandler(null);

        /*TIMING*/timing.startTimer(tp + "ReduceToILP::reduce::readOPL");
        IloOplModelSource modelSource;
        if (settings.migrationBudget != -1) {
            logger.info("Using the OPL Model: " + settings.getIlpOplFile().getAbsolutePath());
            modelSource = oplF.createOplModelSource(settings.getIlpOplFile().getAbsolutePath());
        } else {
            logger.info("Using the OPL Model: " + settings.getIlpOplNoMigrationFile().getAbsolutePath());
            modelSource = oplF.createOplModelSource(settings.getIlpOplNoMigrationFile().getAbsolutePath());
        }
        /*TIMING*/timing.stopTimer(tp + "ReduceToILP::reduce::readOPL");

        IloOplSettings oplSettings = oplF.createOplSettings(errHandler);
        IloOplModelDefinition def = oplF.createOplModelDefinition(modelSource, oplSettings);
        cplex = oplF.createCplex();

        if (!logger.isLoggable(Level.FINE)) {
            cplex.setOut(null);
        }

        /*TIMING*/timing.startTimer(tp + "ReduceToILP::reduce::createOPLModel");
        opl = oplF.createOplModel(def, cplex);
        /*TIMING*/timing.stopTimer(tp + "ReduceToILP::reduce::createOPLModel");

        /*TIMING*/timing.startTimer(tp + "ReduceToILP::reduce::ILPCustomDataSource");
        IloOplDataSource dataSource = new ILPCustomDataSource(oplF, _history, _sumPlacement);
        /*TIMING*/timing.stopTimer(tp + "ReduceToILP::reduce::ILPCustomDataSource");

        opl.addDataSource(dataSource);

        /*TIMING*/timing.startTimer(tp + "ReduceToILP::reduce::generate");
        opl.generate();
        /*TIMING*/timing.stopTimer(tp + "ReduceToILP::reduce::generate");

        // Set the optimization parameters
        // opl.getCplex().setParam(IloCplex.Param.MIP.Tolerances.MIPGap, settings.relativeMIPGapTolerance);
        // opl.getCplex().setParam(IloCplex.Param.MIP.Tolerances.AbsMIPGap, settings.absoluteMIPGapTolerance);
        // opl.getCplex().setParam(IloCplex.IntParam.MIPEmphasis, IloCplex.MIPEmphasis.Optimality);
        // opl.getCplex().setParam(IloCplex.Param.Simplex.Tolerances.Optimality, 1e-9);

        /*TIMING*/timing.stopTimer(tp + "ReduceToILP::reduce");
        return true;
    }
}
