package vagabond.reduction.ilp;

import java.util.ArrayList;
import java.util.Hashtable;
import java.util.logging.Level;
import java.util.logging.Logger;

import ilog.concert.IloException;
import ilog.concert.IloIntMap;
import vagabond.pieces.*;
import vagabond.singleton.VagabondSettings;

/**
 * Takes as input the result from the ILP solver and converts that into a PlacementMap
 * @author Jonathan Shahen
 *
 */
public class PlacementMapFromILPSolver {
    private static Logger logger = null;

    /**
     * Creates a PlacementMap by deciphering the raw output from the ILP Solver and using whatever internal
     * structures were used to map Clients and VMs in the reduction to ILP Solver's language 
     * @param rt reference to access internal structure to help link Clients and VMs to their original IDs
     * @return PlacementMap that has the same Client, VM, and machine IDs as past the input to the reduction had
     * @throws IloException occurs if there is any trouble reading the result from CPLEX 
     */
    public static PlacementMap getPlacementMap(ReduceToILP rt) throws IloException {
        if (logger == null) {
            logger = VagabondSettings.getInstance().getLogger();
        }

        IloIntMap cplexP1 = rt.opl.getElement("p1").asIntMap();

        if (logger.isLoggable(Level.FINE)) {
            logger.fine("cplexP1: " + cplexP1);
        }

        ArrayList<Integer> sortedClientID = rt._currentPlacement.getSortedClientIDs();
        SumOfClientVMsPerMachine p0 = rt._sumPlacement;
        SumOfClientVMsPerMachine p1 = new SumOfClientVMsPerMachine(cplexP1, sortedClientID);

        if (logger.isLoggable(Level.FINE)) {
            logger.fine("p0: " + p0);
            logger.fine("p1: " + p1);
        }

        PlacementMap p0Place = rt._currentPlacement;
        PlacementMap p1Place = PlacementMap.sameSizeAs(rt._currentPlacement);
        SumOfClientVMsPerMachine p0Subp1 = SumOfClientVMsPerMachine.empty(p0.getNumberOfMachines(),
                p0.getNumberOfClients());
        /**
         * Client ID => List of VMs that must be moved
         */
        Hashtable<Integer, ArrayList<VM>> moveableVms = new Hashtable<Integer, ArrayList<VM>>();
        for (Integer cID : sortedClientID) {
            moveableVms.put(cID, new ArrayList<>());
            for (int m = 0; m < p0.getNumberOfMachines(); m++) {
                Integer val = p0.get(m, cID) - p1.get(m, cID);
                p0Subp1.put(m, cID, val);

                // Some VMs moved away from p0 for client CID on Machine m
                if (val > 0) {
                    int i = 0;

                    for (VM vm : p0Place._machines.get(m)._vms) {
                        if (vm._clientID == cID) {
                            if (i < val) {
                                // Grab the VMs that have moved and add them to the pool
                                moveableVms.get(cID).add(vm);
                                i++;
                            } else {
                                // Add any remaining VMs over
                                p1Place.addNewVMToMachine(vm, m);
                            }
                        }
                    }
                    // Make sure we ended because of the break statement
                    if (i != val) { throw new IllegalArgumentException("p0 and p1 do not match!"); }
                }
            }
        }

        logger.fine("p0Subp1:\n" + p0Subp1);
        logger.fine("moveableVms:\n" + moveableVms);

        for (Integer cID : sortedClientID) {
            for (int m = 0; m < p0.getNumberOfMachines(); m++) {
                Integer val2 = p0Subp1.get(m, cID);
                // logger.fine("CID = " + cID + "; m = " + m + "; val2 = " + val2);

                if (val2 <= 0) {
                    // copy the VMs from the old placement map
                    for (VM vm : p0Place._machines.get(m)._vms) {
                        if (vm._clientID == cID) {
                            p1Place.addNewVMToMachine(vm, m);
                        }
                    }

                    if (val2 < 0) {
                        // grab new VMs from the pool
                        for (int i = 0; i < -1 * val2; i++) {
                            p1Place.addNewVMToMachine(moveableVms.get(cID).get(i), m);
                        }
                        // Remove the vms from the pool
                        for (int i = 0; i < -1 * val2; i++) {
                            moveableVms.get(cID).remove(0); // Remove the first one from the list!
                        }
                    }
                }
            }
        }

        logger.fine("p0Place:\n" + p0Place);
        logger.fine("p1Place:\n" + p1Place);

        if (logger.isLoggable(Level.FINE)) {
            SumOfClientVMsPerMachine test = new SumOfClientVMsPerMachine(p1Place);

            logger.fine("p1: " + p1);
            logger.fine("testP1: " + test);

            logger.fine("Passed the equality test: " + test.equals(p1));
        }

        return p1Place;
    }

}
