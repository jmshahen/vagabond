package vagabond.reduction.ilp;

import java.util.ArrayList;

import ilog.opl.*;
import vagabond.pieces.*;
import vagabond.singleton.VagabondSettings;

public class ILPCustomDataSource extends IloCustomOplDataSource {
    public EpochHistory L = null;
    public PlacementMap place = null;
    public SumOfClientVMsPerMachine p0 = null;
    private VagabondSettings settings;

    ILPCustomDataSource(IloOplFactory oplF, EpochHistory eh, SumOfClientVMsPerMachine sum) {
        super(oplF);
        settings = VagabondSettings.getInstance();
        L = eh;
        place = eh._latestPlacementMap;
        p0 = sum;
    }

    public void customRead() {
        IloOplDataHandler handler = getDataHandler();

        ArrayList<Integer> clientIDSorted = place.getSortedClientIDs();

        // Variable r
        handler.startElement("r");
        Integer r = Math.min(place.getMaxNumberOfVMsPerClient(), place.getMaxNumberOfSlotsPerMachine());
        System.out.println("[ILPCustomDataSource] r = " + r);
        handler.addIntItem(r);
        handler.endElement();

        // Variable numMachines
        handler.startElement("numMachines");
        handler.addIntItem(place.getNumberOfMachines());
        handler.endElement();

        // Variable numClients
        handler.startElement("numClients");
        handler.addIntItem(place.getNumberOfClients());
        handler.endElement();

        // Variable migrationBudget
        if (settings.migrationBudget != -1) {
            handler.startElement("migrationBudget");
            handler.addIntItem(settings.migrationBudget);
            handler.endElement();
        }

        // Variable Array vmsPerClient
        handler.startElement("vmsPerClient");
        handler.startArray();
        for (Integer i : clientIDSorted) {
            handler.addIntItem(place.getClient(i).getNumberOfVMs());
        }
        handler.endArray();
        handler.endElement();

        // Variable Array machineCapacity
        handler.startElement("machineCapacity");
        handler.startArray();
        for (Machine m : place._machines) {
            handler.addIntItem(m._maxSpots);
        }
        handler.endArray();
        handler.endElement();

        /**
         * Client ID 1 x Client ID 2 ==> Information Leakage from Client ID 1 to Client ID 2 over the past X epochs
         */
        handler.startElement("L");
        handler.startArray();
        for (Integer c0 : clientIDSorted) {
            handler.startArray();
            for (Integer c1 : clientIDSorted) {
                handler.addIntItem(L._sumOfInformationLeakage.get(c0, c1));
            }
            handler.endArray();
        }
        handler.endArray();
        handler.endElement();

        /**
         * Machine ID x Client ID ==> Sum of VMs on Machine ID that belong to Client ID
         */
        handler.startElement("p0");
        handler.startArray();
        for (int m = 0; m < p0.getNumberOfMachines(); m++) {
            handler.startArray();
            for (Integer c : clientIDSorted) {
                handler.addIntItem(p0.get(m, c));
            }
            handler.endArray();
        }
        handler.endArray();
        handler.endElement();
    }
}