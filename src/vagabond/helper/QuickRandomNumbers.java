package vagabond.helper;

public class QuickRandomNumbers {
    private long x;

    public QuickRandomNumbers() {
        x = System.currentTimeMillis();
    }

    public QuickRandomNumbers(long seed) {
        x = seed;
    }

    /**
     * Returns a random long [0, 2^64 -1], allows for zeros because of the x-1.
     * @return a quickly generated random number
     */
    public long randomLong() {
        x ^= (x << 21);
        x ^= (x >>> 35);
        x ^= (x << 4);
        return x - 1;
    }
}
