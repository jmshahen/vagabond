package vagabond.helper;

/**
 * A generic rotating fixed size array, very efficient: O(1) insert and lookup, O(n) storage.
 * @author Jonathan Shahen
 *
 * @param <Type> Allows the array to be used on any Object
 */
public class RotatingArray<Type> {
    /**
     * Stores the elements in this array
     */
    private Object[] _array;
    /**
     * The size of the array
     */
    private Integer _capacity;
    /**
     * Stores the number of elements that have been added to the system.
     * Increasing value that is capped at {@link #_capacity}
     */
    private Integer _size;

    /**
     * Stores the current physical index to be used to convert logical indexes to phyiscal indexes
     */
    private Integer _currentIndex;

    /**
     * Creates an Object array and pre-fills it with null values (done by default)
     * @param capacity the size of the array (cannot be changed)
     */
    public RotatingArray(Integer capacity) {
        _capacity = capacity;

        _currentIndex = 0;
        _array = new Object[capacity];
        _size = 0;
    }

    /**
     * Returns how many values have been entered in the array
     * @return number of elements in array (max value of _capacity)
     */
    public int size() {
        return _size;
    }

    /**
     * Returns how many elements can be stored in this array 
     * @return maximum number of elements that can be in the array
     */
    public Integer getCapacity() {
        return _capacity;
    }

    /**
     * Grabs the object that is stored at the logical index.
     * @param index the logical index
     * @return The object stored at that address: can be null
     */
    @SuppressWarnings("unchecked")
    public Type get(int index) {
        return (Type) _array[actualIndex(index)];
    }

    /**
     * Puts the element at logical index 0 by deleting the last element in the array and realigning the logical 
     * address to point at the new starting element.
     * @param element Object to be added as the first element
     */
    public void pushToFirst(Type element) {
        int index = actualIndex(-1);
        _array[index] = element;
        _currentIndex = index;

        if (_size < _capacity) {
            _size++;
        }
    }

    /**
     * Grabs the first element of the array
     * @return returns the first element
     */
    public Type getFirst() {
        return get(0);
    }

    /**
     * Wrap around the index value that also works with negative numbers.
     * 
     * <h2>Examples:</h2>
     * <table border="1" summary="An example of inputs and the expected outputs">
     * <tr><th><b>_capacity</b></th><th><b>_currentIndex</b></th>
     * <th><b>Index</b></th><th><b>Returns</b></th></tr>
     * <tr><td> 10 </td><td> 0 </td><td> 0 </td><td> 0 </td></tr>
     * <tr><td> 10 </td><td> 0 </td><td> 1 </td><td> 1 </td></tr>
     * <tr><td> 10 </td><td> 0 </td><td> 9 </td><td> 9 </td></tr>
     * <tr><td> 10 </td><td> 0 </td><td> 10 </td><td> 0 </td></tr>
     * <tr><td> 10 </td><td> 0 </td><td> -1 </td><td> 9 </td></tr>
     * <tr><td> 10 </td><td> 6 </td><td> 0 </td><td> 6 </td></tr>
     * <tr><td> 10 </td><td> 6 </td><td> 1 </td><td> 7 </td></tr>
     * <tr><td> 10 </td><td> 6 </td><td> 9 </td><td> 5 </td></tr>
     * <tr><td> 10 </td><td> 6 </td><td> 10 </td><td> 6 </td></tr>
     * <tr><td> 10 </td><td> 6 </td><td> -1 </td><td> 5 </td></tr>
     * </table>
     * @param index the virtual index
     * @return the physical index of the virtual index
     */
    private int actualIndex(int index) {
        return Math.floorMod((_currentIndex + index), _capacity);
    }

    /**
     * Prints out the toString() value to every element that has been added to the array.
     * {@link java.lang.StringBuilder} is null safe, and thus even if the end user puts nulls in the array this 
     * function is null-safe.
     */
    @Override
    public String toString() {
        StringBuilder sb = new StringBuilder();
        sb.append("RoatatingArray [");
        for (int i = 0; i < _size; i++) {
            if (i != 0) {
                sb.append(", ");
            }
            sb.append(get(i));
        }
        sb.append("]");

        return sb.toString();
    }

    /**
     * Returns the element at the end of the array, the one which will be pushed out when {@link #pushToFirst(Object)} 
     * is called
     * @return last element of the array's capacity; can be null, NOT related to the {@link #size()} of the array
     * @see #getCapacity()
     */
	public Type getEndOfArray() {
		return get(_capacity - 1);
	}
}
