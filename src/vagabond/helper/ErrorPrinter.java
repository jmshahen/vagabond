package vagabond.helper;

import java.io.PrintWriter;
import java.io.StringWriter;

/**
 * A helper class to return the properly formatted string from an error
 * @author Jonathan Shahen
 *
 */
public class ErrorPrinter {
    public Exception e;
    private String errorTxt;

    public ErrorPrinter(Exception e) {
        errorTxt = getErrorString(e);
    }

    @Override
    public String toString() {
        return errorTxt;
    }

    public static String getErrorString(Exception e) {
        StringWriter errors = new StringWriter();
        e.printStackTrace(new PrintWriter(errors));

        return errors.toString();
    }
}
