/**
 *
 * Holds helper functions that deal with odd small tasks.
 * <p>
 * To make reading the code easier and to reduce repetitive tasks, odd and small functions should be removed and 
 * placed in this package.
 */
package vagabond.helper;