package vagabond.helper;

import java.util.*;

/**
 * Helper class to convert a string to a Boolean but throws an exception if it isn't valid.
 * This is different from the Java Standard API way of converting (no Exception is throw, default to false).
 * @author Jonathan Shahen
 *
 */
public class ConvertBooleanWithException {

    public static Boolean convert(String s) {
        s = s.toLowerCase();
        Set<String> trueSet = new HashSet<String>(Arrays.asList("1", "true", "yes"));
        Set<String> falseSet = new HashSet<String>(Arrays.asList("0", "false", "no"));

        if (trueSet.contains(s))
            return Boolean.TRUE;
        if (falseSet.contains(s))
            return Boolean.FALSE;

        throw new NumberFormatException(s + " is not a boolean.");
    }
}
