package vagabond.singleton;

import java.io.*;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.logging.*;

import org.apache.commons.lang3.SystemUtils;

import vagabond.VagabondInstance;
import vagabond.VagabondOptionString;
import vagabond.enums.InformationLeakageType;
import vagabond.enums.PlacementMapDisplayStyle;
import vagabond.helper.ErrorPrinter;
import vagabond.logging.CSVFileFormatter;
import vagabond.logging.ConsoleFormatter;
import vagabond.placement.PlacementMapExamples;
import vagabond.results.ResultsManager;
import vagabond.results.StatisticsManager;
import vagabond.timing.TimingManager;

/**
 * This class stores all of the global variables that might be needed by the whole suite.
 * I am storing variables here that fit the following criteria:
 * <ol>
 *  <li>Have universal use</li>
 *  <li>Configured once in order to use</li>
 *  <li>Contain convenience functions</li>
 *  <li>Has a static value during a specific context</li>
 * </ol>
 * Examples that fit the above: logger (universal use), input settings (static value), timing, ...
 * @author Jonathan Shahen
 *
 */
public class VagabondSettings {
    private static volatile VagabondSettings _instance = null;

    public static final String VERSION = "v0.0.2";
    public static final String AUTHORS = "Jonathan Shahen <jmshahen@uwaterloo.ca>";

    // ################################################################
    // FOLDERS
    /**
     * Stores the folder that contains all of the logs and results produced by this program
     */
    public String logFolder = "logs";
    /**
     * Stores the folder that contains data required by this program
     */
    public String dataFolder = "data";
    /**
     * Stores the folder that contains the solver programs
     */
    public String programFolder = "programs";
    /**
     * The sub-folder that stored the Nomad executable and files
     * <p>
     * This folder is located as a folder within {@link #programFolder}
     */
    public String nomadFolder = "nomad-jonathan-edit";
    /**
     * The sub-folder that stores the CNF Solver and relevant files
     * <p>
     * This folder is located as a folder within {@link #programFolder}
     */
    public String cnfSolverFolder = "lingeling-b85-3ee2a39-151109";

    // ################################################################
    // FILES
    /**
     * Stores the log file of what was recorded during the last run; does not store output to System.out!
     * <p>
     * In Excel you can make the data into a table by selecting all the data (CTRL+A) and then clicking 
     * <i>Insert-&gt;Table</i>; 
     * this will allow you to sort and limit columns by values you specify (Very handy for searching the log)
     */
    public String logFile = "vagabond.log.csv";
    /**
     * Stores the output filename of the PlacementMap if the PlacementMap needs to be printed out
     * @see vagabond.pieces.PlacementMap
     */
    public String placementMapFile = "vagabond.error.placementMap.txt";
    /**
     * The file name of the timing results file to write out; SHOULD be stored under the folder {@link #logFolder}.
     */
    public String timingFile = "vagabond.timing.csv";
    /**
     * The file name of the results file to write out; SHOULD be stored under the folder {@link #logFolder}.
     */
    public String resultsFile = "vagabond.results.txt";
    /**
     * The file name of the stats file to write out; SHOULD be stored under the folder {@link #logFolder}.
     */
    public String statsFile = "vagabond.stats.csv";
    /**
     * The IBM OPL file for the minimize the <b>MAX</b> Client to Client Information Leakage
     * @see #getIlpOplFile()
     */
    public String ilpOplFile = "ilp.mod";
    /**
     * The IBM OPL file for the minimize the <b>MAX</b> Client to Client Information Leakage 
     * with <b>no Migration Budget</b>.
     * @see #getIlpOplNoMigrationFile()
     */
    public String ilpOplNoMigrationFile = "ilpNoMigration.mod";
    /**
     * The IBM OPL file for the minimize the <b>SUM</b> Client to Client Information Leakage
     * @see #getIlpOplFile()
     */
    public String ilpOplFileSumInfoLeak = "ilp_sum.mod";
    /**
     * The IBM OPL file for the minimize the <b>SUM</b> Client to Client Information Leakage 
     * with <b>no Migration Budget</b>.
     * @see #getIlpOplNoMigrationFile()
     */
    public String ilpOplNoMigrationFileSumInfoLeak = "ilpNoMigration_sum.mod";
    /**
     * File to write out the CNF SAT reduction to then pass to the CNF SAT Solver
     * @see #getCNFInstanceFile()
     */
    public String cnfInstanceFile = "instance.cnf";
    // ################################################################
    // PROGRAMS
    /**
     * Filename of the CNF Solver executable for Windows; 
     * Automatically changes this if you are on Linux/Mac to the binary compiled for that system.
     * Can be: lingeling.exe, plingeling.exe, treengeling.exe
     */
    public String cnfSolverProgram_Win = "lingeling.exe";
    /**
     * Filename of the CNF Solver executable for Linux; 
     * Automatically changes this if you are on Linux/Mac to the binary compiled for that system.
     */
    public String cnfSolverProgram_Linux = "lingeling";
    /**
     * Filename of the Nomad executable; 
     * can change this if you are on Linux/Mac to the binary compiled for that system.
     */
    public String nomadProgram = "nomad.exe";
    /**
     * Holds the specific filename of the settings.ini file that Nomad is expecting
     * @see #getNomadSettingsFile()
     */
    private String nomadSettingsFile = "settings.txt";
    /**
     * Holds the specific filename of the input placement map file that Nomad is expecting; must be referenced in the 
     * {@link #getNomadSettingsFile()} file.
     * @see VagabondSettings#getNomadPlacementFile()
     */
    public String nomadPlacementFile = "inputPlacement.txt";
    /**
     * Holds the specific filename of the output placement map history file that Nomad produces; must be referenced in  
     * the {@link #getNomadResultsFile()} file.
     * @see VagabondSettings#getNomadPlacementFile()
     */
    public String nomadResultsFile = "placementMapHistory.csv";

    // ################################################################
    // LOGGING
    private static Logger logger = null;
    public ConsoleHandler consoleHandler = new ConsoleHandler();
    /**
     * The logging level of the program
     */
    public Level logLevel = Level.FINE;
    public FileHandler fileHandler;
    public Boolean WriteCSVFileHeader = true;
    public static final DateFormat df = new SimpleDateFormat("yyyy/MM/dd hh:mm:ss.SSSZ");

    /**
     * A flag that when TRUE will print out to the logger on INFO the PlacementMap at the END of every epoch as well
     * as the Initial Placement Map.
     * <p>
     * Controlled in {@link VagabondInstance#setupLoggerOptions(org.apache.commons.cli.CommandLine, 
     * org.apache.commons.cli.Options)}, when set to verbose or quiet this will be FALSE
     */
    public Boolean displayPlacementMapEveryEpoch = true;

    /**
     * Stores the default display setting that should be used for placement maps
     */
    public PlacementMapDisplayStyle displayStyle = PlacementMapDisplayStyle.CSV;

    // ################################################################
    // MANAGERS
    /**
     * The timing manager that holds all of the raw timing information for the instance.
     */
    public TimingManager timing;
    /**
     * The results manager that holds all of the raw results information for the instance.
     */
    public ResultsManager results;
    /**
     * The statistics manager that calculates all of the stats that can be graphed later
     */
    public StatisticsManager stats;

    // ################################################################
    // CONTROL SETTINGS
    /**
     * Holds the location of the settings file that was passed into the commandline
     */
    public String settingsFile = "";
    /**
     * @see VagabondOptionString#NUMMACHINES
     */
    public Integer numberOfMachines = 0;
    /**
     * @see VagabondOptionString#NUMSLOTS
     */
    public Integer numberOfMachineSlots = 0;
    /**
     * @see VagabondOptionString#NUMCLIENTS
     */
    public Integer numberOfClients = 0;
    /**
     * @see VagabondOptionString#NUMVMS
     */
    public Integer numberOfVMsPerClient = 0; // number of VMs a client has
    /**
     * @see VagabondOptionString#FILLINEMPTY
     */
    public Boolean fillInEmptySpots = false; // Create more VMs for random clients to fill in the remaining space
    /**
     * @see VagabondOptionString#EPOCHS
     */
    public Integer numberOfEpochs = 0;
    /**
     * @see VagabondOptionString#SLIDINGWINDOW
     */
    public Integer slidingWindow = 0;
    /**
     * @see VagabondOptionString#MIGRATIONBUDGET
     */
    public Integer migrationBudget = 0;
    /**
     * @see VagabondOptionString#RANDOMSEED
     */
    public long randomSeed = -1;
    /**
     * @see VagabondOptionString#PLACEMENT
     */
    public VagabondOptionString placementAlgorithm = VagabondOptionString.VALUENOTSET;
    /**
     * @see VagabondOptionString#REDUCTION
     */
    public VagabondOptionString reductionAlgorithm = VagabondOptionString.VALUENOTSET;
    /**
     * Stores the method of computation for Information Leakage
     */
    public InformationLeakageType informationLeakageType = InformationLeakageType.ReplicationAndCollaboration;
    /**
     * Stores the location of the input PlacementMap file, if one was provided, otherwise NULL.
     */
    public String inputPlacementMapFilePath = null;

    /**
     * Stores the number to be passed to {@link PlacementMapExamples#equalSpreadClients(int)}
     * @see VagabondOptionString#EQUALSPREADNUM
     */
    public Integer equalSpreadNum = 0;

    /**
     * The relative MIP Gap Tolerance to set CPLEX to; the default value in CPLEX is 1e-4.
     * @vagabond.category ILP Optimization Parameter
     */
    public double relativeMIPGapTolerance = 0;// 1e-4;
    public double absoluteMIPGapTolerance = 0;// 1e-4;
    /**
     * The number of seconds that a reduction solver can run per EPOCH before it is forced to stop.
     * The default is 0 (less than 1) which means that it can run indefinitely.
     */
    public long numberOfSecondsSolverCanRunPerEpoch = 0;

    /**
     * <b>TRUE</b> - ILP and CNF SAT will optimize/minimize for the Maximum Client to Client Information Leak;
     * <b>FASLE</b> - ILP and CNF SAT will optimize/minimize for the Total/Summation Client to Client Information Leak.
     * @see VagabondOptionString#MAXORSUM
     */
    public boolean minimizeMaxClientToClientInfoLeak = false;

    // ################################################################
    // FUNCTIONS

    /**
     * Prevents programmers from creating their own settings, there must be only one copy of this.
     * @see #getInstance()
     */
    protected VagabondSettings() {
        timing = new TimingManager();
        results = new ResultsManager();
        stats = new StatisticsManager();
    }

    /**
     * Returns the instance of VagabondSettings; will create a new instance on the first call.
     * @return singleton instance of VagabondSettings (there is only ever one copy: every instance has the same values)
     */
    public static VagabondSettings getInstance() {
        if (_instance == null) {
            _instance = new VagabondSettings();
        }
        return _instance;
    }

    @Override
    public String toString() {
        StringBuilder s = new StringBuilder();

        s.append("VagabondSettings{");
        s.append("placementAlgorithm=" + placementAlgorithm + ", ");
        s.append("reductionAlgorithm=" + reductionAlgorithm + ", ");
        s.append("minimizeMaxClientToClientInfoLeak=" + minimizeMaxClientToClientInfoLeak + ", ");
        s.append("numberOfEpochs=" + numberOfEpochs + ", ");
        s.append("slidingWindow=" + slidingWindow + ", ");
        s.append("migrationBudget=" + migrationBudget + ", ");

        switch (reductionAlgorithm) {
            case REDUCTION_ILP :
                s.append("relativeMIPGapTolerance=" + relativeMIPGapTolerance + ", ");
                break;
            default :
        }

        switch (placementAlgorithm) {
            case PLACEMENT_FILE :
                s.append("placementMapFile=" + placementMapFile + ", ");
                break;
            case PLACEMENT_EQUALSPREAD :
            case PLACEMENT_EQUALSPREADHALF :
                s.append("equalSpreadNum=" + equalSpreadNum + ", ");
                break;
            default :
            case PLACEMENT_RANDOM :
                s.append("numberOfMachines=" + numberOfMachines + ", ");
                s.append("numberOfMachineSlots=" + numberOfMachineSlots + ", ");
                s.append("numberOfClients=" + numberOfClients + ", ");
                s.append("numberOfVMsPerClient=" + numberOfVMsPerClient + ", ");
                s.append("fillInEmptySpots=" + fillInEmptySpots + ", ");
                s.append("seed=" + randomSeed + ", ");
                break;
        }

        s.append("settingsFile=" + settingsFile + ", ");

        s.append("}");

        return s.toString();
    }

    public String getInformativePlacement() {
        String name = "";
        switch (placementAlgorithm) {
            case PLACEMENT_EQUALSPREAD :
                name = "EqualSpread_" + equalSpreadNum;
                break;
            case PLACEMENT_EQUALSPREADHALF :
                name = "EqualSpreadHalf_" + equalSpreadNum;
                break;
            case PLACEMENT_SPECIALTEST1 :
                name = "SpecialTest1_" + equalSpreadNum;
                break;
            case PLACEMENT_FILE :
                name = "File_" + placementMapFile;
                break;
            case PLACEMENT_RANDOM :
                name = "Random_" + randomSeed;
                break;
            default :
                name = placementAlgorithm.toString() + "_" + equalSpreadNum;
        }
        return name;
    }

    /**
     * Returns the instance of the logger; creates the logger on the first call.
     * @return instance of the logger
     */
    public Logger getLogger() {
        if (logger == null) {
            setupLogger();
        }
        return logger;
    }

    /**
     * Returns the current logging level
     * @return current logging level
     */
    public Level getLoggerLevel() {
        return logLevel;
    }

    /**
     * Sets the logging level
     * @param loggerLevel new logging level
     */
    public void setLoggerLevel(Level loggerLevel) {
        logLevel = loggerLevel;
    }

    /**
     * Sets up the logger by connecting the ConsoleFormatter and the CSVFileFormatter to the logger 
     */
    public void setupLogger() {
        // Logging Level
        logger = Logger.getLogger("vagabond");
        logger.setUseParentHandlers(false);
        consoleHandler.setFormatter(new ConsoleFormatter());

        logger.setLevel(logLevel);
        consoleHandler.setLevel(logLevel);
        logger.addHandler(consoleHandler);

        // Add Logger File Handler
        if (!logFile.isEmpty()) {
            try {
                File logFolderFile = new File(logFolder);
                logFolderFile.mkdirs();

                if (!logFolderFile.isDirectory()) {
                    logger.severe("logfolder did not contain a folder that exists or that could be created!");
                }

                String logFilePath = logFolder + File.separator + logFile;

                if (!new File(logFilePath).exists()) {
                    WriteCSVFileHeader = true;
                }

                if (WriteCSVFileHeader) {
                    FileOutputStream writer = new FileOutputStream(logFilePath, true);// Always append!
                    writer.write(CSVFileFormatter.csvHeaders().getBytes());
                    writer.flush();
                    writer.close();
                }

                fileHandler = new FileHandler(logFilePath);
                fileHandler.setLevel(getLoggerLevel());
                fileHandler.setFormatter(new CSVFileFormatter());
                logger.addHandler(fileHandler);
            } catch (IOException e) {
                ErrorPrinter ep = new ErrorPrinter(e);
                System.out.println("ERROR: Unable to create the file formatter");
                System.out.println(ep.toString());

                shutdown();
                System.exit(-1);
            }
        }
    }

    /**
     * Closes all of the open handles and removes all the locks from log files.
     * If System.exit() needs to be called or the program exits normally,
     * then this function should be called before hand.
     */
    public void shutdown() {
        if (logger != null) {
            for (Handler h : logger.getHandlers()) {
                h.close();// must call h.close or a .LCK file will remain.
            }
        }
    }

    /**
     * Function to return the file path of the timing file. Used to write out the timing results.
     * This should be considered a WRITE only file.
     * @return File object to the timing file; might not be created yet.
     */
    public File getTimingFile() {
        return new File(logFolder + File.separator + timingFile);
    }

    /**
     * Function to return the file path of the results file. Used to write out the timing results.
     * This should be considered a WRITE only file.
     * @return File object to the results file; might not be created yet.
     */
    public File getResultsFile() {
        return new File(logFolder + File.separator + resultsFile);
    }

    public File getStatsFile() {
        return new File(logFolder + File.separator + statsFile);
    }

    public File getIlpFile() {
        return new File(logFolder + File.separator + "outILP.dat");
    }

    /**
     * Function to return the file path of the Settings.ini file that Nomad is expecting to be filled before running.
     * @return File object to the Nomad settings.ini file
     */
    public File getNomadSettingsFile() {
        return new File(programFolder + File.separator + nomadFolder + File.separator + nomadSettingsFile);
    }

    /**
     * Function to return the file path of the input placement map.
     * @return File object to the Nomad settings.ini file
     */
    public File getNomadPlacementFile() {
        return new File(programFolder + File.separator + nomadFolder + File.separator + nomadPlacementFile);
    }

    /**
     * Function to return the file path of the nomad executable
     * @return File object to the Nomad executable
     */
    public File getNomadExceutable() {
        return new File(programFolder + File.separator + nomadFolder + File.separator + nomadProgram);
    }

    /**
     * Function to return the file path of the nomad executable
     * @return File object to the Nomad executable
     */
    public File getNomadResultsFile() {
        return new File(programFolder + File.separator + nomadFolder + File.separator + nomadResultsFile);
    }

    /**
     * Function to return the file path of the CNF Solver executable; by default it is Lingeling.
     * @return File object to the CNF Solver executable
     * @throws IOException Is thrown if you are on an operating system that doesn't have a binary for the CNF solver
     */
    public File getCNFSolverExceutable() throws IOException {
        if (SystemUtils.IS_OS_WINDOWS) {
            return new File(programFolder + File.separator + cnfSolverFolder + File.separator + cnfSolverProgram_Win);
        } else if (SystemUtils.IS_OS_LINUX) {
            return new File(programFolder + File.separator + cnfSolverFolder + File.separator + cnfSolverProgram_Linux);
        } else {
            throw new IOException("There is no CNF Solver binary for your OS: " + SystemUtils.OS_NAME);
        }
    }

    /**
     * Returns the ILP OPL file according to the {@link #minimizeMaxClientToClientInfoLeak} variable.
     * If {@link #minimizeMaxClientToClientInfoLeak} is TRUE then {@link #ilpOplFile} is returned
     * Else {@link #ilpOplFileSumInfoLeak} is returned.
     * @return
     */
    public File getIlpOplFile() {
        if (minimizeMaxClientToClientInfoLeak) { return new File(dataFolder + File.separator + ilpOplFile); }

        return new File(dataFolder + File.separator + ilpOplFileSumInfoLeak);
    }

    /**
     * Returns the ILP OPL file without migration budget according to the 
     * {@link #minimizeMaxClientToClientInfoLeak} variable.
     * If {@link #minimizeMaxClientToClientInfoLeak} is TRUE then {@link #ilpOplNoMigrationFile} is returned
     * Else {@link #ilpOplNoMigrationFileSumInfoLeak} is returned.
     * @return
     */
    public File getIlpOplNoMigrationFile() {
        if (minimizeMaxClientToClientInfoLeak) { return new File(dataFolder + File.separator + ilpOplNoMigrationFile); }

        return new File(dataFolder + File.separator + ilpOplNoMigrationFileSumInfoLeak);
    }

    public File getCNFInstanceFile() {
        return new File(dataFolder + File.separator + cnfInstanceFile);
    }

    public static String getCurrentDateTimeStr() {
        return df.format(new Date(System.currentTimeMillis()));
    }

    public String getObjectiveStr() {
        if (minimizeMaxClientToClientInfoLeak) { return "Max CCIL"; }
        return "Sum CCIL";
    }
}
