/**
 *
 * Holds the settings for the whole project; it is a singleton which means that there is only ever one instance of the 
 * settings and every class can read and update the settings. 
 */
package vagabond.singleton;