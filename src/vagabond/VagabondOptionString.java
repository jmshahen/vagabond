package vagabond;

import java.util.ArrayList;

import vagabond.placement.PlacementMapExamples;
import vagabond.results.StatisticsManager;

/**
 * Holds Command Line Options as well as strings that can be contained in the Settings file.
 * @author Jonathan Shahen
 *
 */
public enum VagabondOptionString {
    /**
     * Empty Value
     * @vagabond.category None
     */
    VALUENOTSET("valueNotSet"),

    // Command Line Options
    /**
     * [CommandLine Parameter] Output the authors of the Vagabond Code and then quit
     * @vagabond.category CommandLine Parameter
     */
    AUTHORS("authors"),
    /**
     * [CommandLine Parameter] Test a large number of files
     * @vagabond.category CommandLine Parameter
     */
    BULK("bulk"),
    /**
     * [CommandLine Parameter] Check to see if MINISAT is correctly installed on the PATH and then quit
     * @vagabond.category CommandLine Parameter
     */
    CHECKMINISAT("checkminisat"),
    /**
     * [CommandLine Parameter] Display the HELP message and then quit
     * @vagabond.category CommandLine Parameter
     */
    HELP("help"),
    /**
     * [CommandLine Parameter] The end of line string to use when wrapping long lines in the terminal
     * @vagabond.category CommandLine Parameter
     */
    LINESTR("linstr"),
    /**
     * [CommandLine Parameter] What should the logging level be for the Terminal and for the File
     * @vagabond.category CommandLine Parameter
     */
    LOGLEVEL("loglevel"),
    /**
     * [CommandLine Parameter] The output logging file; written in CSV format
     * @vagabond.category CommandLine Parameter
     */
    LOGFILE("logfile"),
    /**
     * [CommandLine Parameter] Folder where all of the logs will be placed
     * @vagabond.category CommandLine Parameter
     */
    LOGFOLDER("logfolder"),
    /**
     * [CommandLine Parameter] The maximum width in the terminal before wrapping the line
     * @vagabond.category CommandLine Parameter
     */
    MAXW("maxw"),
    /**
     * [CommandLine Parameter] Ability to skip writing the header to the output log file
     * @vagabond.category CommandLine Parameter
     */
    NOHEADER("noheader"),
    /**
     * [CommandLine Parameter] Regardless of the logging level no Placement Map will be put in the log file
     * @vagabond.category CommandLine Parameter
     */
    NOPLACEMENTMAPS("noplacementmaps"),
    /**
     * [CommandLine Parameter] The file path to where the Results of the test should be written; in CSV format. 
     * @vagabond.category CommandLine Parameter
     */
    RESULTSFILE("results"),
    /**
     * [CommandLine Parameter] Write to the terminal the current version and then quit.
     * @vagabond.category CommandLine Parameter
     */
    VERSION("version"),

    // Control Settings
    /**
     * [CommandLine Parameter] Allows for settings to be put into a file
     * @vagabond.category CommandLine Parameter
     */
    SETTINGSFILE("settings"),
    /**
     * [Settings File Property] The number of how many machines there are
     * @vagabond.category Settings File Property
     */
    NUMMACHINES("numberOfMachines"),
    /**
     * [Settings File Property] The number of slots each machine has
     * @vagabond.category Settings File Property
     */
    NUMSLOTS("numberOfMachineSlots"),
    /**
     * [Settings File Property] The number of clients there are
     * @vagabond.category Settings File Property
     */
    NUMCLIENTS("numberOfClients"),
    /**
     * [Settings File Property] The number of Virtual Machines (VMs) per client
     * @vagabond.category Settings File Property
     */
    NUMVMS("numberOfVMsPerClient"),
    /**
     * [Settings File Property] Fills in the empty space by adding random vms to clients and machines
     * @vagabond.category Settings File Property
     */
    FILLINEMPTY("fillInEmpty"),
    /**
     * [Settings File Property] The number of EPOCHS to run before stopping
     * @vagabond.category Settings File Property
     */
    EPOCHS("numberOfEpochs"),
    /**
     * [Settings File Property] The number of EPOCHS to look into the past to calculate the Information Leakage
     * @vagabond.category Settings File Property
     */
    SLIDINGWINDOW("slidingWindow"),
    /**
     * [Settings File Property] The maximum number of migrations allowed per EPOCH. If -1 then do not have a migration
     * budget.
     * @vagabond.category Settings File Property
     */
    MIGRATIONBUDGET("migrationBudget"),
    /**
     * [Settings File Property] Determines if the ILP or CNF SAT reductions will take into account the Max 
     * Client-To-Client Information Leakage (true), or the Sum/Total Client-To-Client Information Leakage (false).
     * @vagabond.category Settings File Property
     */
    MAXORSUM("maxCCIL"),
    /**
     * [Settings File Property] This is an optional setting for controlling the random number generator
     * @vagabond.category Settings File Property
     */
    RANDOMSEED("seed"),
    /**
     * [Settings File Property] This is an optional setting for controlling the number sent to 
     * {@link PlacementMapExamples#equalSpreadClients(int)}
     * @vagabond.category Settings File Property
     */
    EQUALSPREADNUM("equalSpreadNum"),
    /**
     * [Settings File Property] This is an optional setting for setting the placement map file when 
     * {@link #PLACEMENT_FILE} is used as the value for {@link #PLACEMENT}.
     * @vagabond.category Settings File Property
     */
    PLACEMENTFILEPATH("placementFile"),
    /**
     * [Settings File Property] Which Placement Algorithm to use
     * @vagabond.category Settings File Property
     */
    PLACEMENT("placement"),
    /**
     * [Settings File Property] Which Reduction Algorithm to use
     * @vagabond.category Settings File Property
     */
    REDUCTION("reduction"),
    /**
     * [Settings File Property] Any comments that should be recorded in the STATS file 
     * @vagabond.category Settings File Property
     * @see StatisticsManager#comments
     */
    TESTCOMMENT("comment"),
    /**
     * [Settings File Property] The name of the test to be recorded in the STATS file. This is optional.
     * @vagabond.category Settings File Property
     * @see StatisticsManager#testName
     */
    TESTNAME("name"),

    // Placement Algorithms
    /**
     * [Placement Algorithms] Randomly distribute VMs into machines
     * @vagabond.category Placement Algorithms
     */
    PLACEMENT_RANDOM("randomPlacement"),

    /**
     * [Placement Algorithms] Generates a placement map using 
     * {@link PlacementMapExamples#randomHalfFull(vagabond.placement.RandomPlacement, int)}
     * @vagabond.category Placement Algorithms
     */
    PLACEMENT_RANDOMHALFFULL("randomHalfFull"),

    /**
     * [Placement Algorithms] Reads in the placement map from a CSV formatted file
     * @vagabond.category Placement Algorithms
     */
    PLACEMENT_FILE("readFile"),

    /**
     * [Placement Algorithms] Generates a placement map using {@link PlacementMapExamples#equalSpreadClients(int)}
     * @vagabond.category Placement Algorithms
     */
    PLACEMENT_EQUALSPREAD("equalSpread"),

    /**
     * [Placement Algorithms] Generates a placement map using 
     * {@link PlacementMapExamples#equalSpreadClientsHalfFull(int)}
     * @vagabond.category Placement Algorithms
     */
    PLACEMENT_EQUALSPREADHALF("equalSpreadHalf"),
    /**
     * [Placement Algorithms] Generates a placement map using 
     * {@link PlacementMapExamples#equalSpreadClientsHalfFull(int)}
     * @vagabond.category Placement Algorithms
     */
    PLACEMENT_EQUALSPREADHALF2("equalSpreadHalf2"),

    /**
     * [Placement Algorithms] Generates a placement map using 
     * {@link PlacementMapExamples#percentFilled(vagabond.placement.RandomPlacement, int)}
     * @vagabond.category Placement Algorithms
     */
    PLACEMENT_PERCENTFILL("percentFill"),

    /**
     * [Placement Algorithms] Generates a placement map using 
     * {@link PlacementMapExamples#nomadSubOptimalRCPlacement()}
     * @vagabond.category Placement Algorithms
     */
    PLACEMENT_NAHIDTEST("nahidTest"),

    /**
     * [Placement Algorithms] Generates a placement map using 
     * {@link PlacementMapExamples#nomadTest(vagabond.placement.RandomPlacement, int)}
     * @vagabond.category Placement Algorithms
     */
    PLACEMENT_NOMADTEST("nomadTest"),

    /**
     * [Placement Algorithms] Generates a placement map using 
     * {@link PlacementMapExamples#specialTest1(vagabond.placement.RandomPlacement, int)}
     * @vagabond.category Placement Algorithms
     */
    PLACEMENT_SPECIALTEST1("specialTest1"),

    // Reduction Algorithms
    /**
     * [Reduction Algorithms] Reduces a PlacementMap to CSAT and then to CNF-SAT and then runs it through Minisat
     * @vagabond.category Reduction Algorithms
     */
    REDUCTION_SAT("cnf"),
    /**
     * [Reduction Algorithms] Reduces a PlacementMap to ILP and then runs it through CPLEX
     * @vagabond.category Reduction Algorithms
     */
    REDUCTION_ILP("ilp"),
    /**
     * [Reduction Algorithms] Sends a PlacementMap to NOMAD and runs it
     * @vagabond.category Reduction Algorithms
     */
    REDUCTION_NOMAD("nomad"),;

    private String _str;

    private VagabondOptionString(String s) {
        _str = s;
    }

    /**
     * Returns a list of available/implemented Placement Algorithms
     * @return a list of strings that can be used for the placement algorithm setting
     */
    public static ArrayList<String> getPlacementAlgorithms() {
        ArrayList<String> list = new ArrayList<String>();

        list.add(PLACEMENT_RANDOM._str);

        return list;
    }

    /**
     * Returns a list of available/implemented Reduction Algorithms
     * @return a list of strings that can be used for the reduction algorithm setting
     */
    public static ArrayList<String> getReductionAlgorithms() {
        ArrayList<String> list = new ArrayList<String>();

        list.add(REDUCTION_SAT._str);
        list.add(REDUCTION_ILP._str);
        list.add(REDUCTION_NOMAD._str);

        return list;
    }

    /**
     * Returns a list of required settings that must be in the settings file
     * @return a list of required settings
     */
    public static ArrayList<String> getRequiredControlSettings() {
        ArrayList<String> list = new ArrayList<String>();

        list.add(EPOCHS._str);
        list.add(SLIDINGWINDOW._str);
        list.add(MIGRATIONBUDGET._str);
        list.add(PLACEMENT._str);
        list.add(REDUCTION._str);

        // Semi-Optional
        list.add(NUMMACHINES._str);
        list.add(NUMSLOTS._str);
        list.add(NUMCLIENTS._str);
        list.add(NUMVMS._str);

        return list;
    }

    @Override
    public String toString() {
        return _str;
    }

    /**
     * Returns the commandline equivalent with the hyphen and a space following: AUTHORS -&gt; "-authors "
     * 
     * @return direct commandline string
     */
    public String c() {
        return "-" + _str + " ";
    }

    /**
     * Returns the commandline equivalent with the hyphen and a space following: LOGLEVEL("debug") -&gt; "-loglevel debug "
     * @param param String parameter that you want to put on the command line
     * @return direct commandline string
     */
    public String c(String param) {
        return "-" + _str + " " + param + " ";
    }
}
