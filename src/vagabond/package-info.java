/**
 * Provides the classes necessary to run Vagabond.
 * <p>
 * Vagabond is a program that can determine VM migrations, within a system of machines, to limit the Information Leakage
 * while limited by a migration budget.
 */
package vagabond;