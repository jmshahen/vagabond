/*********************************************
 * OPL 12.6.1.0 Model
 * Author: jonathan
 * Creation Date: May 13, 2016 at 1:02:36 PM
 *********************************************/

int numMachines=...;
range Machines=1..numMachines;

int numClients=...;
range Clients=1..numClients;

int numVMs=...;
int migrationBudget=...;
int vmsPerClient[Clients]=...;
int machineCapacity[Machines]=...;
 
// Sum of previous Information Leakage ClinetToClient
int L[Clients,Clients]=...;
// Sum of Client VMs per Machine
int p0[Machines,Clients]=...;
// NEW Placement of the Sum of Client VMs per Machine
dvar int+ ppp1[Machines,Clients] in 0..numVMs;

minimize
	// max(c0 in Clients, c1 in Clients) (z[c0,c1] + (sum(k in Machines) (ppp1[k,c0] * ppp1[k,c1])) ); // not positive semidefinite?
  	 max(c0 in Clients, c1 in Clients) (L[c0,c1] + sum(k in Machines) (p0[k,c0] * ppp1[k,c1]) ); // not correct but optimal solution
	// sum(c in Clients, k in Machines) (p1[k,c] * p1[k,c]); // works
	// max(c0 in Clients, c1 in Clients) ( sum(k in Machines) (p1[k,c0] * p1[k,c1]) );// not positive semidefinite (PSD)
	// max(c0 in Clients, c1 in Clients) (z[c0,c1]); // works
	// sum(c0 in Clients, c1 in Clients) (z[c0,c1] + sum(k in Machines) (p1[k,c0] * p1[k,c1]) ); // not the optimal solution
	// max(c0 in Clients, c1 in Clients) (z[c0,c1] + sum(k in Machines) (ppp1[k,c0] + ppp1[k,c1]) ); // works
	// max(c0 in 1..(numClients-1)) (z[c0,c0+1] + ppp1[1,c0] * ppp1[1,c0+1] ); // not PSD
	// sum(k in Machines) (sum(c0 in Clients, c1 in Clients) (ppp1[k,c0] * ppp1[k,c1]) ); // not optimal
	// max(c0 in Clients, c1 in Clients) ( z[c0,c1] + (sum(k in Machines) ( sum(i in 1 .. ppp1[k,c0]) ( ppp1[k,c1] ) ) ) ); // fails to run
	//max(c0 in Clients, c1 in Clients) (L[c0,c1] + (sum(k in Machines) (ppp1[k,c0] * ppp1[k,c1])) );
subject to {
	///*
	// The migration budget is not exceeded
	2*migrationBudget >= sum(k in Machines,c in Clients) (abs(p0[k,c] - ppp1[k,c]));
	// The amount of VMs per Client does not change
	forall(c in Clients)
		vmsPerClient[c] == sum(k in Machines) (ppp1[k,c]);
	// The Machine capacity is not exceeded
	forall(k in Machines)
		machineCapacity[k] >= sum(c in Clients) (ppp1[k,c]);
	//*/
}