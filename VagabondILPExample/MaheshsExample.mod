/*********************************************
 * OPL 12.6.1.0 Model
 * Author: jonathan
 * Creation Date: May 14, 2016 at 2:14:07 PM
 *********************************************/

dvar boolean a0;
dvar boolean a1;
dvar boolean b0;
dvar boolean b1;

dvar boolean c0;
dvar boolean c1;
dvar boolean c2;
dvar boolean c3;

dvar boolean a;
dvar boolean b;
dvar boolean c;
dvar boolean d;
dvar boolean e;
dvar boolean f;
dvar boolean g;




subject to {
c0 == 1;
c1 == 1;
c2 == 0;
c3 == 1;

c == a * b;
d == (a + b + c) mod 2;

/*
c3 == (c2 + g);
c2 == (g || f) && !(g && f);
c1 == (d || e) && !(d && e);
c0 == a0 && b0;
d == a1 && b0;
e == a0 && b1;
f == d && e;
g == a1 && b1;
*/
}