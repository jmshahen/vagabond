/*********************************************
 * OPL 12.6.1.0 Model
 * Author: jonathan
 * Creation Date: May 14, 2016 at 2:14:07 PM
 *********************************************/


dvar boolean a0;
dvar boolean a1;
dvar boolean b0;
dvar boolean b1;

dvar boolean c0;
dvar boolean c1;
dvar boolean c2;
dvar boolean c3;

dvar int a in 0 .. 1;
dvar int b in 0 .. 1;
dvar boolean c;

dvar boolean d;
dvar boolean e;
dvar boolean f;
dvar boolean g;



minimize a+b;
subject to {

c0 >= 1;
c1 >= 1;
c2 <= 0;
c3 >= 1;

c3 == (c2 + g);
// c2 = (g OR f) AND NOT(g AND f)
c2 <= (g + f - g*f) ;//* abs((g * f)-1);
// c1 = (d or e) AND NOT(d AND e)
c1 >= (d + e - d*e) ;//* abs((d * e)-1);
c0 == a0 * b0;
d == a1 * b0;
e == a0 * b1;
f == d * e;
g == a1 * b1;


/*
a*b >= c;
c <= 0;

c == a * b;
c == a + b - pow(a,b);
*/

}