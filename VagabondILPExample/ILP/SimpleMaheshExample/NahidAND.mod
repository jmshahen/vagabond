/*********************************************
 * OPL 12.6.1.0 Model
 * Author: jonathan
 * Creation Date: May 16, 2016 at 2:04:19 PM
 *********************************************/

dvar boolean a;
dvar boolean b;
dvar boolean c;

maximize a;

subject to {
c == 0;
0 <= a + b -2*c <=1;
}